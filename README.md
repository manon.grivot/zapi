# zAPI

zAPI

## Plugin configuration

### `config.json`
The config.json file contains multiple configuration options:

****

## Projects implementing this API
**This plugin is required for the following projects:**

Chocolia/ChocoCoinflip (API v1)
Chocolia/ChocoDailyRewards (API v1)
Chocolia/ChocoRankup (API v1)
Soleria/SoleriaCoinflip (API v2)
Soleria/SoleriaDailyRewards (API v2)
Soleria/SoleriaDuels (API v2)
Soleria/SoleriaDynamicSGP (API v2)
Soleria/SoleriaRankup (API v2)
Soleria v2/Argiles (API v3)
Soleria v2/Coinflip (API v3)
Soleria v2/CustomItems (API v3)
Soleria v2/DailyRewards (API v3)
Soleria v2/Poubelle (API v3)
Soleria v2/Rankup (API v3)
Soleria v2/DatabaseTester (API v3)
Breakerland/Christmas Calendar (API v4)
Horizion/hItems (API v4)
Horizion/LoginUtils (API v4)
Lenaria/FactionsUUID (API v4)
Lenaria/zCore (API v4)
Idralwel/Trade (API v4)
