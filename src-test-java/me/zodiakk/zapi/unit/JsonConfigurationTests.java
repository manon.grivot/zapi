package me.zodiakk.zapi.unit;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import be.seeseemelk.mockbukkit.MockBukkit;
import be.seeseemelk.mockbukkit.ServerMock;
import junit.runner.BaseTestRunner;
import me.zodiakk.zapi.ZApiPlugin;
import me.zodiakk.zapi.config.JsonConfiguration;
import me.zodiakk.zapi.config.types.Duration;
import me.zodiakk.zapi.config.types.Reward;
import me.zodiakk.zapi.config.types.RewardList;
import me.zodiakk.zapi.util.ItemUtil;

public class JsonConfigurationTests {
    private static JsonConfiguration cfg;
    private static final double DBL_DELTA = 0.001d;
    private static final double DBL_NO_DELTA = Double.MIN_NORMAL;
    @SuppressWarnings("unused")
    private ServerMock server;
    @SuppressWarnings("unused")
    private ZApiPlugin plugin;

    @BeforeClass
    public static void loadConfig() {
        cfg = JsonConfiguration.getConfiguration(new File(JsonConfigurationTests.class.getClassLoader().getResource("sampleJson.json").getFile()));

        BaseTestRunner.setPreference("maxmessage", "-1");
    }

    @Before
    public void setup() {
        server = MockBukkit.mock();
        plugin = (ZApiPlugin) MockBukkit.load(ZApiPlugin.class);
    }

    @After
    public void teardown() {
        MockBukkit.unload();
    }

    @Test
    public void numberTypes() {
        assertEquals(1, (int) cfg.getInteger("aNumber"));
        assertEquals(1.4d, (double) cfg.getDouble("aNumber"), DBL_DELTA);
        assertEquals(1.4f, (float) cfg.getFloat("aNumber"), (float) DBL_DELTA);
        assertEquals(1, (short) cfg.getShort("aNumber"));
        assertEquals(1, (byte) cfg.getByte("aNumber"));
        assertEquals(1.4d, cfg.getNumber("aNumber").doubleValue(), DBL_DELTA);
        assertEquals(1L, (long) cfg.getLong("aNumber"));
        assertEquals(4, cfg.getBigInteger("iNumber").intValue());
        assertEquals(1.4d, cfg.getBigDecimal("aNumber").doubleValue(), DBL_DELTA);
    }

    @Test
    public void intArray() {
        List<Integer> list = cfg.getIntegerArray("numberList");

        assertEquals(1, (long) list.get(0));
        assertEquals(3, (long) list.get(1));
        assertEquals(4, (long) list.get(2));
    }

    @Test
    public void doubleArray() {
        List<Double> list = cfg.getDoubleArray("numberList");

        assertEquals(1.0d, list.get(0), DBL_DELTA);
        assertEquals(3.5d, list.get(1), DBL_DELTA);
        assertEquals(4.0d, list.get(2), DBL_DELTA);
    }

    @Test
    public void floatArray() {
        List<Float> list = cfg.getFloatArray("numberList");

        assertEquals(1.0f, list.get(0), DBL_DELTA);
        assertEquals(3.5f, list.get(1), DBL_DELTA);
        assertEquals(4.0f, list.get(2), DBL_DELTA);
    }

    @Test
    public void shortArray() {
        List<Short> list = cfg.getShortArray("numberList");

        assertEquals(1, (long) list.get(0));
        assertEquals(3, (long) list.get(1));
        assertEquals(4, (long) list.get(2));
    }

    @Test
    public void byteArray() {
        List<Byte> list = cfg.getByteArray("numberList");

        assertEquals(1, (long) list.get(0));
        assertEquals(3, (long) list.get(1));
        assertEquals(4, (long) list.get(2));
    }

    @Test
    public void numberArray() {
        List<Number> list = cfg.getNumberArray("numberList");

        assertEquals(1L, list.get(0).longValue());
        assertEquals(3.5d, list.get(1).doubleValue(), DBL_DELTA);
        assertEquals(4L, list.get(2).longValue());
    }

    @Test
    public void longArray() {
        List<Long> list = cfg.getLongArray("numberList");

        assertEquals(1, (long) list.get(0));
        assertEquals(3, (long) list.get(1));
        assertEquals(4, (long) list.get(2));
    }

    @Test
    public void bigdecimalArray() {
        List<BigDecimal> list = cfg.getBigDecimalArray("numberList");

        assertEquals(1.0d, list.get(0).doubleValue(), DBL_DELTA);
        assertEquals(3.5d, list.get(1).doubleValue(), DBL_DELTA);
        assertEquals(4.0d, list.get(2).doubleValue(), DBL_DELTA);
    }

    @Test
    public void bigintegerArray() {
        List<BigInteger> list = cfg.getBigIntegerArray("iNumberList");

        assertEquals(1, list.get(0).intValue());
        assertEquals(3, list.get(1).intValue());
        assertEquals(4, list.get(2).intValue());
    }

    @Test
    public void stringTypes() {
        String str = cfg.getString("string");
        List<String> strArray = cfg.getStringArray("stringList");
        Object[] strArrayExpected = {"Hello", "world", "!"};

        assertEquals("Hello world!", str);
        assertArrayEquals(strArrayExpected, strArray.toArray());
    }

    @Test
    public void booleanTypes() {
        Boolean bool = cfg.getBoolean("bool");
        List<Boolean> boolArray = cfg.getBooleanArray("boolList");
        Object[] boolArrayExpected = {false, true, false, false, true};

        assertEquals(true, bool);
        assertArrayEquals(boolArrayExpected, boolArray.toArray());
    }

    @Test
    public void durationType() {
        Duration duration = cfg.getDuration("duration");

        assertEquals(duration.getDurationMillis(), 44335093873L);
        assertEquals(duration.milliseconds(), 873L);
        assertEquals(duration.seconds(), 13L);
        assertEquals(duration.minutes(), 18L);
        assertEquals(duration.hours(), 3L);
        assertEquals(duration.days(), 3L);
        assertEquals(duration.months(), 5L);
        assertEquals(duration.years(), 1L);
    }

    @Test
    public void rewardTest() {
        Reward reward = cfg.getReward("singleReward");

        assertEquals(1.0, reward.getChance(), DBL_NO_DELTA);
        assertEquals(Material.DIAMOND_SWORD, reward.getItems().get(0).getType());
        assertEquals(64, reward.getItems().get(0).getAmount());
        assertEquals(1, reward.getItems().size());
        assertEquals(2, reward.getCommands().size());
        assertEquals("Test command", reward.getCommands().get(0));
        assertEquals("Second command", reward.getCommands().get(1));
        assertEquals(45000.0d, reward.getMoney(), DBL_DELTA);
    }

    @Test
    public void rewardListTest() {
        RewardList list = cfg.getRewardList("rewardTest");
        double total = 0.0d;

        assertEquals(3, list.getList().size());
        for (Reward reward : list.getList()) {
            total += reward.getChance();
        }
        assertEquals(1.0d, total, DBL_DELTA);
    }

    @Test
    public void inventoryTest() {
        Inventory inv = cfg.getInventory("mixedInventory");
        ItemStack stack = inv.getItem(0);
        ItemMeta meta = ItemUtil.getItemMeta(stack);

        assertEquals(InventoryType.CHEST, inv.getType());
        assertEquals(36, inv.getSize());
        assertEquals(Material.DIAMOND_SWORD, stack.getType());
        assertEquals(63, stack.getAmount());
        assertEquals(1, stack.getDurability());
        assertEquals("Super epee", meta.getDisplayName());
        assertEquals("Line 1", meta.getLore().get(0));
        assertEquals("Line 2", meta.getLore().get(1));
        for (int i = 1; i < 36; i++) {
            assertEquals(Material.AIR, inv.getItem(i).getType());
        }
    }
}
