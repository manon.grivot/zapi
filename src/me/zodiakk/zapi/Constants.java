package me.zodiakk.zapi;

import org.bukkit.ChatColor;
import org.bukkit.plugin.Plugin;

import me.zodiakk.zapi.config.JsonConfiguration;
import me.zodiakk.zapi.config.JsonConfigurationSection;

/**
 * Constants to use inside or outside the API.
 *
 * @author Zodiak
 * @since 1.1
 * @deprecated See {@link ZApi#getConfiguration()}
 */
@Deprecated
public final class Constants {
    /**
     * zAPI plugin instance.
    */
    @Deprecated
    public static Plugin PLUGIN;

    /**
     * Maximum threads for the thread pool.
     */
    @Deprecated
    public static final Integer MAX_THREADS;

    /**
     * Database anti-timeout time.
     */
    @Deprecated
    public static final Integer ANTI_TIMEOUT_PERIOD;

    /**
     * ChatSession title color.
     */
    @Deprecated
    public static final ChatColor CHAT_TITLE_COLOR;

    /**
     * Database base name.
     */
    @Deprecated
    public static final String DATABASE_BASE;

    /**
     * Whether if debug logs are enabled.
     */
    @Deprecated
    public static final Boolean EXTENDED_LOG;

    /**
     * Money sign.
     */
    @Deprecated
    public static final String MONEYSIGN;

    /**
     * Whether if database errors are fatal.
     */
    @Deprecated
    public static final String DB_INIT_ERR_FATAL;

    /**
     * Maximum lore line length.
     */
    @Deprecated
    public static final Integer MAX_LORE_LINE_LENGTH;

    static {
        JsonConfiguration config = ZApi.getConfiguration();

        MAX_THREADS = config.getInteger("maxThreads");
        CHAT_TITLE_COLOR = ChatColor.valueOf(config.getString("chatTitle"));
        EXTENDED_LOG = config.getBoolean("debug");
        MONEYSIGN = config.getString("moneySign");
        MAX_LORE_LINE_LENGTH = config.getInteger("loreLineLength");

        JsonConfigurationSection database = config.getSection("database");

        DB_INIT_ERR_FATAL = database.getBoolean("errorsAreFatal").toString();
        DATABASE_BASE = database.getString("base");
        ANTI_TIMEOUT_PERIOD = (int) database.getDuration("antiTimeoutPeriod").getDurationSeconds();
    }
}
