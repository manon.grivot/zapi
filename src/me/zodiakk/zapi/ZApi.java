package me.zodiakk.zapi;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandMap;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import co.aikar.timings.lib.MCTiming;
import co.aikar.timings.lib.TimingManager;
import me.zodiakk.zapi.commands.ApiCommand;
import me.zodiakk.zapi.config.JsonConfiguration;
import me.zodiakk.zapi.data.DataManager;
import me.zodiakk.zapi.multiversion.NMSVersion;
import me.zodiakk.zapi.threading.ThreadsManager;
import me.zodiakk.zapi.util.ChatSession;
import me.zodiakk.zapi.util.RandomUtil;
import me.zodiakk.zapi.util.ReflectionUtil;

/**
 * Main API class.
 *
 * @author Zodiak
 * @since 1.0
 */
public class ZApi {
    // Test environment switch
    public static final boolean TEST_ENV;

    private static ZApi me;
    private static ZApiPlugin plugin;

    private static ThreadsManager threads;
    private static DataManager data;

    private static HashMap<String, ApiCommand> apiCommands;

    private static TimingManager timings;
    private static MCTiming zAPITimings;

    private static JsonConfiguration cfg;

    private static NMSVersion nmsVersion;

    static {
        boolean test;
        try {
            Bukkit.getServer().getServicesManager();
            test = false;
        } catch (Exception ex) {
            test = true;
        }
        TEST_ENV = test;
    }

    protected void onLoad(ZApiPlugin plugin) {
        // I am myself
        me = this;

        // Variable init
        apiCommands = new HashMap<String, ApiCommand>();
        timings = TimingManager.of(plugin);
        zAPITimings = timings.of("zAPI");

        // Class loading ?
        ClassLoader loader = getClass().getClassLoader();
        try {
            // Temporary fix, we need a better solution because it looks AWFUL.
            loader.loadClass("me.zodiakk.zapi.data.table.TableData");
            loader.loadClass("me.zodiakk.zapi.data.table.TableField");
            loader.loadClass("me.zodiakk.zapi.data.table.TableModel");
            loader.loadClass("me.zodiakk.zapi.data.table.TableRow");
            loader.loadClass("me.zodiakk.zapi.data.table.TableValue");
            loader.loadClass("me.zodiakk.zapi.data.result.Result");
            loader.loadClass("me.zodiakk.zapi.data.result.ResultRow");
            loader.loadClass("me.zodiakk.zapi.data.result.ResultValue");
            loader.loadClass("me.zodiakk.zapi.data.query.DeleteQuery");
            loader.loadClass("me.zodiakk.zapi.data.query.InsertQuery");
            loader.loadClass("me.zodiakk.zapi.data.query.Query");
            loader.loadClass("me.zodiakk.zapi.data.query.SelectQuery");
            loader.loadClass("me.zodiakk.zapi.data.query.UnsafeQueryException");
            loader.loadClass("me.zodiakk.zapi.data.query.UpdateQuery");
            loader.loadClass("me.zodiakk.zapi.data.filter.FieldsFilter");
            loader.loadClass("me.zodiakk.zapi.data.filter.Filter");
            loader.loadClass("me.zodiakk.zapi.data.filter.LimitFilter");
            loader.loadClass("me.zodiakk.zapi.data.filter.OrderFilter");
            loader.loadClass("me.zodiakk.zapi.data.filter.OrderType");
            loader.loadClass("me.zodiakk.zapi.data.filter.WhereFilter");
            loader.loadClass("me.zodiakk.zapi.data.filter.WhereOperator");
            loader.loadClass("me.zodiakk.zapi.data.filter.WhereOperatorType");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        // Plugin
        ZApi.plugin = plugin;
    }

    @SuppressWarnings("deprecation")
    protected void onEnable(ZApiPlugin plugin) {
        // Welcome message
        // http://patorjk.com/software/taag/#p=display&h=2&v=1&f=Slant&t=zAPI%0A%0A
        String versionString = plugin.getDescription().getVersion().split("-")[0];
        char[] spaces = new char[23 - versionString.length()];

        Arrays.fill(spaces, ' ');
        Bukkit.getServer().getConsoleSender()
                .sendMessage(Arrays.asList("§3§l        ___    ____  ____",
                                           "§3§l ____  /   |  / __ \\/  _/",
                                           "§3§l/_  / / /| | / /_/ // /",
                                           "§3§l / /_/ ___ |/ ____// /",
                                           "§3§l/___/_/  |_/_/   /___/",
                                           "§3§l" + new String(spaces) + "v" + versionString).toArray(new String[6]));

        // Warning for beta versions
        if (plugin.getDescription().getVersion().contains("-")) {
            Bukkit.getLogger().warning("Please note that this build of zCustomItems is not an official release, which means that unreported bugs may occurs.");
            Bukkit.getLogger().warning("Unless you know what you're doing, this build should never be used on a production server.");
            Bukkit.getLogger().warning("I (Zodiak, the author) am not responsible for any damage caused by the use of beta builds.");
        }

        // Save config files
        List<String> files = Arrays.asList("config.json");

        for (String str : files) {
            JsonConfiguration.extractConfiguration(plugin, str);
        }

        // Constants
        Constants.PLUGIN = plugin;
        nmsVersion = NMSVersion.getCurrentVersion();

        // Timings
        timings = TimingManager.of(plugin);

        // API Managers
        threads = new ThreadsManager(this);
        if (!getConfiguration().getBoolean("disableDatabase")) {
            data = new DataManager(this);
        }

        // Utils
        ChatSession csEvents = new ChatSession("event", null);
        Bukkit.getServer().getPluginManager().registerEvents(csEvents, plugin);

        // API Command
        plugin.getCommand("api").setExecutor(new ZApiCommand(this, apiCommands));
    }

    /**
     * <b>INTERNAL</b>. Called when the zAPI plugin unloads
     *
     * @param plugin The plugin
     */
    public void onDisable(ZApiPlugin plugin) {
        Bukkit.getLogger().info("Closing ThreadsManager...");
        threads.close(this);
        Bukkit.getLogger().info("Waiting for all threads to resolve");
        try {
            threads.awaitClosing(30000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            postError(e);
            Bukkit.getLogger().severe("Could not close ThreadsManager (timed out after 30s).");
        }
    }

    protected static ZApi myself() {
        return me;
    }

    protected static HashMap<String, ApiCommand> getApiCommands() {
        return apiCommands;
    }

    /**
     * Get the current Bukkit implementation version.
     * @return The Bukkit implementation version
     */
    public static NMSVersion getBukkitVersion() {
        return nmsVersion;
    }

    /**
     * Get the zAPI logger. It should be used by default for any log message from
     * modules.
     *
     * @return The zAPI logger.
     * @deprecated Use Bukkit logger
     */
    public static Logger getLogger() {
        return Bukkit.getLogger();
    }

    /**
     * Log a debug message.
     * This message will not appear in the console if the debug option is disabled.
     *
     * @param message Message to log
     */
    public static void logDebug(String message) {
        if (getConfiguration().getBoolean("debug")) {
            Bukkit.getLogger().info(String.format("(DEBUG) %s", message));
        }
    }

    /**
     * Get the thread manager.
     *
     * @return The thread manager.
     */
    public static ThreadsManager getThreadsManager() {
        return threads;
    }

    /**
     * Get the data manager.
     *
     * @return The data manager
     */
    public static DataManager getData() {
        if (!getConfiguration().getBoolean("disableDatabase")) {
            return data;
        }
        throw new IllegalStateException("Database is disabled.");
    }

    /**
     * Get the plugin constants.
     *
     * @return The plugin constants
     * @deprecated {@link Constants} is deprecated, please use {@link #getConfiguration()}
     */
    @Deprecated
    public static Constants getConstants() {
        return new Constants();
    }

    /**
     * Get the plugin configuration.
     *
     * @return The plugin configuration
     */
    public static JsonConfiguration getConfiguration() {
        if (cfg == null) {
            cfg = JsonConfiguration.getConfiguration(new File(plugin.getDataFolder(), "config.json"));
        }
        return cfg;
    }

    /**
     * Post an error to a player with an error code to trace this error.
     *
     * @param e  An {@link Exception} representing this error
     * @param pl The player, represented by a {@link Player}, experiencing this
     *           error
     * @return The error code.
     */
    public static String postError(Exception e, Player pl) {
        return postError(e, pl.getUniqueId());
    }

    /**
     * Post an error to a player with an error code to trace this error.
     *
     * @param e  An {@link Exception} representing this error
     * @param pl The player, represented by an {@link OfflinePlayer}, experiencing
     *           this error
     * @return The error code.
     */
    public static String postError(Exception e, OfflinePlayer pl) {
        return postError(e, pl.getUniqueId());
    }

    /**
     * Post an error to a player with an error code to trace this error.
     *
     * @param e  An {@link Exception} representing this error
     * @param pl The player, represented by an {@link UUID}, experiencing this error
     * @return The error code.
     */
    public static String postError(Exception e, UUID pl) {
        String code = RandomUtil.getString(7);
        Bukkit.getLogger().severe("Error with code #" + code + ":");
        e.printStackTrace();
        ChatSession.sendOne(pl, "Erreur",
                "§cUne erreur est survenue. Si le problème persiste, veuillez signaler le problème avec ce code : #"
                        + code);
        return code;
    }

    /**
     * Post an error to the console with an error code to trace this error.
     *
     * @param e An {@link Exception} representing this error
     * @return The error code.
     */
    public static String postError(Exception e) {
        if (TEST_ENV) {
            e.printStackTrace();
            return null;
        }

        String code = RandomUtil.getString(7);
        Bukkit.getLogger().severe("Error with code #" + code + ":");
        e.printStackTrace();
        return code;
    }

    public static void postTestError(Exception e) {
        if (!TEST_ENV) {
            Bukkit.getLogger().severe("A test error has been fired. It should not have happened.");
        }

        e.printStackTrace();
    }

    /**
     * Register a new command to the server.
     *
     * @param label   The name of the command
     * @param command A {@link BukkitCommand} representing this command
     * @return True if the command was successfully registered.
     */
    public static boolean registerCommand(String label, BukkitCommand command) {
        try {
            return ((CommandMap) ReflectionUtil.getNMSClass("CraftServer").getMethod("getCommandMap").invoke(Bukkit.getServer())).register(label, command);
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * Register a new sub-command to the API /api command.
     * @param label   The name of the sub-command
     * @param command A {@link ApiCommand} representing this command
     * @return True if the command was successfully registered.
     */
    public static boolean registerApiCommand(String label, ApiCommand command) {
        if (apiCommands.containsKey(label)) {
            return false;
        } else {
            apiCommands.put(label, command);
            ZApi.logDebug("Registered sub api command \"/api " + label + "\".");
        }
        plugin.getCommand("api").setExecutor(new ZApiCommand(me, apiCommands));
        return true;
    }

    /**
     * Get the timings of the specified key in the zAPI namespace.
     * @param name Timings key
     * @return Timings of this key
     */
    public static MCTiming timingsOf(String name) {
        return timings.of(name, zAPITimings);
    }

    /**
     * Get the plugin representation of zAPI.
     * @return Plugin representation
     */
    public static Plugin toPlugin() {
        return plugin;
    }
}
