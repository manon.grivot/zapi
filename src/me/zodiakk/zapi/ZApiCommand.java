package me.zodiakk.zapi;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import co.aikar.timings.lib.MCTiming;
import de.tr7zw.changeme.nbtapi.NBTCompound;
import de.tr7zw.changeme.nbtapi.NBTCompoundList;
import de.tr7zw.changeme.nbtapi.NBTItem;
import me.zodiakk.zapi.commands.ApiCommand;
import me.zodiakk.zapi.commands.HelpMenu;
import me.zodiakk.zapi.tests.TestSuite;
import me.zodiakk.zapi.util.ChatSession;

// TODO: Requires refactoring
/**
 * Main command for managing and manipulating the API.
 *
 * @author Zodiak
 * @since 3.0
 */
final class ZApiCommand implements CommandExecutor {
    HelpMenu menu;
    HashMap<String, ApiCommand> commands;

    ZApiCommand(ZApi core, HashMap<String, ApiCommand> subCommands) {
        this.core = core;
        this.commandTiming = ZApi.timingsOf("ZApiCommand");
        menu = new HelpMenu("API", "api");
        menu.addCommand("help <page>", "Get this help menu.");
        commands = subCommands;
        commands.put("status", new SubCommandStatus());
        commands.put("nbt", new SubCommandNbt());
        commands.put("data", new SubCommandData());
        commands.put("test", new SubCommandTest());
        for (Entry<String, ApiCommand> cmd : commands.entrySet()) {
            Map<String, String> lines = cmd.getValue().getHelpLines();
            if (lines == null) {
                continue;
            }
            for (Entry<String, String> e : lines.entrySet()) {
                menu.addCommand(cmd.getKey() + " " + e.getKey(), e.getValue());
            }
        }
        menu.buildMenu(6);
    }

    ZApi core;
    MCTiming commandTiming;

    @Override
    public boolean onCommand(CommandSender s, Command c, String lb, String[] arg) {
        commandTiming.startTiming();

        ChatSession cs;
        if (s instanceof Player) {
            cs = new ChatSession("API", Bukkit.getPlayer(s.getName()).getUniqueId());
        } else {
            cs = new ChatSession("API", null);
        }

        Set<String> subs = commands.keySet();
        if (arg.length == 0) {
            sendHelp(cs, 1);
            return true;
        } else if (arg[0].equalsIgnoreCase("help")) {
            if (arg.length == 1) {
                sendHelp(cs, 1);
                return true;
            } else {
                sendHelp(cs, Integer.parseInt(arg[1]));
                return true;
            }
        } else if (!subs.contains(arg[0])) {
            sendHelp(cs, 1);
            return true;
        }

        for (String sub : subs) {
            if (sub.equals(arg[0])) {
                String[] subargs = Arrays.copyOfRange(arg, 1, arg.length);
                if (!commands.get(sub).onCommand(s, cs, sub, subargs)) {
                    sendHelp(cs, 1);
                } else {
                    commandTiming.stopTiming();
                }
                return true;
            }
        }

        commandTiming.stopTiming();
        return true;
    }

    private boolean sendHelp(ChatSession cs, Integer page) {
        if (page > menu.getPageCount()) {
            return false;
        } else {
            menu.sendPage(cs, page);
        }
        commandTiming.stopTiming();
        return true;
    }

    private class SubCommandNbt implements ApiCommand {

        @Override
        public boolean onCommand(CommandSender sender, ChatSession cs, String label, String[] arg) {
            ZApi.getThreadsManager().execute(new Runnable() {
                @Override
                public void run() {
                    NBTItem nbti = new NBTItem(cs.getPlayer().getInventory().getItemInMainHand());
                    switch (arg[0]) {
                        case "set":
                            nbti.setString(arg[1], String.join(" ", Arrays.copyOfRange(arg, 2, arg.length)));
                            cs.send(arg[1] + " > " + nbti.hasKey(arg[1]) + ", " + nbti.getString(arg[1]) + ".");
                            cs.getPlayer().getInventory().setItemInMainHand(nbti.getItem());
                            break;
                        case "get":
                            cs.send(arg[1] + " > " + nbti.hasKey(arg[1]) + ", " + nbti.getString(arg[1]) + ".");
                            break;
                        case "getall":
                            sendAllNbtTags(cs.getPlayer().getInventory().getItemInMainHand(), cs);
                            break;
                        default:
                            sendHelp(cs, 1);
                            break;
                    }
                }
            });
            return true;
        }

        private void sendAllNbtTags(ItemStack stack, ChatSession cs) {
            NBTItem nbti = new NBTItem(stack);
            sendAllNbtCompoundTags(nbti, cs, 0);
        }

        private void sendAllNbtCompoundTags(NBTCompound compound, ChatSession cs, Integer indent) {
            if (indent > 0) {
                cs.send(getIndented("§l" + compound.getName(), indent - 1));
            }
            for (String key : compound.getKeys()) {
                switch (compound.getType(key)) {
                case NBTTagByte:
                    cs.send(getIndented(key + " (byte): " + compound.getByte(key), indent));
                    break;
                case NBTTagByteArray:
                    cs.send(getIndented(key + " (byte[]): " + compound.getByteArray(key), indent));
                    break;
                case NBTTagCompound:
                    sendAllNbtCompoundTags(compound.getCompound(key), cs, indent + 1);
                    break;
                case NBTTagDouble:
                    cs.send(getIndented(key + " (double): " + compound.getDouble(key), indent));
                    break;
                case NBTTagEnd:
                    cs.send(getIndented(key + " (void)", indent));
                    break;
                case NBTTagFloat:
                    cs.send(getIndented(key + " (float): " + compound.getFloat(key), indent));
                    break;
                case NBTTagInt:
                    cs.send(getIndented(key + " (int): " + compound.getInteger(key), indent));
                    break;
                case NBTTagIntArray:
                    cs.send(getIndented(key + " (int[]): " + compound.getIntArray(key), indent));
                    break;
                case NBTTagList:
                    cs.send(getIndented("§l" + key + " (List)", indent));
                    sendNbtList(compound.getCompoundList(key), cs, indent + 1);
                    break;
                case NBTTagLong:
                    cs.send(getIndented(key + " (long): " + compound.getLong(key), indent));
                    break;
                case NBTTagShort:
                    cs.send(getIndented(key + " (short): " + compound.getShort(key), indent));
                    break;
                case NBTTagString:
                    cs.send(getIndented(key + " (String): " + compound.getString(key), indent));
                    break;
                default:
                    cs.send(getIndented(key + " (?)", indent));
                    break;
                }
            }
        }

        private void sendNbtList(NBTCompoundList list, ChatSession cs, Integer indent) {
            list.forEach(elem -> {
                for (String key : elem.getKeys()) {
                    if (elem.getString(key) != null) {
                        cs.send(getIndented(key + " (String): " + elem.getString(key), indent));
                    } else if (elem.getDouble(key) != 0.0d) {
                        cs.send(getIndented(key + " (double): " + elem.getDouble(key), indent));
                    } else if (elem.getInteger(key) != 0) {
                        cs.send(getIndented(key + " (int): " + elem.getInteger(key), indent));
                    } else {
                        cs.send(getIndented(key + " (?): 0", indent));
                    }
                }
            });
        }

        private String getIndented(String text, Integer indent) {
            StringBuilder builder = new StringBuilder("");
            for (int i = 0; i < indent; i++) {
                builder.append("  ");
            }
            builder.append(text);
            return builder.toString();
        }

        @Override
        public HashMap<String, String> getHelpLines() {
            menu.addCommand("nbt get <key>", "Get the NBT content at the given key from the currently held item.");
            menu.addCommand("nbt set <key> <value ..>", "Set the NBT content at the given key from the currently held item.");
            menu.addCommand("nbt getall", "Get all NBT tags from the currently held item.");
            return null;
        }
    }

    private class SubCommandStatus implements ApiCommand {
        @Override
        public boolean onCommand(CommandSender sender, ChatSession cs, String label, String[] arg) {
            Player pl = cs.getPlayer();

            pl.sendMessage("§6§lThreadsManager:§7 " + ZApi.getThreadsManager().getActiveCount() + "/" + ZApi.getConfiguration().getInteger("maxThreads") + " used threads.");
            return true;
        }

        @Override
        public HashMap<String, String> getHelpLines() {
            menu.addCommand("status", "Get status of API components.");
            return null;
        }
    }

    private class SubCommandData implements ApiCommand {

        @Override
        public boolean onCommand(CommandSender sender, ChatSession cs, String label, String[] arg) {
            ZApi.getThreadsManager().execute(new Runnable() {
                @Override
                public void run() {
                    cs.send("§eThis API has been removed and the subcommand needs to be updated.");
                }
            });
            return true;
        }

        @Override
        public HashMap<String, String> getHelpLines() {
            menu.addCommand("data get <table> <row> <field>", "(deprecated) Get the value of a field.");
            menu.addCommand("data set <table> <row> <field> <value>", "(deprecated) Set the value of a field.");
            menu.addCommand("data getplayer <player> <field>", "(deprecated) Get the value of a field in the player table.");
            menu.addCommand("data setplayer <player> <field> <value>", "(deprecated) Set the value of a field in the player table.");
            return null;
        }
    }

    private class SubCommandTest implements ApiCommand {
        Map<String, TestSuite> testSuites;

        public SubCommandTest() {
            testSuites = ZApiTests.getTests();
        }

        @Override
        public boolean onCommand(CommandSender sender, ChatSession session, String label, String[] args) {
            ZApi.getThreadsManager().execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (args.length < 1) {
                            session.send("Not enough arguments.");
                            return;
                        }
                        switch (args[0]) {
                            case "run":
                                if (args.length < 2) {
                                    session.send("Not enough arguments.");
                                    return;
                                }
                                if (!testSuites.containsKey(args[1])) {
                                    session.send("This test suite does not exists.");
                                    return;
                                }
                                if (args.length == 3) {
                                    testSuites.get(args[1]).runOneCase(args[2], session);
                                } else {
                                    testSuites.get(args[1]).runSuite(session);
                                }
                                break;
                            case "output":
                                if (args.length < 2) {
                                    session.send("Not enough arguments.");
                                    return;
                                }
                                if (!testSuites.containsKey(args[1])) {
                                    session.send("This test suite does not exists.");
                                    return;
                                }
                                if (args.length == 3) {
                                    testSuites.get(args[1]).printLastOutput(session, Integer.parseInt(args[2]));
                                } else {
                                    testSuites.get(args[1]).printLastOutput(session, 0);
                                }
                                break;
                            case "list":
                                session.send("§7List of all test suites available: §e" + String.join("§7, §e", testSuites.keySet()) + "§7.");
                                break;
                            default:
                                break;
                        }
                    } catch (Exception ex) {
                        ZApi.postError(ex, session.getPlayer());
                    }
                }
            });
            return true;
        }

        @Override
        public Map<String, String> getHelpLines() {
            menu.addCommand("test run <suite>", "Run this test suite.");
            menu.addCommand("test run <suite> <test>", "Run a specific test of this test suite.");
            menu.addCommand("test output <suite>", "Get the output of the last run of this test suite.");
            menu.addCommand("test output <suite> <trim>", "Get the last x lines from the output of the last run of this test suite.");
            menu.addCommand("test list", "List all the test suites available.");
            return null;
        }
    }
}
