package me.zodiakk.zapi;

import java.io.File;

import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.JavaPluginLoader;

/**
 * zAPI plugin.
 *
 * @author Zodiak
 * @since 2.0
 */
public class ZApiPlugin extends JavaPlugin {
    private static ZApiPlugin instance;
    private ZApi apiInstance;

    // Default constructor
    public ZApiPlugin() {
        super();

        if (instance != null) {
            throw new IllegalAccessError("Constructor has been called twice!");
        }
        instance = this;
    }

    // MockBukkit constructor
    protected ZApiPlugin(JavaPluginLoader loader, PluginDescriptionFile desc, File dataFolder, File file) {
        super(loader, desc, dataFolder, file);

        instance = this;
    }

    @Override
    public void onLoad() {
        apiInstance = new ZApi();
        apiInstance.onLoad(this);
    }

    @Override
    public void onEnable() {
        apiInstance.onEnable(this);
    }

    @Override
    public void onDisable() {
        apiInstance.onDisable(this);
    }

    protected static final ZApiPlugin getInstance() {
        return instance;
    }
}
