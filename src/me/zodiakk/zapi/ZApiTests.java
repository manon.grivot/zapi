package me.zodiakk.zapi;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;

import me.zodiakk.zapi.tests.TestAsserter;
import me.zodiakk.zapi.tests.TestCase;
import me.zodiakk.zapi.tests.TestException;
import me.zodiakk.zapi.tests.TestResult;
import me.zodiakk.zapi.tests.TestSuite;

// TODO: Refactoring
// All bad tests should be actual unit tests, not dummy tests to check the return value
final class ZApiTests {
    private ZApiTests() {
    }

    public static Map<String, TestSuite> getTests() {
        Map<String, TestSuite> suites = new HashMap<String, TestSuite>();

        suites.put("good_examples", getGoodExamples());
        suites.put("bad1", badSuite1());
        suites.put("bad2", badSuite2());
        suites.put("bad3", badSuite3());
        suites.put("bad4", badSuite4());
        suites.put("bad5", badSuite5());
        suites.put("bad6", badSuite6());
        suites.put("bad7", badSuite7());
        suites.put("bad8", badSuite8());
        suites.put("bad9", badSuite9());
        suites.put("hashcode", hashCodeItems());

        return suites;
    }

    private static TestSuite getGoodExamples() {
        TestSuite suite = new TestSuite();

        suite.addTestCase(new TestCase("test") {
            @Override
            public TestResult run() throws TestException {
                TestAsserter asserter = new TestAsserter(this);

                asserter.assertEquals(1, 1);
                asserter.assertEquals("Bonjour", "Bonjour");
                asserter.assertNotEquals("Bonjour", "Au revoir");
                asserter.assertNull(null);
                asserter.assertNotNull(0);
                asserter.assertComparator("BONJOUR", 7, (actual, reference) -> {
                    if (((String) actual).length() == (Integer) reference) {
                        return true;
                    } else {
                        return false;
                    }
                });
                asserter.assertRawComparator("BONJOUR", 7, (actual, reference) -> {
                    if (((String) actual).length() == (Integer) reference) {
                        return true;
                    } else {
                        return false;
                    }
                });
                return TestResult.OK;
            }
        });
        suite.addTestCase(new TestCase("test2") {
            @Override
            public TestResult run() throws TestException {
                TestAsserter asserter = new TestAsserter(this);

                asserter.assertEquals(1, 1);
                asserter.assertEquals("Bonjour", "Bonjour");
                asserter.assertNotEquals("Bonjour", "Au revoir");
                asserter.assertNull(null);
                asserter.assertNotNull(0);
                asserter.assertComparator("BONJOUR", 7, (actual, reference) -> {
                    if (((String) actual).length() == (Integer) reference) {
                        return true;
                    } else {
                        return false;
                    }
                });
                asserter.assertRawComparator("BONJOUR", 7, (actual, reference) -> {
                    if (((String) actual).length() == (Integer) reference) {
                        return true;
                    } else {
                        return false;
                    }
                });
                return TestResult.OK;
            }
        });
        suite.addTestCase(new TestCase("test3") {
            @Override
            public TestResult run() throws TestException {
                TestAsserter asserter = new TestAsserter(this);

                asserter.assertEquals(1, 1);
                asserter.assertEquals("Bonjour", "Bonjour");
                asserter.assertNotEquals("Bonjour", "Au revoir");
                asserter.assertNull(null);
                asserter.assertNotNull(0);
                asserter.assertComparator("BONJOUR", 7, (actual, reference) -> {
                    if (((String) actual).length() == (Integer) reference) {
                        return true;
                    } else {
                        return false;
                    }
                });
                asserter.assertRawComparator("BONJOUR", 7, (actual, reference) -> {
                    if (((String) actual).length() == (Integer) reference) {
                        return true;
                    } else {
                        return false;
                    }
                });
                return TestResult.OK;
            }
        });
        suite.addTestCase(new TestCase("skipped one") {
            @Override
            public TestResult run() throws TestException {
                return TestResult.SKIPPED;
            }
        });
        suite.addTestCase(new TestCase("a failed one") {
            @Override
            public TestResult run() throws TestException {
                return TestResult.FAILED;
            }
        });

        return suite;
    }

    private static TestSuite badSuite1() {
        TestSuite suite = new TestSuite();

        suite.addTestCase(new TestCase("good") {
            @Override
            public TestResult run() throws TestException {
                return TestResult.OK;
            }
        });
        suite.addTestCase(new TestCase("crash") {
            @Override
            public TestResult run() throws TestException {
                String test = getNull();
                test.length();
                return TestResult.OK;
            }

            public String getNull() {
                return null;
            }
        });
        suite.addTestCase(new TestCase("good2") {
            @Override
            public TestResult run() throws TestException {
                return TestResult.OK;
            }
        });

        return suite;
    }

    private static TestSuite badSuite2() {
        TestSuite suite = new TestSuite();

        suite.addTestCase(new TestCase("bad") {
            @Override
            public TestResult run() throws TestException {
                TestAsserter asserter = new TestAsserter(this);

                asserter.assertEquals(1, 2);
                return TestResult.OK;
            }
        });

        return suite;
    }

    private static TestSuite badSuite3() {
        TestSuite suite = new TestSuite();

        suite.addTestCase(new TestCase("bad") {
            @Override
            public TestResult run() throws TestException {
                TestAsserter asserter = new TestAsserter(this);

                asserter.assertNotEquals(1, 1);
                return TestResult.OK;
            }
        });

        return suite;
    }

    private static TestSuite badSuite4() {
        TestSuite suite = new TestSuite();

        suite.addTestCase(new TestCase("bad") {
            @Override
            public TestResult run() throws TestException {
                TestAsserter asserter = new TestAsserter(this);

                asserter.assertNull(1);
                return TestResult.OK;
            }
        });

        return suite;
    }

    private static TestSuite badSuite5() {
        TestSuite suite = new TestSuite();

        suite.addTestCase(new TestCase("bad") {
            @Override
            public TestResult run() throws TestException {
                TestAsserter asserter = new TestAsserter(this);

                asserter.assertNotNull(null);
                return TestResult.OK;
            }
        });

        return suite;
    }

    private static TestSuite badSuite6() {
        TestSuite suite = new TestSuite();

        suite.addTestCase(new TestCase("bad") {
            @Override
            public TestResult run() throws TestException {
                TestAsserter asserter = new TestAsserter(this);

                asserter.assertRawComparator(1, 2, (a, b) -> {
                    return a == b;
                });
                return TestResult.OK;
            }
        });

        return suite;
    }

    private static TestSuite badSuite7() {
        TestSuite suite = new TestSuite();

        suite.addTestCase(new TestCase("bad") {
            @Override
            public TestResult run() throws TestException {
                TestAsserter asserter = new TestAsserter(this);

                asserter.assertComparator(1, 1, (a, b) -> {
                    return a != b;
                });
                return TestResult.OK;
            }
        });

        return suite;
    }

    private static TestSuite badSuite8() {
        TestSuite suite = new TestSuite();

        suite.addTestCase(new TestCase("bad") {
            @Override
            public TestResult run() throws TestException {
                TestAsserter asserter = new TestAsserter(this);

                asserter.assertEquals(1, 2, "LOL SA MARCH PA");
                return TestResult.OK;
            }
        });

        return suite;
    }

    private static TestSuite badSuite9() {
        TestSuite suite = new TestSuite();

        suite.addTestCase(new TestCase("bad") {
            @Override
            public TestResult run() throws TestException {
                TestAsserter asserter = new TestAsserter(this);

                asserter.assertNotNull(null, "MY GREAT POEM IS ACTUALLY NULL");
                return TestResult.OK;
            }
        });

        return suite;
    }

    private static TestSuite hashCodeItems() {
        TestSuite suite = new TestSuite();

        suite.addTestCase(new TestCase("nbt-perfs") {
            @Override
            public TestResult run() throws TestException {
                ItemStack stack = Bukkit.getOnlinePlayers().stream().findAny().get().getInventory().getItemInMainHand();
                Bukkit.getLogger().info("Hascode: " + stack.hashCode() + ".");
                Bukkit.getLogger().info("toString: " + stack.toString() + ".");
                return TestResult.OK;
            }
        });

        return suite;
    }
}
