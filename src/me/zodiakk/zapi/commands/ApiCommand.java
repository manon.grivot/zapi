package me.zodiakk.zapi.commands;

import java.util.Map;

import org.bukkit.command.CommandSender;

import me.zodiakk.zapi.util.ChatSession;

/**
 * A sub-command to the API /api command.
 *
 * @author Zodiak
 * @since 3.0
 */
public interface ApiCommand {
    /**
     * What to do when the api command is called.
     * @param sender Sender of this command
     * @param session Chat session from the sender
     * @param label Name of the api command being used
     * @param args Arguments of the api command
     * @return True if the command completed successfully, false if it did not. When returning false, the help will be printed.
     */
    public abstract boolean onCommand(CommandSender sender, ChatSession session, String label, String[] args);

    /**
     * Retrieve the help lines of this api command, represented by a {@link Map}.
     * The key is one sub-command of this api command, and the value the help associated with it.
     * @return This {@link Map}.
     */
    public abstract Map<String, String> getHelpLines();
}
