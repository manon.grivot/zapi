package me.zodiakk.zapi.commands;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.zodiakk.zapi.util.ChatSession;
import net.md_5.bungee.api.ChatColor;

/**
 * A class for creating help menu's menus.
 *
 * @author Zodiak
 * @since 3.0
 */
public class HelpMenu {
    private static final int DEFAULT_CMDS_PER_PAGE = 6;

    private String menuName;
    private String menuCommand;
    private LinkedHashMap<String, String> menuLines;
    private HashMap<Integer, String[]> generatedPages;

    /**
     * Create a menu with the specified command label.
     * @param commandLabel Menu command label
     * @deprecated Please use {@link HelpMenu#HelpMenu(String, String)}
    */
    @Deprecated
    public HelpMenu(String commandLabel) {
        this("/" + commandLabel, commandLabel);
    }

    /**
     * Create a menu with the specified name and command label.
     * @param name Menu name
     * @param commandLabel Menu command label (for example, "/help")
     */
    public HelpMenu(String name, String commandLabel) {
        menuName = name;
        menuCommand = commandLabel;
        menuLines = new LinkedHashMap<String, String>();
    }

    /**
     * Add a sub-command to the menu.
     * @param command Command to add, without the label
     * @param description Description of the command
     */
    public void addCommand(String command, String description) {
        menuLines.put(command, description);
    }

    /**
     * Build this help menu.
     * <p>Must be called after adding all commands to the menu.
     * @deprecated This method has been renamed to {@link HelpMenu#buildMenu()}
     */
    public void buildPages() {
        buildMenu();
    }

    /**
     * Build this help menu.
     * <p>Must be called after adding all commands to the menu.
     * @param commandsPerPage Amount of commands per page
     * @deprecated This method has been renamed to {@link HelpMenu#buildMenu(Integer)}
     */
    public void buildPages(Integer commandsPerPage) {
        buildMenu();
    }

    /**
     * Build this help menu.
     * <p>Must be called after adding all commands to the menu.
     */
    public void buildMenu() {
        buildMenu(DEFAULT_CMDS_PER_PAGE);
    }

    /**
     * Build this help menu.
     * <p>Must be called after adding all commands to the menu.
     * @param commandsPerPage Amount of commands per page
     */
    public void buildMenu(Integer commandsPerPage) {
        int totalPages = (int) Math.ceil((double) menuLines.size() / (double) commandsPerPage);
        int currentPageIdx = 0;
        String[] currentPage = null;
        int i = commandsPerPage;

        generatedPages = new HashMap<Integer, String[]>();
        for (Entry<String, String> line : menuLines.entrySet()) {
            if (i == commandsPerPage) {
                if (currentPage != null) {
                    generatedPages.put(currentPageIdx, currentPage);
                }
                currentPageIdx++;
                i = 0;
                currentPage = new String[commandsPerPage + 3];
                currentPage[0] = "&7 -------[ &2Aide : &e" + menuName + " &7(&2Page : &e" + currentPageIdx + "&7/&e" + totalPages + "&7) ]-------";
                currentPage[1] = "";
                currentPage[commandsPerPage] = "";
            }
            i++;
            currentPage[i + 1] = " &7➜ &e/" + menuCommand + " " + line.getKey() + " &8&l- &3" + line.getValue();
        }
        i++;
        currentPage = Arrays.copyOf(currentPage, i + 2);
        currentPage[i + 1] = "";
        generatedPages.put(currentPageIdx, currentPage);
    }

    /**
     * Get a String array representing a page.
     * @param pageNumber Index of the page
     * @return String array of this page, {@code null} if the page doesn't exist.
     */
    public String[] getPage(Integer pageNumber) {
        return generatedPages.get(pageNumber);
    }

    /**
     * Get the command label of this menu.
     * @return Menu command label
     * @deprecated This method has been renamed to {@link HelpMenu#getCommandLabel()}
     */
    public String getMainCommand() {
        return menuCommand;
    }

    /**
     * Get the command label of this menu.
     * @return Menu command label
     */
    public String getCommandLabel() {
        return menuCommand;
    }

    /**
     * Get the total amount of pages.
     * @return Pages count
     */
    public int getPageCount() {
        return generatedPages.size();
    }

    /**
     * Send a page to a Player.
     * @param player Player to send the page to
     * @param page Index of the page to send
     * @deprecated Useless method, please use {@link HelpMenu#sendPage(CommandSender, Integer)}
     */
    public void sendPageToPlayer(Player player, Integer page) {
        sendPage(player, page);
    }

    /**
     * Send a page to a CommandSender (such as a Player).
     * @param sender CommandSender to send the page to
     * @param page Index of the page to send
     */
    public void sendPage(CommandSender sender, Integer page) {
        if (page > getPageCount()) {
            return;
        }
        for (String str : generatedPages.get(page)) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', str));
        }
    }

    /**
     * Send a page to a ChatSession.
     * @param cs ChatSession to send the page to
     * @param page Index of the page to send
     */
    public void sendPage(ChatSession cs, Integer page) {
        if (page > getPageCount()) {
            return;
        }
        for (String str : generatedPages.get(page)) {
            cs.getPlayer().sendMessage(str);
        }
    }
}
