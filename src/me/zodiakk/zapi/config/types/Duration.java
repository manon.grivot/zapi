package me.zodiakk.zapi.config.types;

import org.bukkit.Bukkit;

/**
 * A class representing durations.
 * <p>
 * Months are assumed to be 30 days longs, which means that a year is
 * represented by 360 days instead of a normal 365 days.
 * <p>
 * Durations are represented by strings with the following format:
 * {@code 1y2mo3w4d5h6m7s8t9ms}
 * <p>
 * Where notations are as follows:<br>
 * - <b>y</b>: Years<br>
 * - <b>mo</b>: Months<br>
 * - <b>w</b>: Weeks<br>
 * - <b>d</b>: Days<br>
 * - <b>h</b>: Hours<br>
 * - <b>m</b>: Minutes<br>
 * - <b>s</b>: Seconds<br>
 * - <b>t</b>: Ticks (1 tick = 1/20 second)<br>
 * - <b>ms</b>: Milliseconds
 *
 * @author Zodiak
 * @since 4.1
 */
public class Duration {
    private static final long MILLISECONDS_PER_SECOND = 1000L;
    private static final long SECONDS_PER_MINUTE = 60L;
    private static final long MINUTES_PER_HOURS = 60L;
    private static final long HOURS_PER_DAY = 24L;
    private static final long DAYS_PER_MONTH = 30L;
    private static final long MONTHS_PER_YEAR = 12L;
    private static final long TICK_MILLIS = 50L;
    private static final long SECOND_MILLIS = 1L * MILLISECONDS_PER_SECOND;
    private static final long MINUTE_MILLIS = SECOND_MILLIS * SECONDS_PER_MINUTE;
    private static final long HOUR_MILLIS = MINUTE_MILLIS * MINUTES_PER_HOURS;
    private static final long DAY_MILLIS = HOUR_MILLIS * HOURS_PER_DAY;
    private static final long WEEK_MILLIS = DAY_MILLIS * 7L;
    private static final long MONTH_MILLIS = DAY_MILLIS * DAYS_PER_MONTH;
    private static final long YEAR_MILLIS = MONTH_MILLIS * MONTHS_PER_YEAR;
    long millis = 0;

    /**
     * Create a new duration from a string.
     * @param durationString Duration
     */
    public Duration(String durationString) {
        if (durationString == null || durationString.length() == 0) {
            return;
        }

        durationString.replace(" ", "");

        String[] singleDurationStrings = durationString.split("[^0-9]{1,}");
        int idx = 0;

        for (String duration : singleDurationStrings) {
            if (duration == null || duration.length() == 0) {
                continue;
            }
            idx += duration.length();
            String unitString = durationString.substring(idx).split("[0-9]", 2)[0].toLowerCase();
            long durationLong = Long.parseLong(duration);

            switch (unitString) {
            case "y":
                millis += YEAR_MILLIS * durationLong;
                break;
            case "mo":
                millis += MONTH_MILLIS * durationLong;
                break;
            case "w":
                millis += WEEK_MILLIS * durationLong;
                break;
            case "d":
                millis += DAY_MILLIS * durationLong;
                break;
            case "h":
                millis += HOUR_MILLIS * durationLong;
                break;
            case "m":
                millis += MINUTE_MILLIS * durationLong;
                break;
            case "s":
                millis += SECOND_MILLIS * durationLong;
                break;
            case "t":
                millis += TICK_MILLIS * durationLong;
                break;
            case "ms":
                millis += durationLong;
                break;
            default:
                Bukkit.getLogger().warning(unitString + " is not a time unit.");
                break;
            }
            idx += unitString.length();
        }
    }

    /**
     * Get the total duration in milliseconds.
     * @return Milliseconds
     */
    public long getDurationMillis() {
        return millis;
    }

    /**
     * Get the total duration in ticks.
     * @return Ticks
     */
    public long getDurationTicks() {
        return millis / TICK_MILLIS;
    }

    /**
     * Get the total duration in seconds.
     * @return Seconds
     */
    public long getDurationSeconds() {
        return millis / SECOND_MILLIS;
    }

    /**
     * Get the total duration in minutes.
     * @return Minutes
     */
    public long getDurationMinutes() {
        return millis / MINUTE_MILLIS;
    }

    /**
     * Get the total duration in hours.
     * @return Hours
     */
    public long getDurationHours() {
        return millis / HOUR_MILLIS;
    }

    /**
     * Get the total duration in days.
     * @return Days
     */
    public long getDurationDays() {
        return millis / DAY_MILLIS;
    }

    /**
     * Get the total duration in weeks.
     * @return Weeks
     */
    public long getDurationWeeks() {
        return millis / WEEK_MILLIS;
    }

    /**
     * Get the total duration in months.
     * @return Months
     */
    public long getDurationMonths() {
        return millis / MONTH_MILLIS;
    }

    /**
     * Get the total duration in years.
     * @return Years
     */
    public long getDurationYears() {
        return millis / YEAR_MILLIS;
    }

    /**
     * Get the amount of milliseconds in this duration, between 0 and 999.
     * @return Milliseconds
     */
    public long milliseconds() {
        return millis % MILLISECONDS_PER_SECOND;
    }

    /**
     * Get the amount of seconds in this duration, between 0 and 59.
     * @return Seconds
     */
    public long seconds() {
        return (millis / SECOND_MILLIS) % SECONDS_PER_MINUTE;
    }

    /**
     * Get the amount of minutes in this duration, between 0 and 59.
     * @return Minutes
     */
    public long minutes() {
        return (millis / MINUTE_MILLIS) % MINUTES_PER_HOURS;
    }

    /**
     * Get the amount of hours in this duration, between 0 and 23.
     * @return Hours
     */
    public long hours() {
        return (millis / HOUR_MILLIS) % HOURS_PER_DAY;
    }

    /**
     * Get the amount of days in this duration, between 0 and 29.
     * @return Days
     */
    public long days() {
        return (millis / DAY_MILLIS) % DAYS_PER_MONTH;
    }

    /**
     * Get the amount of months in this duration, between 0 and 11.
     * @return Months
     */
    public long months() {
        return (millis / MONTH_MILLIS) % MONTHS_PER_YEAR;
    }

    /**
     * Get the amount of years in this duration.
     * @return Years
     */
    public long years() {
        return (millis / YEAR_MILLIS);
    }

    /**
     * Convert this duration into a readable string that can be used to create another duration.
     */
    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();

        if (years() > 0) {
            res.append(years()).append("y");
        }
        if (months() > 0) {
            res.append(months()).append("mo");
        }
        if (days() > 0) {
            res.append(days()).append("d");
        }
        if (hours() > 0) {
            res.append(hours()).append("h");
        }
        if (minutes() > 0) {
            res.append(minutes()).append("m");
        }
        if (seconds() > 0) {
            res.append(seconds()).append("s");
        }
        if (milliseconds() > 0) {
            res.append(milliseconds()).append("ms");
        }
        return res.toString();
    }
}
