package me.zodiakk.zapi.config.types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.clip.placeholderapi.PlaceholderAPI;
import me.zodiakk.zapi.ZApi;
import me.zodiakk.zapi.util.JsonUtil;
import net.milkbowl.vault.economy.Economy;

/**
 * Represents a single reward, which may be money, item or command.
 * <p>It can also have a chance.
 *
 * @author Zodiak
 * @since 4.1
 */
public class Reward {
    private static final Economy ECONOMY;

    private List<String> commandRewards;
    private List<ItemStack> itemRewards;
    private Double moneyReward;
    private Double chance;

    static {
        if (!ZApi.TEST_ENV) {
            ECONOMY = Bukkit.getServer().getServicesManager().getRegistration(Economy.class).getProvider();
        } else {
            ECONOMY = null;
        }
    }

    private Reward() {
        commandRewards = new ArrayList<String>();
        itemRewards = new ArrayList<ItemStack>();
        moneyReward = null;
        chance = 1.0d;
    }

    /**
     * Create a reward based on a JSON object representation.
     * @param object JSON object
     */
    public Reward(JsonObject object) {
        this();

        if (object.has("money")) {
            setMoney(object.get("money").getAsDouble());
        }
        if (object.has("items")) {
            for (JsonElement itemReward : object.get("items").getAsJsonArray()) {
                addItem(JsonUtil.itemStackFromJson(itemReward.getAsJsonObject()));
            }
        }
        if (object.has("commands")) {
            for (JsonElement commandReward : object.get("commands").getAsJsonArray()) {
                addCommand(commandReward.getAsString());
            }
        }
        if (object.has("chance")) {
            setChance(object.get("chance").getAsDouble());
        }
    }

    /**
     * Set the money reward amount.
     * @param money Money to give
     */
    public void setMoney(Double money) {
        moneyReward = money;
    }

    /**
     * Remove any money reward.
     */
    public void resetMoney() {
        moneyReward = null;
    }

    /**
     * Get the current money reward.
     * @return
     */
    public Double getMoney() {
        return moneyReward;
    }

    /**
     * Add a command to execute when rewarding the player.
     * @param command Command to execute
     */
    public void addCommand(String command) {
        commandRewards.add(command);
    }

    /**
     * Remove a command to execute when rewarding the player.
     * @param command Command to remove
     * @return True if the command was removed
     */
    public boolean removeCommand(String command) {
        return commandRewards.remove(command);
    }

    /**
     * Remove all commands from the command list.
     */
    public void resetCommands() {
        commandRewards.clear();
    }

    /**
     * Get the command list.
     * @return
     */
    public List<String> getCommands() {
        return commandRewards;
    }

    /**
     * Add an item to give to the player.
     * @param item Item to give
     */
    public void addItem(ItemStack item) {
        itemRewards.add(item);
    }

    /**
     * Remove an item to give to the player.
     * @param item Item to remove
     * @return True if the item was removed
     */
    public boolean removeItem(ItemStack item) {
        return itemRewards.remove(item);
    }

    /**
     * Remove all items from the item list.
     */
    public void resetItems() {
        itemRewards.clear();
    }

    /**
     * Get the items list.
     * @return The item list
     */
    public List<ItemStack> getItems() {
        return itemRewards;
    }

    /**
     * Set the chance of this reward being selected when used in a {@link RewardList}.
     * @param chance Chance
     */
    public void setChance(Double chance) {
        this.chance = chance;
    }

    /**
     * Get the chance of this reward being selected when used in a {@link RewardList}.
     * @return
     */
    public double getChance() {
        return chance;
    }

    /**
     * Give this reward to a {@link Player}.
     * @param player Player to give the reward to
     */
    public void rewardPlayer(Player player) {
        ZApi.getThreadsManager().execute(new Runnable() {
            @Override
            public void run() {
                // Money
                if (moneyReward != null) {
                    ECONOMY.depositPlayer(player, moneyReward);
                }

                // Items
                if (itemRewards.size() > 0) {
                    HashMap<Integer, ItemStack> remaining = player.getInventory().addItem(itemRewards.toArray(new ItemStack[itemRewards.size()]));

                    for (ItemStack remainingItem : remaining.values()) {
                        player.getLocation().getWorld().dropItemNaturally(player.getLocation(), remainingItem);
                    }
                }

                // Commands
                if (commandRewards.size() > 0) {
                    ArrayList<String> parsedCommands = new ArrayList<String>();
                    for (String command : commandRewards) {
                        parsedCommands.add(PlaceholderAPI.setPlaceholders(player, command));
                    }

                    ZApi.getThreadsManager().executeInMainThread(new Runnable() {
                        @Override
                        public void run() {
                            for (String command : parsedCommands) {
                                Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), command);
                            }
                        }
                    });
                }
            }
        });
    }
}
