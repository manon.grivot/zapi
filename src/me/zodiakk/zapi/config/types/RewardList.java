package me.zodiakk.zapi.config.types;

import java.util.HashSet;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import org.bukkit.entity.Player;

import me.zodiakk.zapi.util.RandomUtil;

/**
 * Represents a reward list, composed of {@link Reward}s.
 *
 * @author Zodiak
 * @since 4.1
 */
public class RewardList {
    private final HashSet<Reward> rewards;

    /**
     * Create a reward list based on a JSON array.
     * @param array JSON array
     */
    public RewardList(JsonArray array) {
        this.rewards = new HashSet<Reward>();

        for (JsonElement element : array) {
            rewards.add(new Reward(element.getAsJsonObject()));
        }
    }

    public HashSet<Reward> getList() {
        return rewards;
    }

    /**
     * Reward a {@link Player}, randomly selecting one reward out of all available.
     * @param player Player to reward
     * @return True if the player received anything, false if there was a problem
     */
    public boolean rewardPlayer(Player player) {
        Double randomizedValue = RandomUtil.getDouble();

        for (Reward reward : rewards) {
            randomizedValue -= reward.getChance();

            if (randomizedValue <= 0.0d) {
                reward.rewardPlayer(player);
                return true;
            }
        }
        return false;
    }
}
