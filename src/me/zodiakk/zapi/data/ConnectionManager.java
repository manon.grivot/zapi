package me.zodiakk.zapi.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import org.bukkit.Bukkit;

import me.zodiakk.zapi.ZApi;
import me.zodiakk.zapi.config.JsonConfigurationSection;

/**
 * Internal class.
 *
 * @since 1.0
 * @author Zodiak
 */
public class ConnectionManager {
    private static String route;
    private static Properties props;
    private static boolean isInitialized = false;
    private static ConnectionManager singleton;
    private static boolean errorsAreFatal = true;
    private static long timeoutPeriod;

    /**
     * Internal constructor.
     */
    public ConnectionManager() {
        if (isInitialized) {
            return;
        }
        JsonConfigurationSection config = ZApi.getConfiguration().getSection("database");

        isInitialized = true;
        errorsAreFatal = config.getBoolean("errorsAreFatal");
        timeoutPeriod = config.getDuration("antiTimeoutPeriod").getDurationMillis();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Bukkit.getLogger().info("Loaded JDBC driver from classpath.");
        } catch (ClassNotFoundException ex) {
            ZApi.postError(ex);
            Bukkit.getLogger().severe("Unable to load class com.mysql.jdbc.Driver from classpath. Database connections will not be available.");
            if (errorsAreFatal) {
                shutdownServer();
            }
            return;
        }

        String portPart = config.isNull("port") ? "" : ":" + config.getInteger("port");

        route = new String("jdbc:mysql://" + config.getString("ip") + portPart + "/" + config.getString("base") + "?autoReconnect=true&useUnicode=yes");
        props = new Properties();
        props.put("user", config.getString("user"));
        props.put("password", config.getString("password"));
        singleton = this;
    }

    protected static ConnectionManager getSingleton() {
        if (singleton == null) {
            singleton = new ConnectionManager();
        }
        return singleton;
    }

    protected Connection createConnection() throws SQLException {
        try {
            Connection conn = DriverManager.getConnection(route, props);
            Timer timer = new Timer();

            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    try {
                        if (conn == null || conn.isClosed()) {
                            timer.cancel();
                        }
                        Statement statement = conn.createStatement();

                        statement.executeQuery("SELECT 1;").close();
                        statement.close();
                    } catch (SQLException e) {
                        ZApi.postError(e);
                        timer.cancel();
                    }
                }
            }, timeoutPeriod, timeoutPeriod);
            return conn;
        } catch (SQLException ex) {
            ZApi.postError(ex);
            if (errorsAreFatal) {
                shutdownServer();
            }
            return null;
        }
    }

    protected Connection validateConnection(Connection conn) throws SQLException {
        if (!conn.isValid(30)) {
            return renewConnection(conn);
        }
        return conn;
    }

    protected Connection renewConnection(Connection conn) throws SQLException {
        if (!conn.isClosed()) {
            conn.close();
        }
        return conn = createConnection();
    }

    private static final void shutdownServer() {
        for (int i = 0; i < 10; i++) {
            Bukkit.getServer().getConsoleSender().sendMessage("Server will shutdown due to an SQL error.");
        }
        Bukkit.shutdown();
    }
}
