package me.zodiakk.zapi.data;

import me.zodiakk.zapi.ZApi;

/**
 * Get all data controllers. Available data controllers are:
 * <p>- TableManager: Data from a database.</p>
 *
 * @author Zodiak
 * @since 4.0
 */
public class DataManager {
    private TableManager tableManager;

    /**
     * Internal constructor.
     */
    public DataManager(ZApi api) {
        this.tableManager = new TableManager();
    }

    /**
     * Get the table manager.
     * @return The table manager
     */
    public TableManager getTableManager() {
        return tableManager;
    }
}
