package me.zodiakk.zapi.data;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException;

import me.zodiakk.zapi.data.table.TableModel;

/**
 * Manages all table models.
 *
 * @author Zodiak
 * @since 4.0
 */
public class TableManager {
    private Map<String, TableModel> tableModels;

    protected TableManager() {
        tableModels = new HashMap<String, TableModel>();
    }

    /**
     * Get a Table model.
     * @param name The name of the Table model
     * @return The table model, null if it doesn't exists
     */
    public TableModel getModel(String name) {
        return tableModels.get(name);
    }

    /**
     * Set a new Table model.
     * @param model The model
     * @throws SQLException If any SQL exception occurred while creating the table
     */
    public void setModel(TableModel model) throws SQLException {
        setModel(model, model.getName());
    }

    /**
     * Set a new Table model.
     * @param model The model
     * @param tableName The name of the table
     * @throws SQLException If any SQL exception occurred while creating the table
     */
    public void setModel(TableModel model, String tableName) throws SQLException {
        Connection connection = ConnectionManager.getSingleton().createConnection();
        model.setConnection(connection);

        Statement createStatement = connection.createStatement();
        createStatement.executeUpdate("CREATE TABLE IF NOT EXISTS `" + tableName
            + "` ( `__tmp` INT(10) NULL DEFAULT NULL ) ENGINE = InnoDB;");
        createStatement.close();

        // Remove tmp if needed
        Statement removeTempStatement = connection.createStatement();
        try {
            removeTempStatement.executeUpdate("ALTER TABLE `" + tableName + "` DROP `__tmp`");
        } catch (MySQLSyntaxErrorException ex) {
            // Skip error as it means that it was already removed before
        }
        removeTempStatement.close();

        tableModels.put(tableName, model);
    }
}
