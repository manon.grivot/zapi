package me.zodiakk.zapi.data.filter;

import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import me.zodiakk.zapi.data.query.Query;

/**
 * A fields filter is a filter that allows a SELECT statement to retrieve only the specified fields.
 * <p>If the fields filter has no field specified, it will have no effect and all the fields will be retrieved.</p>
 *
 * @author Zodiak
 * @since 4.0
 */
public class FieldsFilter implements Filter {
    Collection<String> fields;

    /**
     * Create a new fields filter with a provided list of fields to request.
     * @param fields A list of fields to request.
     */
    public FieldsFilter(List<String> fields) {
        this.fields = new HashSet<String>();
        for (String str : fields) {
            this.fields.add(str);
        }
    }

    /**
     * Create a new fields filter with a provided list of fields to request.
     * @param fields A list of fields to request.
     */
    public FieldsFilter(String... fields) {
        this.fields = new HashSet<String>();
        for (String str : fields) {
            this.fields.add(str);
        }
    }

    /**
     * Create a new fields filter with no fields.
     * <p>If you do not add fields later on, this filter will have no effect.</p>
     */
    public FieldsFilter() {
        this.fields = new HashSet<String>();
    }

    /**
     * Add a new field to this fields filter.
     * @param field The field to add
     */
    public void addField(String field) {
        this.fields.add(field);
    }

    /**
     * Remove a field from this fields filter.
     * @param field The field to remove
     * @return true if the field was in the filter
     */
    public boolean removeField(String field) {
        return this.fields.remove(field);
    }

    @Override
    public String getSqlDefinition() {
        if (fields.size() == 0) {
            return "*";
        } else {
            String[] fieldNames = new String[fields.size()];
            String[] fieldsArray = fields.toArray(new String[0]);

            for (int i = 0; i < fields.size(); i++) {
                fieldNames[i] = "`" + fieldsArray[i] + "`";
            }
            return String.join(",", fieldNames);
        }
    }

    @Override
    public Query setStatementParameters(Query query) throws SQLException {
        return query;
    }
}
