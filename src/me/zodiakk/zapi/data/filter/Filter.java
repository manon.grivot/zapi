package me.zodiakk.zapi.data.filter;

import java.sql.SQLException;

import me.zodiakk.zapi.data.query.Query;

/**
 * Provides a class model for all filters.
 *
 * @author Zodiak
 * @since 4.0
 */
public interface Filter {
    /**
     * Set parameters for a PreparedStatement following this filter.
     * @param query The Query being created
     * @return The Query with all parameters set
     * @throws SQLException Defined by {@link java.sql.PreparedStatement#setObject(int, Object)}
     */
    public Query setStatementParameters(Query query) throws SQLException;

    /**
     * Get the SQL definition of this field, that is to say, how this filter is represented in an SQL syntax.
     * @return The SQL definition of this field
     */
    public String getSqlDefinition();
}
