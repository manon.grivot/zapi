package me.zodiakk.zapi.data.filter;

import java.sql.SQLException;

import me.zodiakk.zapi.data.query.Query;

/**
 * A limit filter is a filter that limits the number of rows to retrieve from a SELECT statement.
 * <p>A limit filter can either have one or two parameters:<br>
 * - A {@code limit} parameter (required), that limits the number of rows that will be retrieved,<br>
 * - A {@code skip} parameter (optional), that skips a certain number of rows, the first x ones in the order that they are being stored (or sorted)<br>
 * </p>
 *
 * @author Zodiak
 * @since 4.0
 */
public class LimitFilter implements Filter {
    Integer limit;
    Integer skip;

    /**
     * Create a new limit filter.
     * @param limit The maximum number of rows to retrieve
     */
    public LimitFilter(Integer limit) {
        this.limit = limit;
        this.skip = null;
    }

    /**
     * Create a new limit filter.
     * @param skip The number of rows to skip
     * @param limit The maximum number of rows to retrieve
     */
    public LimitFilter(Integer skip, Integer limit) {
        this.limit = limit;
        this.skip = skip;
    }

    @Override
    public String getSqlDefinition() {
        if (skip == null) {
            return "LIMIT " + limit;
        } else {
            return "LIMIT " + skip + ", " + limit;
        }
    }

    @Override
    public Query setStatementParameters(Query query) throws SQLException {
        return query;
    }
}
