package me.zodiakk.zapi.data.filter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import me.zodiakk.zapi.data.query.Query;

/**
 * An order filter allows a SELECT statement to be sorted in a precise order.
 *
 * @author Zodiak
 * @since 4.0
 */
public class OrderFilter implements Filter {
    LinkedHashMap<String, OrderType> orders;

    /**
     * Create a new order filter with the specified list of sorting rules.
     * @param orders A list of sorting rules, with a field by which the results will be sorted, and the sort type (ASC/DESC)
     */
    public OrderFilter(LinkedHashMap<String, OrderType> orders) {
        this.orders = new LinkedHashMap<String, OrderType>();
        orders.forEach((field, type) -> this.orders.put(field, type));
    }

    /**
     * Create a new order filter with one sorting rule (more can be added later on).
     * @param field The field by which the results will be sorted
     * @param type The type of sort (ASC/DESC)
     */
    public OrderFilter(String field, OrderType type) {
        this.orders = new LinkedHashMap<String, OrderType>();
        orders.put(field, type);
    }

    /**
     * Create a new order filter with no values. Adding this filter to a request will have no effect unless you add sort rules later.
     */
    public OrderFilter() {
        this.orders = new LinkedHashMap<String, OrderType>();
    }

    /**
     * Add a new sorting rule.
     * @param field The field by which the results will be sorted
     * @param type The type of sort (ASC/DESC)
     */
    public void addOrder(String field, OrderType type) {
        this.orders.put(field, type);
    }

    /**
     * Add multiple sorting rules.
     * @param orders A list of sorting rules, with a field by which the results will be sorted, and the sort type (ASC/DESC)
     */
    public void addAllOrders(LinkedHashMap<String, OrderType> orders) {
        this.orders.putAll(orders);
    }

    @Override
    public Query setStatementParameters(Query query) throws SQLException {
        return query;
    }

    @Override
    public String getSqlDefinition() {
        if (orders.size() == 0) {
            return "";
        }
        ArrayList<String> rules = new ArrayList<String>(orders.size());
        orders.forEach((field, type) -> rules.add(new String("`" + field + "` " + type.toString())));
        return new String("ORDER BY " + String.join(", ", rules));
    }
}
