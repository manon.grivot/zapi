package me.zodiakk.zapi.data.filter;

/**
 * Sorting order.
 *
 * @author Zodiak
 * @since 4.0
 */
public enum OrderType {
    /**
     * Ascendant order (A to Z).
     */
    ASC,

    /**
     * Descendant order (Z to A).
     */
    DESC;
}
