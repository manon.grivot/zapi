package me.zodiakk.zapi.data.filter;

import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;

import me.zodiakk.zapi.data.query.Query;

/**
 * A where filter allows any data selection or filtering to only occurs on rows that validates that where filter.
 * <p>A where filter will specify possible value(s) for any type of data and with different verification methods.</p>
 * <p>The {@code WhereFilter} in itself will do nothing, it needs one or more {@link WhereOperator}.</p>
 *
 * @author Zodiak
 * @since 4.0
 */
public class WhereFilter implements Filter {
    protected static final HashSet<WhereOperatorType> NULL_VALUES;
    protected static final HashSet<WhereOperatorType> COMPARE_VALUES;
    protected static final HashSet<WhereOperatorType> BETWEEN_VALUES;
    protected static final HashSet<WhereOperatorType> LIST_VALUES;
    LinkedList<WhereOperator> operators;

    static {
        NULL_VALUES = new HashSet<WhereOperatorType>();
        NULL_VALUES.add(WhereOperatorType.NULL);
        NULL_VALUES.add(WhereOperatorType.NOTNULL);

        COMPARE_VALUES = new HashSet<WhereOperatorType>();
        COMPARE_VALUES.add(WhereOperatorType.EQ);
        COMPARE_VALUES.add(WhereOperatorType.NEQ);
        COMPARE_VALUES.add(WhereOperatorType.GT);
        COMPARE_VALUES.add(WhereOperatorType.GTE);
        COMPARE_VALUES.add(WhereOperatorType.LT);
        COMPARE_VALUES.add(WhereOperatorType.LTE);
        COMPARE_VALUES.add(WhereOperatorType.LIKE);
        COMPARE_VALUES.add(WhereOperatorType.NLIKE);
        COMPARE_VALUES.add(WhereOperatorType.REGEXP);
        COMPARE_VALUES.add(WhereOperatorType.NREGEXP);

        BETWEEN_VALUES = new HashSet<WhereOperatorType>();
        BETWEEN_VALUES.add(WhereOperatorType.IN);
        BETWEEN_VALUES.add(WhereOperatorType.NIN);

        LIST_VALUES = new HashSet<WhereOperatorType>();
        LIST_VALUES.add(WhereOperatorType.INQ);
        LIST_VALUES.add(WhereOperatorType.NINQ);
    }

    public WhereFilter() {
        this.operators = new LinkedList<WhereOperator>();
    }

    public WhereFilter(Collection<WhereOperator> operators) {
        this.operators = new LinkedList<WhereOperator>();
        operators.forEach(operator -> this.operators.add(operator));
    }

    public void addOperator(WhereOperator operator) {
        this.operators.add(operator);
    }

    public void addAllOperators(Collection<WhereOperator> operators) {
        operators.forEach(operator -> this.operators.add(operator));
    }

    @Override
    public String getSqlDefinition() {
        if (operators.size() == 0) {
            return "";
        }
        boolean and = false;
        StringBuilder definition = new StringBuilder("WHERE");
        for (WhereOperator operator : operators) {
            if (and) {
                definition.append(" AND");
            } else {
                and = true;
            }
            definition.append(" `" + operator.getField() + "` ").append(operator.getSqlDefinition());
        }
        return definition.toString();
    }

    @Override
    public Query setStatementParameters(Query query) throws SQLException {
        for (WhereOperator operator : operators) {
            if (NULL_VALUES.contains(operator.type)) {
                continue;
            }
            if (operator.getType().requiresArray()) {
                for (Object obj : operator.getValues()) {
                    query.statement.setObject(query.statementParametersCounter++, obj);
                }
            } else {
                query.statement.setObject(query.statementParametersCounter++, operator.getValue());
            }
        }
        return query;
    }
}
