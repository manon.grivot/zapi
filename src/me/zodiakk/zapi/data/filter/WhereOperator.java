package me.zodiakk.zapi.data.filter;

/**
 * A single where condition, that will be passed to a {@link WhereFilter}.
 * <p>
 * A where operator can have multiple forms, like equality, between two values,
 * matching a regex, etc..., however not all data types are compatible with all
 * operator types. See SQL specifications for more informations on
 * compatibility.
 * </p>
 *
 * @author Zodiak
 * @since 4.0
 */
public class WhereOperator {
    String field;
    WhereOperatorType type;
    Object value;
    Object[] values;

    /**
     * Create a new where operator. Use this constructor for the following operator types:<br>
     * <i>NULL</i>, <i>NOTNULL</i>.
     * @param field The field to check
     * @param type Where operator type
     */
    public WhereOperator(String field, WhereOperatorType type) {
        if (WhereFilter.NULL_VALUES.contains(type)) {
            this.field = field;
            this.type = type;
            this.value = null;
            this.values = null;
        } else {
            throw new IllegalStateException("Cannot instanciate a where operator of type " + type.toString() + " with a null value.");
        }
    }

    /**
     * Create a new where operator. Use this constructor for the following operator types:<br>
     * <i>EQ</i>, <i>NEQ</i>, <i>GT</i>, <i>GTE</i>, <i>LT</i>, <i>LTE</i>, <i>LIKE</i>, <i>NLIKE</i>, <i>REGEXPR</i>, <i>NREGEXPR</i>.
     * @param field The field to check
     * @param type Where operator type
     * @param value Where operator value
     */
    public WhereOperator(String field, WhereOperatorType type, Object value) {
        if (type.requiresArray() || WhereFilter.NULL_VALUES.contains(type)) {
            throw new IllegalStateException("Cannot instanciate a where operator of type " + type.toString() + " with a single value.");
        }
        this.field = field;
        this.value = value;
        this.type = type;
        this.values = null;
    }

    /**
     * Create a new where operator. Use this constructor for the following operator types:<br>
     * <i>IN</i>, <i>NIN</i>, <i>INQ</i>, <i>NINQ</i>.
     * @param field The field to check
     * @param type Where operator type
     * @param values Where operator values
     */
    public WhereOperator(String field, WhereOperatorType type, Object[] values) {
        if (!type.requiresArray()) {
            throw new IllegalStateException("Cannot instanciate a where operator of type " + type.toString() + " with multiple values.");
        } else if (type.arraySize() != -1 && values.length != type.arraySize()) {
            throw new IllegalStateException("Cannot instanciate a where operator of type " + type.toString() + " with anything else than "
                    + type.arraySize() + " values.");
        }
        this.field = field;
        this.values = values;
        this.type = type;
        this.value = null;
    }

    /**
     * Get the field that is affected by this operator.
     * @return The field name
     */
    public String getField() {
        return this.field;
    }

    /**
     * Get the operator type of this operator.
     * @return The operator type
     */
    public WhereOperatorType getType() {
        return this.type;
    }

    /**
     * Get the value of this where operator.
     * @return The value of this where operator. If value is an array, it will return the first value. If the operator is NULL or NOTNULL, will return null.
     */
    public Object getValue() {
        if (value != null) {
            return value;
        } else if (values != null) {
            return values[0];
        } else {
            return null;
        }
    }

    /**
     * Get values of this where operator.
     * @return Values of this where operator. If value is a single value, it will return an array containing only this value.
     *     If the operator is NULL or NOTNULL, will return null.
     */
    public Object[] getValues() {
        if (values != null) {
            return values;
        } else if (value != null) {
            Object[] array = { value };
            return array;
        } else {
            return null;
        }
    }

    protected String getSqlDefinition() {
        String args;
        if (WhereFilter.COMPARE_VALUES.contains(this.type)) {
            args = "?";
        } else if (WhereFilter.BETWEEN_VALUES.contains(this.type)) {
            args = "? AND ?";
        } else if (WhereFilter.LIST_VALUES.contains(this.type)) {
            String[] placeholders = new String[this.values.length];
            for (int i = 0; i < values.length; i++) {
                placeholders[i] = "?";
            }
            args = String.format("(%s)", String.join(",", placeholders));
        } else {
            args = "";
        }
        return String.format("%s %s", this.type.getSql(), args);
    }
}
