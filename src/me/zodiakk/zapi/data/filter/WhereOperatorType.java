package me.zodiakk.zapi.data.filter;

/**
 * Possible operator types.
 *
 * @author Zodiak
 * @since 4.0
 */
public enum WhereOperatorType {
    /**
     * Equals to.
     */
    EQ(false, "="),

    /**
     * Not equals to.
     */
    NEQ(false, "!="),

    /**
     * Greater than.
     */
    GT(false, ">"),

    /**
     * Greater than or equals to.
     */
    GTE(false, ">="),

    /**
     * Less than.
     */
    LT(false, "<"),

    /**
     * Less than or equals to.
     */
    LTE(false, "<="),

    /**
     * Between two values.
     */
    IN(true, 2, "BETWEEN"),

    /**
     * Not between two values.
     */
    NIN(true, 2, "NOT BETWEEN"),

    /**
     * In an array.
     */
    INQ(true, "IN"),

    /**
     * Not in an array.
     */
    NINQ(true, "NOT IN"),

    /**
     * SQL LIKE operator.
     */
    LIKE(false, "LIKE"),

    /**
     * SQL NOT LIKE operator.
     */
    NLIKE(false, "NOT LIKE"),

    /**
     * Matches a regular expression.
     */
    REGEXP(false, "REGEXP"),

    /**
     * Does not match a regular expression.
     */
    NREGEXP(false, "NOT REGEXP"),

    /**
     * NULL value.
     */
    NULL(false, "IS NULL"),

    /**
     * NOT NULL value.
     */
    NOTNULL(false, "IS NOT NULL");

    private boolean requiresArray;
    private int arraySize = -1;
    private String sql;

    WhereOperatorType(boolean requiresArray, String sql) {
        this.arraySize = -1;
        this.requiresArray = requiresArray;
        this.sql = sql;
    }

    WhereOperatorType(boolean requiresArray, int arraySize, String sql) {
        this.arraySize = arraySize;
        this.requiresArray = requiresArray;
        this.sql = sql;
    }

    protected boolean requiresArray() {
        return requiresArray;
    }

    protected int arraySize() {
        return arraySize;
    }

    protected String getSql() {
        return sql;
    }
}
