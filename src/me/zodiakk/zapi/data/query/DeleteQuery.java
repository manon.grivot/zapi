package me.zodiakk.zapi.data.query;

import java.sql.Connection;
import java.sql.SQLException;

import me.zodiakk.zapi.data.filter.WhereFilter;
import me.zodiakk.zapi.data.result.Result;

public class DeleteQuery extends Query {
    private WhereFilter where;

    public DeleteQuery(String table, Connection connection, WhereFilter where) throws SQLException, UnsafeQueryException {
        super(table, connection, where);

        this.where = where;

        StringBuilder builder = new StringBuilder("DELETE FROM `" + table + "`");

        String whereStr = where.getSqlDefinition();
        if (whereStr.equals("")) {
            throw new UnsafeQueryException("WHERE clause is empty!", this);
        }
        builder.append(";");

        this.statement = connection.prepareStatement(builder.toString());
    }

    @Override
    public void build() throws SQLException {
        this.where.setStatementParameters(this);
    }

    @Override
    public Result execute() throws SQLException {
        this.statement.executeUpdate();
        this.statement.close();

        return null;
    }
}
