package me.zodiakk.zapi.data.query;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;

import me.zodiakk.zapi.data.result.Result;
import me.zodiakk.zapi.data.table.TableRow;
import me.zodiakk.zapi.data.table.TableValue;

/**
 * An insert query is an implementation of the SQL INSERT statement. It will insert into the database a list of {@link me.zodiakk.zapi.data.table.TableRow}.
 *
 * @author Zodiak
 * @since 4.0
 */
public class InsertQuery extends Query {
    LinkedList<String> fieldOrder;
    TableRow[] rows;

    /**
     * Create a new insert query on the specified table, that will insert a list of {@link me.zodiakk.zapi.data.table.TableRow}.
     * @param table The table to insert values into
     * @param connection The connection to use
     * @param rows The list of rows to insert
     * @throws SQLException If any SQL exception occurs.
     */
    public InsertQuery(String table, Connection connection, TableRow... rows) throws SQLException {
        super(table, connection);

        if (rows == null || rows.length == 0) {
            throw new NullPointerException("List of rows to insert is null.");
        }

        fieldOrder = new LinkedList<String>();
        this.rows = rows;
        StringBuilder builder = new StringBuilder("INSERT INTO `" + this.table + "` (");

        int length = rows[0].getValues().size();
        String[] fieldList = new String[length];
        TableValue[] tableValues = rows[0].getValues().toArray(new TableValue[0]);

        for (int i = 0; i < length; i++) {
            String name = tableValues[i].getField().getName();

            fieldOrder.addLast(name);
            fieldList[i] = "`" + name + "`";
        }

        builder.append(String.join(",", fieldList));
        builder.append(") VALUES ");

        String[] qmList = new String[length];
        String[] valueList = new String[rows.length];

        for (int i = 0; i < length; i++) {
            qmList[i] = "?";
        }

        String qmString = "(" + String.join(",", qmList) + ")";

        for (int i = 0; i < rows.length; i++) {
            valueList[i] = qmString;
        }

        builder.append(String.join(",", valueList));
        builder.append(";");

        statement = connection.prepareStatement(builder.toString());
    }

    @Override
    public void build() throws SQLException {
        for (TableRow row : rows) {
            for (String field : fieldOrder) {
                statement.setObject(statementParametersCounter++, row.getValue(field).getValue());
            }
        }
    }

    @Override
    public Result execute() throws SQLException {
        statement.executeUpdate();
        return null;
    }
}
