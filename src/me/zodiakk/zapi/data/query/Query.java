package me.zodiakk.zapi.data.query;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import me.zodiakk.zapi.data.filter.Filter;
import me.zodiakk.zapi.data.result.Result;

/**
 * An abstract query model, that is to say an implemented SQL statement.
 *
 * @author Zodiak
 * @since 4.0
 */
public abstract class Query {
    public Integer statementParametersCounter;
    public PreparedStatement statement;
    ResultSet resultSet;
    public Filter[] filters;
    public String table;

    /**
     * Create an instance of this query on the specified table with a list of filters.
     * @param table The table on which the query will be executed
     * @param connection The connection to use
     * @param filters A list of filters for this query
     * @throws SQLException If any SQL exception occurs.
     */
    public Query(String table, Connection connection, Filter... filters) throws SQLException {
        this.statementParametersCounter = 1;
        this.filters = filters;
        this.table = table;
        this.resultSet = null;
    }

    public abstract void build() throws SQLException;

    public abstract Result execute() throws SQLException;

    public void close() throws SQLException {
        if (statement != null) {
            statement.close();
        }
        if (resultSet != null) {
            resultSet.close();
        }
    }

    public <T> T getFilter(Filter[] filters, Class<T> clazz) {
        for (Filter filter : filters) {
            if (clazz.isInstance(filter)) {
                return clazz.cast(filter);
            }
        }
        return null;
    }
}
