package me.zodiakk.zapi.data.query;

import java.sql.Connection;
import java.sql.SQLException;

import me.zodiakk.zapi.data.filter.FieldsFilter;
import me.zodiakk.zapi.data.filter.Filter;
import me.zodiakk.zapi.data.filter.LimitFilter;
import me.zodiakk.zapi.data.filter.OrderFilter;
import me.zodiakk.zapi.data.filter.WhereFilter;
import me.zodiakk.zapi.data.result.Result;

/**
 * A select query is an implementation of the SQL SELECT statement. It can take multiple filters in parameter, that will restrict and organize the data being
 * retrieved.
 *
 * @author Zodiak
 * @since 4.0
 */
public class SelectQuery extends Query {
    FieldsFilter fieldsFilter;
    LimitFilter limitFilter;
    OrderFilter orderFilter;
    WhereFilter whereFilter;

    /**
     * Create a new select query on the specified table, with a list of filters.
     * @param table The table the select will retrieve the data on
     * @param connection The connection that will be used
     * @param filters A list of filters for that select query
     * @throws SQLException If any SQL exception occurs.
     */
    public SelectQuery(String table, Connection connection, Filter... filters) throws SQLException {
        super(table, connection, filters);

        StringBuilder builder = new StringBuilder("SELECT ");

        // Fields filter
        fieldsFilter = this.getFilter(filters, FieldsFilter.class);
        if (fieldsFilter == null) {
            builder.append("*");
        } else {
            builder.append(fieldsFilter.getSqlDefinition());
        }

        builder.append(" FROM `" + this.table + "` ");

        // Where filter
        whereFilter = this.getFilter(filters, WhereFilter.class);
        if (whereFilter != null) {
            builder.append(whereFilter.getSqlDefinition());
        }

        // Order filter
        orderFilter = this.getFilter(filters, OrderFilter.class);
        if (orderFilter != null) {
            builder.append(orderFilter.getSqlDefinition());
        }

        // Limit filter
        limitFilter = this.getFilter(filters, LimitFilter.class);
        if (limitFilter != null) {
            builder.append(limitFilter.getSqlDefinition());
        }

        builder.append(";");

        // Prepared statement
        statement = connection.prepareStatement(builder.toString());
    }

    @Override
    public void build() throws SQLException {
        if (fieldsFilter != null) {
            fieldsFilter.setStatementParameters(this);
        }

        if (whereFilter != null) {
            whereFilter.setStatementParameters(this);
        }

        if (orderFilter != null) {
            orderFilter.setStatementParameters(this);
        }

        if (limitFilter != null) {
            limitFilter.setStatementParameters(this);
        }
    }

    @Override
    public Result execute() throws SQLException {
        this.resultSet = this.statement.executeQuery();

        return new Result(resultSet);
    }
}
