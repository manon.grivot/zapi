package me.zodiakk.zapi.data.query;

public class UnsafeQueryException extends Exception {
    private static final long serialVersionUID = 5924339379372484537L;
    private Query query;

    public UnsafeQueryException(String message, Query query) {
        super(message);
        this.query = query;
    }

    public Query getQuery() {
        return query;
    }
}
