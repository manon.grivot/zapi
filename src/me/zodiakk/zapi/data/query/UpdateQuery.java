package me.zodiakk.zapi.data.query;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import me.zodiakk.zapi.ZApi;
import me.zodiakk.zapi.data.filter.WhereFilter;
import me.zodiakk.zapi.data.result.Result;

public class UpdateQuery extends Query {
    WhereFilter where;
    LinkedHashMap<String, Object> values;

    public UpdateQuery(String table, Connection connection, WhereFilter where, LinkedHashMap<String, Object> values) throws SQLException {
        super(table, connection, where);

        this.where = where;
        this.values = values;

        StringBuilder builder = new StringBuilder("UPDATE `" + table + "` SET ");
        Collection<String> valuesDef = new ArrayList<String>();

        for (Entry<String, Object> entry : values.entrySet()) {
            valuesDef.add("`" + entry.getKey() + "` = ?");
        }

        builder.append(String.join(", ", valuesDef));
        builder.append(" ");
        builder.append(where.getSqlDefinition());
        builder.append(";");

        this.statement = connection.prepareStatement(builder.toString());
    }

    @Override
    public void build() throws SQLException {
        for (Entry<String, Object> entry : values.entrySet()) {
            try {
                statement.setObject(this.statementParametersCounter++, entry.getValue());
            } catch (SQLException ex) {
                ZApi.postError(ex);
            }
        }

        this.where.setStatementParameters(this);
    }

    @Override
    public Result execute() throws SQLException {
        statement.executeUpdate();
        statement.close();

        return null;
    }
}
