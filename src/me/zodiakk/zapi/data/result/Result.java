package me.zodiakk.zapi.data.result;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.function.Consumer;

import me.zodiakk.zapi.data.table.TableData;

/**
 * This class represents the results of a SELECT query.
 *
 * @author Zodiak
 * @since 4.0
 */
public class Result {
    private LinkedList<ResultRow> results;

    /**
     * Create a new result from a {@link ResultSet}.
     * @param resultSet The {@link ResultSet} to use
     * @throws SQLException If any SQL exception occurs.
     */
    public Result(ResultSet resultSet) throws SQLException {
        results = new LinkedList<ResultRow>();

        while (resultSet.next()) {
            results.add(new ResultRow(resultSet));
        }
    }

    /**
     * Get all the results.
     * @return A list of all the rows this select retrieved.
     */
    public LinkedList<ResultRow> getResults() {
        return results;
    }

    /**
     * Performs the given action on all the rows from this result.
     * @param consumer A lambda function to execute on all the rows
     */
    public void forEach(Consumer<? super ResultRow> consumer) {
        results.forEach(consumer);
    }

    /**
     * Transforms this result into a {@link TableData} that can be modified and passed to an update query.
     * @return A {@link TableData} from this result.
     */
    public TableData toTableData() {
        return new TableData(this);
    }
}
