package me.zodiakk.zapi.data.result;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.function.Consumer;

import me.zodiakk.zapi.ZApi;
import me.zodiakk.zapi.data.table.TableRow;

/**
 * A single row from a select result.
 *
 * @author Zodiak
 * @since 4.0
 */
public class ResultRow {
    private Collection<ResultValue> results;

    protected ResultRow(ResultSet resultSet) throws SQLException {
        ResultSetMetaData meta = resultSet.getMetaData();
        results = new HashSet<ResultValue>();

        for (int i = 1; i <= meta.getColumnCount(); i++) {
            results.add(
                new ResultValue(ZApi.getData().getTableManager().getModel(meta.getTableName(i)).getField(meta.getColumnName(i)),
                resultSet.getObject(meta.getColumnName(i)))
            );
        }
    }

    /**
     * Get all the values from this row.
     * @return All the values
     */
    public Collection<ResultValue> getValues() {
        return results;
    }

    /**
     * Get a specific value from this row.
     * @param field The field to retrieve
     * @return The value ofthis field.
     */
    public ResultValue getValue(String field) {
        for (ResultValue value : results) {
            if (value.getField().getName().equals(field)) {
                return value;
            }
        }
        return null;
    }

    /**
     * Perform the given action on all values of this row.
     * @param consumer A lambda function to execute on all values of this row
     */
    public void forEach(Consumer<? super ResultValue> consumer) {
        results.forEach(consumer);
    }

    /**
     * Transforms this result row into a {@link me.zodiakk.zapi.data.table.TableRow} that can be modified and passed to an update query.
     * @return A {@link me.zodiakk.zapi.data.table.TableRow} from this row.
     */
    public TableRow toTableRow() {
        return new TableRow(this);
    }
}
