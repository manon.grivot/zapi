package me.zodiakk.zapi.data.result;

import me.zodiakk.zapi.data.table.TableField;
import me.zodiakk.zapi.data.table.TableValue;

/**
 * A single value from a result of a select query.
 *
 * @author Zodiak
 * @since 4.0
 */
public class ResultValue {
    private final TableField<?> field;
    private final Object value;

    protected ResultValue(TableField<?> field, Object value) {
        this.field = field;
        this.value = value;
    }

    /**
     * Get the actual value.
     * @return The value.
     */
    public Object getValue() {
        return value;
    }

    /**
     * Get the field in which this value is.
     * @return The field.
     */
    public TableField<?> getField() {
        return field;
    }

    /**
     * Transforms this value into a {@link TableValue} that can be modified and passed to an update query.
     * @return A {@link TableValue} from this value.
     */
    public TableValue toTableValue() {
        return new TableValue(this);
    }
}
