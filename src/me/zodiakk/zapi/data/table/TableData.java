package me.zodiakk.zapi.data.table;

import java.util.Collection;
import java.util.HashSet;
import java.util.function.Consumer;

import me.zodiakk.zapi.data.result.Result;
import me.zodiakk.zapi.data.result.ResultRow;

/**
 * A table data object contains one or multiple rows from a table, that can be manipulated and later parsed to an update query.
 *
 * @author Zodiak
 * @since 4.0
 */
public class TableData {
    private Collection<TableRow> rows;

    public TableData(Result result) {
        rows = new HashSet<TableRow>();

        for (ResultRow resRow : result.getResults()) {
            rows.add(new TableRow(resRow));
        }
    }

    /**
     * Get a list of all {@link TableRow} contained in this table data.
     * @return The list
     */
    public Collection<TableRow> getRows() {
        return rows;
    }

    /**
     * Pass all values into a function.
     * @param consumer The function to which the values will be passed
     */
    public void forEach(Consumer<? super TableRow> consumer) {
        rows.forEach(consumer);
    }
}
