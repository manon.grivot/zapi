package me.zodiakk.zapi.data.table;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.function.Predicate;

/**
 * A table field represents one field from a table, with all its informations.
 * <p>A field is represented by different properties:<br>
 * - A <b>name</b>, that should comply with the snake case format,<br>
 * - A <b>type</b>, that can be of any SQL type (<i>INT</i>, <i>VARCHAR</i>, <i>DATE</i>, <i>BOOLEAN</i>,
 *     <i>ENUM</i>, <i>JSON</i>, ...) and that should fit the specified type parameter when instanciating this class,<br>
 * - An optional <b>type size</b>, for example a VARCHAR of size 256 can store up to 256 characters,<br>
 * - When a field is of type <i>ENUM</i> or <i>SET</i>, a list of <b>possible values</b> must be specified insteaf of the type size,<br>
 * - An optional <b>default value</b> for that field, that must be of the type parameter,<br>
 * - If you want to index this field, you can specify an <b>index type</b>, followed by an <b>index name</b> if the type is not <i>PRIMARY</i>,<br>
 * - When a field is indexed, you can also add <b>auto incrementation</b> to this field,<br>
 * - A <b>data validator</b>, that will disallow any data to be inserted or updated into the table if it doesn't pass this validation.</p>
 * <p>Not all parameters are inter-compatible, and thus all of these situation cannot happens and will throw exceptions:<br>
 * - Index of type PRIMARY with an index name, or index not of type PRIMARY without index name,<br>
 * - Non-index field that has auto incrementation,<br>
 * - Field of type ENUM or SET without a list of values, or field not of type ENUM or SET with a list of values,<br>
 * - NULL default value on a not nullable field.</p>
 * @param <T> The Java type corresponding to the SQL type of this field
 *
 * @author Zodiak
 * @since 4.0
 */
public class TableField<T> {
    String name;
    FieldType type;
    Integer size;
    Collection<String> values;
    DefaultValue<T> defaultValue;
    IndexType index;
    String indexName;
    Boolean nullable;
    Boolean autoIncrement;
    Predicate<T> validator;

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type) {
        this(name, type, null, null, null, null, null, true, false, null);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Integer size) {
        this(name, type, size, null, null, null, null, true, false, null);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Collection<String> values) {
        this(name, type, null, values, null, null, null, true, false, null);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, DefaultValue<T> defaultValue) {
        this(name, type, null, null, defaultValue, null, null, true, false, null);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Integer size, DefaultValue<T> defaultValue) {
        this(name, type, size, null, defaultValue, null, null, true, false, null);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Collection<String> values, DefaultValue<T> defaultValue) {
        this(name, type, null, values, defaultValue, null, null, true, false, null);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, IndexType index) {
        this(name, type, null, null, null, index, null, false, false, null);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Integer size, IndexType index) {
        this(name, type, size, null, null, index, null, false, false, null);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Collection<String> values, IndexType index) {
        this(name, type, null, values, null, index, null, false, false, null);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, IndexType index, String indexName) {
        this(name, type, null, null, null, index, indexName, false, false, null);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Integer size, IndexType index, String indexName) {
        this(name, type, size, null, null, index, indexName, false, false, null);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Collection<String> values, IndexType index, String indexName) {
        this(name, type, null, values, null, index, indexName, false, false, null);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Boolean nullable) {
        this(name, type, null, null, null, null, null, nullable, false, null);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Integer size, Boolean nullable) {
        this(name, type, size, null, null, null, null, nullable, false, null);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Collection<String> values, Boolean nullable) {
        this(name, type, null, values, null, null, null, nullable, false, null);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, IndexType index, Boolean autoIncrement) {
        this(name, type, null, null, null, index, null, false, autoIncrement, null);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Integer size, IndexType index, Boolean autoIncrement) {
        this(name, type, size, null, null, index, null, false, autoIncrement, null);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Collection<String> values, IndexType index, Boolean autoIncrement) {
        this(name, type, null, values, null, index, null, false, autoIncrement, null);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, IndexType index, String indexName, Boolean autoIncrement) {
        this(name, type, null, null, null, index, indexName, false, autoIncrement, null);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Integer size, IndexType index, String indexName, Boolean autoIncrement) {
        this(name, type, size, null, null, index, indexName, false, autoIncrement, null);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Collection<String> values, IndexType index, String indexName, Boolean autoIncrement) {
        this(name, type, null, values, null, index, indexName, false, autoIncrement, null);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Predicate<T> validator) {
        this(name, type, null, null, null, null, null, true, false, validator);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Integer size, Predicate<T> validator) {
        this(name, type, size, null, null, null, null, true, false, validator);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Collection<String> values, Predicate<T> validator) {
        this(name, type, null, values, null, null, null, true, false, validator);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, DefaultValue<T> defaultValue, Predicate<T> validator) {
        this(name, type, null, null, defaultValue, null, null, true, false, validator);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Integer size, DefaultValue<T> defaultValue, Predicate<T> validator) {
        this(name, type, size, null, defaultValue, null, null, true, false, validator);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Collection<String> values, DefaultValue<T> defaultValue, Predicate<T> validator) {
        this(name, type, null, values, defaultValue, null, null, true, false, validator);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, IndexType index, Predicate<T> validator) {
        this(name, type, null, null, null, index, null, false, false, validator);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Integer size, IndexType index, Predicate<T> validator) {
        this(name, type, size, null, null, index, null, false, false, validator);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Collection<String> values, IndexType index, Predicate<T> validator) {
        this(name, type, null, values, null, index, null, false, false, validator);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, IndexType index, String indexName, Predicate<T> validator) {
        this(name, type, null, null, null, index, indexName, false, false, validator);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Integer size, IndexType index, String indexName, Predicate<T> validator) {
        this(name, type, size, null, null, index, indexName, false, false, validator);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Collection<String> values, IndexType index, String indexName, Predicate<T> validator) {
        this(name, type, null, values, null, index, indexName, false, false, validator);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Boolean nullable, Predicate<T> validator) {
        this(name, type, null, null, null, null, null, nullable, false, validator);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Integer size, Boolean nullable, Predicate<T> validator) {
        this(name, type, size, null, null, null, null, nullable, false, validator);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Collection<String> values, Boolean nullable, Predicate<T> validator) {
        this(name, type, null, values, null, null, null, nullable, false, validator);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, IndexType index, Boolean autoIncrement, Predicate<T> validator) {
        this(name, type, null, null, null, index, null, false, autoIncrement, validator);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Integer size, IndexType index, Boolean autoIncrement, Predicate<T> validator) {
        this(name, type, size, null, null, index, null, false, autoIncrement, validator);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Collection<String> values, IndexType index, Boolean autoIncrement, Predicate<T> validator) {
        this(name, type, null, values, null, index, null, false, autoIncrement, validator);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, IndexType index, String indexName, Boolean autoIncrement, Predicate<T> validator) {
        this(name, type, null, null, null, index, indexName, false, autoIncrement, validator);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Integer size, IndexType index, String indexName, Boolean autoIncrement, Predicate<T> validator) {
        this(name, type, size, null, null, index, indexName, false, autoIncrement, validator);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that fits what you really need.<br>
     * Advanced documentation can be found here:
     * {@linkplain TableField#TableField(String, FieldType, Integer, Collection, DefaultValue, IndexType, String, Boolean, Boolean, Predicate)}
     */
    public TableField(String name, FieldType type, Collection<String> values, IndexType index, String indexName, Boolean autoIncrement, Predicate<T> validator) {
        this(name, type, null, values, null, index, indexName, false, autoIncrement, validator);
    }

    /**
     * Define a new field with the specified parameters. Parameters are not always inter-compatible,
     * you should use the constructor that best fits what you really need.<br><br>
     * A field is represented by different properties:<br>
     * - A <b>name</b>, that should comply with the snake case format,<br>
     * - A <b>type</b>, that can be of any SQL type (<i>INT</i>, <i>VARCHAR</i>, <i>DATE</i>, <i>BOOLEAN</i>,
     *     <i>ENUM</i>, <i>JSON</i>, ...) and that should fit the specified type parameter when instanciating this class,<br>
     * - An optional <b>type size</b>, for example a VARCHAR of size 256 can store up to 256 characters,<br>
     * - When a field is of type <i>ENUM</i> or <i>SET</i>, a list of <b>possible values</b> must be specified insteaf of the type size,<br>
     * - An optional <b>default value</b> for that field, that must be of the type parameter,<br>
     * - If you want to index this field, you can specify an <b>index type</b>, followed by an <b>index name</b> if the type is not <i>PRIMARY</i>,<br>
     * - When a field is indexed, you can also add <b>auto incrementation</b> to this field,<br>
     * - A <b>data validator</b>, that will disallow any data to be inserted or updated into the table if it doesn't pass this validation.
     * @param name The name of the field
     * @param type The type of the field
     * @param size The size of the field
     * @param values Possible values for SET and ENUM fields
     * @param defaultValue Default value of this field, may be NULL, CURRENT_TIMESTAMP or DEFINED with a specified value
     * @param index The index type of the field
     * @param indexName The name of this index, for any index other than PRIMARY
     * @param nullable Whether if this field can contains NULL values
     * @param autoIncrement Whether if this field has auto incrementation of the value
     * @param validator A lambda function (of type Predicate) that takes a value of this field, and return true if this value is valid, false elsewise
     * @throws IllegalStateException If two parameters are incompatible:<br>
     *     - Index of type PRIMARY with an index name, or index not of type PRIMARY without index name,<br>
     *     - Non-index field that has auto incrementation,<br>
     *     - Field of type ENUM or SET without a list of values, or field not of type ENUM or SET with a list of values,<br>
     *     - NULL default value on a not nullable field.
     */
    public TableField(String name, FieldType type, Integer size, Collection<String> values, DefaultValue<T> defaultValue, IndexType index,
            String indexName, Boolean nullable, Boolean autoIncrement, Predicate<T> validator) {
        if (index != null) {
            if (index.equals(IndexType.PRIMARY) && indexName != null) {
                throw new IllegalStateException("Indexes of type PRIMARY KEY cannot be named.");
            } else if (!index.equals(IndexType.PRIMARY) && indexName == null) {
                throw new IllegalStateException("Indexes of type other than PRIMARY KEY must be named.");
            }
        }
        if (autoIncrement != null && autoIncrement == true && index == null) {
            throw new IllegalStateException("Cannot have auto incrementation on a non-index field.");
        }
        if (Arrays.asList(FieldType.ENUM, FieldType.SET).contains(type) && values == null) {
            throw new IllegalStateException("Cannot have fields of type ENUM or SET without a list of values.");
        } else if (!Arrays.asList(FieldType.ENUM, FieldType.SET).contains(type) && values != null) {
            throw new IllegalStateException("Cannot have fields of type other than ENUM or SET with a list of values.");
        }
        this.name = name;
        this.type = type;
        this.size = size;
        this.values = values;
        this.defaultValue = defaultValue;
        this.index = index;
        this.indexName = indexName;
        this.nullable = nullable;
        this.autoIncrement = autoIncrement;
        this.validator = validator;
    }

    public String getName() {
        return this.name;
    }

    public FieldType getType() {
        return this.type;
    }

    public Integer getTypeSize() {
        return this.size;
    }

    public Collection<String> getTypeValues() {
        return this.values;
    }

    public DefaultValue<T> getDefaultValue() {
        return this.defaultValue;
    }

    public IndexType getIndexType() {
        return this.index;
    }

    public String getIndexName() {
        return this.indexName;
    }

    public Boolean isNullable() {
        return this.nullable;
    }

    public Boolean isAutoIncrement() {
        return this.autoIncrement;
    }

    public void createRequest(Connection connection, String table) throws SQLException {
        String sizeOrValue;
        if (this.size != null) {
            sizeOrValue = String.format("(%s)", this.size.toString());
        } else if (this.values != null) {
            sizeOrValue = String.format("(%s)", String.join(",", this.values));
        } else {
            sizeOrValue = "";
        }

        String indexStr;
        if (this.index == null) {
            indexStr = "";
        } else {
            indexStr = String.format(", ADD %s %s (`%s`)", this.index.toString(), this.index.equals(IndexType.PRIMARY) ? "KEY" :
                    String.format("`%s`", this.indexName), this.name);
        }

        String sql = String.format("ALTER TABLE `%s` ADD `%s` %s%s %s %s %s%s;",
                table,
                this.name,
                this.type.toString(),
                sizeOrValue,
                this.nullable != null && !this.nullable ? "NOT NULL" : "NULL",
                this.defaultValue != null ? defaultValue.getSqlDefinition() : null,
                this.autoIncrement != null && this.autoIncrement ? "AUTO_INCREMENT" : "",
                indexStr);
        PreparedStatement statement = connection.prepareStatement(sql);
        if (defaultValue != null && defaultValue.isDefined()) {
            statement.setObject(1, defaultValue.getValue());
        }
        try {
            statement.executeUpdate();
        } catch (SQLException ex) {
            if (ex.getErrorCode() != 1060) {
                throw ex;
            }
        } finally {
            if (statement != null) {
                statement.close();
            }
        }
    }

    /**
     * Perform a validation on this field for a specific value.
     * @param value The value to validate
     * @return True if this value is valid, false elsewise
     */
    @SuppressWarnings({"unchecked", "unused"})
    public boolean validate(Object value) {
        if (value == null && nullable == false && defaultValue == null) {
            return false;
        }
        try {
            if (validator == null) {
                Object tmp = (T) value; // Same as value instanceof T (but you cannot do that because T doesn't exist at runtime)
                tmp = null;
                return true;
            } else {
                return validator.test((T) value);
            }
        } catch (ClassCastException ex) {
            return false;
        }
    }

    /**
     * This class represents a default value for a field. A default value can be of multiple forms:<br>
     * - <b>NULL</b>, which means NULL values will be the default values for this field,<br>
     * - <b>CURRENT_TIMESTAMP</b>, which means that the current timestamp will be inserted on this field when a new row is created,<br>
     * - <b>DEFINED</b>, which means a defined value will be the default value.<br>
     * A type parameter is not mandatory in case of a NULL default value. When using the CURRENT_TIMESTAMP type, a type of {@link java.util.Date}
     * is recommended, altough not mandatory.
     * @author Zodiak
     * @param <T> The Java type of this default value.
     */
    public static class DefaultValue<T> {
        private T value;
        private DefaultValueType type;

        /**
         * Define a new default value of the type NULL or CURRENT_TIMESTAMP. Do not use this constructor to create a DEFINED default value.
         * @param type The type of this default value.
         * @throws IllegalStateException In case of a DEFINED default value being instanciated.
         */
        public DefaultValue(DefaultValueType type) {
            if (type.equals(DefaultValueType.DEFINED)) {
                throw new IllegalStateException("Cannot create a new default value with the DEFINED type without an actual value.");
            }
            this.type = type;
            this.value = null;
        }

        /**
         * Define a new default value of the type DEFINED, with the specified value.
         * @param value The actual value of this default value
         */
        public DefaultValue(T value) {
            this.type = DefaultValueType.DEFINED;
            this.value = value;
        }

        protected boolean isDefined() {
            return type.equals(DefaultValueType.DEFINED);
        }

        private String getSqlDefinition() {
            if (type.equals(DefaultValueType.DEFINED)) {
                return new String("DEFAULT ?");
            } else {
                return String.format("DEFAULT %s", type.toString());
            }
        }

        /**
         * Get the value of this default value.
         * @return The actual value of this default value. If this default value is not of the type DEFINED, it will return null.
         */
        public Object getValue() {
            return value;
        }

        /**
         * Get the default value type.
         * @return The default value type
         */
        public DefaultValueType getType() {
            return type;
        }
    }

    public enum DefaultValueType {
        DEFINED,
        NULL,
        CURRENT_TIMESTAMP,
    }

    public enum IndexType {
        PRIMARY,
        UNIQUE,
        INDEX,
        FULLTEXT,
        SPACIAL;
    }

    public enum FieldType {
        TINYINT,
        SMALLINT,
        MEDIUMINT,
        INT,
        BIGINT,
        DECIMAL,
        FLOAT,
        DOUBLE,
        REAL,
        BIT,
        BOOLEAN,
        SERIAL,
        DATE,
        DATETIME,
        TIMESTAMP,
        TIME,
        YEAR,
        CHAR,
        VARCHAR,
        TINYTEXT,
        TEXT,
        MEDIUMTEXT,
        LONGTEXT,
        BINARY,
        VARBINARY,
        TINYBLOB,
        MEDIUMBLOB,
        BLOB,
        LONGBLOB,
        ENUM,
        SET,
        GEOMETRY,
        POINT,
        LINESTRING,
        POLYGON,
        MULTIPOINT,
        MULTILINESTRING,
        MULTIPOLYGON,
        GEOMETRYCOLLECTION,
        JSON;
    }
}
