package me.zodiakk.zapi.data.table;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException;

import me.zodiakk.zapi.ZApi;
import me.zodiakk.zapi.data.filter.Filter;
import me.zodiakk.zapi.data.filter.WhereFilter;
import me.zodiakk.zapi.data.filter.WhereOperator;
import me.zodiakk.zapi.data.filter.WhereOperatorType;
import me.zodiakk.zapi.data.query.DeleteQuery;
import me.zodiakk.zapi.data.query.InsertQuery;
import me.zodiakk.zapi.data.query.SelectQuery;
import me.zodiakk.zapi.data.query.UnsafeQueryException;
import me.zodiakk.zapi.data.query.UpdateQuery;
import me.zodiakk.zapi.data.result.Result;
import me.zodiakk.zapi.data.result.ResultRow;
import me.zodiakk.zapi.data.table.TableField.IndexType;

/**
 * A table model represents the structure of a table.
 *
 * @author Zodiak
 * @since 4.0
 */
public class TableModel {
    private String tableName;
    private Map<String, TableField<?>> fields;
    private Connection connection;

    /**
     * Create a new Table Model with the specified fields.
     * @param fields The fields of this table
     * @param tableName The name of the table
     */
    public TableModel(Map<String, TableField<?>> fields, String tableName) {
        this.tableName = tableName;
        this.fields = new HashMap<String, TableField<?>>();
        fields.forEach((name, field) -> this.fields.put(name, field));
    }

    /**
     * Create a new Table Model with the specified fields.
     * @param fields The fields of this table
     * @param tableName The name of the table
     */
    public TableModel(Collection<TableField<?>> fields, String tableName) {
        this.tableName = tableName;
        this.fields = new HashMap<String, TableField<?>>();
        fields.forEach((field) -> this.fields.put(field.getName(), field));
    }

    /**
     * Create a new Table Model without any fields. Fields must be created after
     * creating the model.
     * @param tableName The name of the table
     */
    public TableModel(String tableName) {
        this.tableName = tableName;
        this.fields = new HashMap<String, TableField<?>>();
    }

    /**
     * Add a new field to this table.
     * @param name The name of this field
     * @param field The field to add
     */
    public void addField(String name, TableField<?> field) {
        try {
            field.createRequest(getConnection(), getName());
            this.fields.put(name, field);

            if (fields.size() == 1) {
                // Remove tmp if needed
                Statement removeTempStatement = getConnection().createStatement();
                try {
                    removeTempStatement.executeUpdate("ALTER TABLE `" + getName() + "` DROP `__tmp`");
                } catch (MySQLSyntaxErrorException ex) {
                    // Skip error as it means that it was already removed before
                }
                removeTempStatement.close();
            }
        } catch (SQLException ex) {
            ZApi.postError(ex);
        }
    }

    /**
     * Add a new field to this table.
     * @param field The field to add
     */
    public void addField(TableField<?> field) {
        addField(field.getName(), field);
    }

    /**
     * Add all fields from a map of fields.
     * @param fields The fields to add
     */
    public void addAllFields(Map<String, TableField<?>> fields) {
        fields.forEach((name, field) -> addField(name, field));
    }

    /**
     * Add all fields from a list of fields.
     * @param fields The fields to add
     */
    public void addAllFields(Collection<TableField<?>> fields) {
        fields.forEach(field -> addField(field.getName(), field));
    }

    public TableField<?> getPrimaryKey() {
        for (TableField<?> field : fields.values()) {
            if (field.getIndexType() != null && field.getIndexType().equals(IndexType.PRIMARY)) {
                return field;
            }
        }
        return null;
    }

    /**
     * Get a field from its name.
     * @param name The name of the field
     * @return The field with the specified name, {@code null} if there is no field with this name
     */
    public TableField<?> getField(String name) {
        return fields.get(name);
    }

    /**
     * Get all fields from this table model.
     * @return The list of fields
     */
    public Collection<TableField<?>> getFields() {
        return fields.values();
    }

    /**
     * Get a new row that can be parsed to an insert request.
     * @return A new row for this table
     * @throws IllegalStateException If the key of this table is not auto-incrementing (and thus should have a proper value)
     */
    public TableRow getNewRow() throws IllegalStateException {
        if (!this.getPrimaryKey().isAutoIncrement()) {
            throw new IllegalStateException("No key was specified on a table with non auto-incrementing primary key");
        }
        return getNewRow(null);
    }

    /**
     * Get a new row with the specified values that can be parsed to an insert request.
     * @param values The values in this table
     * @return A new row for this table
     * @throws IllegalStateException If the key of this table is not auto-incrementing (and thus should have a proper value)
     */
    public TableRow getNewRow(Map<String, Object> values) throws IllegalStateException {
        if (!this.getPrimaryKey().isAutoIncrement()) {
            throw new IllegalStateException("No key was specified on a table with non auto-incrementing primary key");
        }
        if (values == null) {
            return new TableRow(this);
        } else {
            TableRow row = new TableRow(this);

            for (Entry<String, Object> value : values.entrySet()) {
                row.setValue(value.getKey(), value.getValue());
            }
            return row;
        }
    }

    /**
     * Get a new row that can be parsed to an insert request.
     * @param key The key of this row
     * @return A new row for this table
     * @throws IllegalStateException If the key of this table is auto-incrementing (and thus should not have a proper value)
     */
    public TableRow getNewRow(Object key) throws IllegalStateException {
        if (this.getPrimaryKey().isAutoIncrement()) {
            throw new IllegalStateException("A key was specified on a table with non auto-incrementing primary key");
        }
        return getNewRow(key, null);
    }

    /**
     * Get a new row with the specified values that can be parsed to an insert request.
     * @param key The key of this row
     * @param values The values in this table
     * @return A new row for this table
     * @throws IllegalStateException If the key of this table is auto-incrementing (and thus should not have a proper value)
     */
    public TableRow getNewRow(Object key, Map<String, Object> values) throws IllegalStateException {
        if (this.getPrimaryKey().isAutoIncrement()) {
            throw new IllegalStateException("A key was specified on a table with non auto-incrementing primary key");
        }
        TableRow row = new TableRow(this);
        row.setKey(key);
        if (values == null) {
            return row;
        } else {
            for (Entry<String, Object> value : values.entrySet()) {
                row.setValue(value.getKey(), value.getValue());
            }
            return row;
        }
    }

    /**
     * Get the table name.
     * @return The table name
     */
    public String getName() {
        return tableName;
    }

    /**
     * Set the connection that will be used to create SQL requests.
     * @param connection The connection to use
     */
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    /**
     * Get the SQL connection to create SQL requests.
     * @return The connection
     */
    public Connection getConnection() {
        return this.connection;
    }

    /**
     * Select values with the specified filters.
     * @param filters The filters that will restrict the data being retrieved
     * @return The results of this select
     * @throws SQLException If any SQL exception occurs
     */
    public Result select(Filter... filters) throws SQLException {
        SelectQuery query = new SelectQuery(getName(), getConnection(), filters);
        query.build();
        Result result = query.execute();
        query.close();
        return result;
    }

    /**
     * Select one row identified by this key (with the table primary key).
     * @param key The key of the row to select
     * @return The row identified by this key - returns {@code null} if there is no row with that key
     * @throws SQLException If any SQL exception occurs
     */
    public ResultRow selectOne(Object key) throws SQLException {
        WhereFilter where = new WhereFilter();
        where.addOperator(new WhereOperator(getPrimaryKey().getName(), WhereOperatorType.EQ, key));
        ResultRow row;
        try {
            row = this.select(where).getResults().get(0);
        } catch (IndexOutOfBoundsException ex) {
            return null;
        }
        return row;
    }

    /**
     * Insert new rows into the table. Primary key will be set automatically if it is auto-incrementing.
     * @param newData Rows to be inserted
     * @throws SQLException If any SQL exception occurs
     */
    public void insert(TableData newData) throws SQLException {
        this.insert(newData.getRows().toArray(new TableRow[newData.getRows().size()]));
    }

    /**
     * Insert new rows into the table. Primary key will be set automatically if it is auto-incrementing.
     * @param newRows Rows to be inserted
     * @throws SQLException If any SQL exception occurs
     */
    public void insert(TableRow... newRows) throws SQLException {
        InsertQuery query = new InsertQuery(getName(), getConnection(), newRows);
        query.build();
        query.execute();
        query.close();
    }

    /**
     * Update existing rows with the specified new data.
     * @param data The new data
     * @throws SQLException If any SQL exception occurs
     */
    public void updateRows(TableData data) throws SQLException {
        this.updateRows(data.getRows().toArray(new TableRow[data.getRows().size()]));
    }

    /**
     * Update existing rows with the specified new data.
     * @param rows The new data
     * @throws SQLException If any SQL exception occurs
     */
    public boolean updateRows(TableRow... rows) throws SQLException {
        String primaryKey = getPrimaryKey().getName();

        for (TableRow row : rows) {
            WhereFilter where = new WhereFilter();
            where.addOperator(new WhereOperator(primaryKey, WhereOperatorType.EQ, row.getValue(primaryKey).getValue()));
            LinkedHashMap<String, Object> values = new LinkedHashMap<String, Object>();
            row.forEach(value -> {
                if (!value.getField().getName().equals(primaryKey)) {
                    values.put(value.getField().getName(), value.getValue());
                }
            });
            updateWhere(where, values);
        }
        return true;
    }

    /**
     * Update matching rows with the specified new data.
     * @param where Where condition to match rows
     * @param values New values
     * @throws SQLException If any SQL exception occurs
     */
    public boolean updateWhere(WhereFilter where, LinkedHashMap<String, Object> values) throws SQLException {
        // Vérification
        for (Entry<String, Object> entry : values.entrySet()) {
            if (!this.getField(entry.getKey()).validate(entry.getValue())) {
                return false;
            }
        }
        UpdateQuery update = new UpdateQuery(getName(), getConnection(), where, values);
        update.build();
        update.execute();
        update.close();
        return true;
    }

    /**
     * Delete specified rows.
     * @param data Rows to delete
     * @throws UnsafeQueryException If the where filter is empty
     * @throws SQLException If any SQL exception occurs
     */
    public void deleteRows(TableData data) throws UnsafeQueryException, SQLException {
        this.deleteRows(data.getRows().toArray(new TableRow[data.getRows().size()]));
    }

    /**
     * Delete specified rows.
     * @param rows Rows to delete
     * @throws UnsafeQueryException If the where filter is empty
     * @throws SQLException If any SQL exception occurs
     */
    public void deleteRows(TableRow... rows) throws UnsafeQueryException, SQLException {
        String primaryKey = getPrimaryKey().getName();

        for (TableRow row : rows) {
            WhereFilter where = new WhereFilter();
            where.addOperator(new WhereOperator(primaryKey, WhereOperatorType.EQ, row.getValue(primaryKey)));
            this.deleteWhere(where);
        }
    }

    /**
     * Delete matching rows.
     * @param where Where condition to match rows
     * @throws UnsafeQueryException If the where filter is empty
     * @throws SQLException If any SQL exception occurs
     */
    public void deleteWhere(WhereFilter where) throws UnsafeQueryException, SQLException {
        DeleteQuery query = new DeleteQuery(getName(), getConnection(), where);
        query.build();
        query.execute();
        query.close();
    }
}
