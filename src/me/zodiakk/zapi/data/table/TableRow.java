package me.zodiakk.zapi.data.table;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.function.Consumer;
import java.util.function.Function;

import me.zodiakk.zapi.data.result.ResultRow;
import me.zodiakk.zapi.data.result.ResultValue;
import me.zodiakk.zapi.data.table.TableField.IndexType;

/**
 * Represents a row from a table, with all its data. It can be passed to an update query to update the row.
 *
 * @author Zodiak
 * @since 4.0
 */
public class TableRow {
    private Collection<TableValue> values;

    /**
     * Get a table row from a result row.
     * @param result The result row
     */
    public TableRow(ResultRow result) {
        values = new HashSet<TableValue>();

        for (ResultValue value : result.getValues()) {
            values.add(new TableValue(value));
        }
    }

    /**
     * Get a new table row from its model.
     * @param model The table model
     */
    public TableRow(TableModel model) {
        values = new ArrayList<TableValue>();

        for (TableField<?> field : model.getFields()) {
            values.add(new TableValue(field));
        }
    }

    /**
     * Get all values of this row.
     * @return The values
     */
    public Collection<TableValue> getValues() {
        return values;
    }

    /**
     * Set one value on this row.
     * @param field The field on which the value will be set
     * @param value The value to set
     * @return false if the value did not pass the validation or if the field does not exists, true elsewise
     */
    public boolean setValue(String field, Object value) {
        TableValue tableValue = getValue(field);

        if (tableValue == null) {
            return false;
        }
        return tableValue.setValue(value);
    }

    /**
     * Set one value on this row.
     * @param <T> Java type of the field
     * @param field The field on which the value will be set
     * @param value The value to set
     * @return {@code false} if the value did not pass the validation or if the field does not exists, {@code true} elsewise
     */
    public <T> boolean setValue(TableField<T> field, T value) {
        return setValue(field.getName(), value);
    }

    /**
     * Get one value on this row.
     * @param field The field to get
     * @return The value of the field
     */
    public TableValue getValue(String field) {
        for (TableValue value : values) {
            if (value.getField().getName().equals(field)) {
                return value;
            }
        }
        return null;
    }

    /**
     * Get one value on this row.
     * @param field The field to get
     * @return The value of the field
     */
    public TableValue getValue(TableField<?> field) {
        return getValue(field.getName());
    }

    /**
     * Get the key of this field (defined by the primary key of the table).
     * @return The key
     */
    public Object getKey() {
        for (TableValue value : values) {
            if (value.getField().getIndexType() != null && value.getField().getIndexType().equals(IndexType.PRIMARY)) {
                return value;
            }
        }
        return null;
    }

    /**
     * Iterate over all values.
     * @param consumer Iteration function
     */
    public void forEach(Consumer<? super TableValue> consumer) {
        values.forEach(consumer);
    }

    /**
     * Compute all values.
     * @param function Compute function
     * @return {@code false} if all values did not pass the validation or if the field does not exists, {@code true} elsewise
     */
    public boolean computeAll(Function<? super TableValue, Object> function) {
        for (TableValue value : values) {
            if (!value.setValue(function.apply(value))) {
                return false;
            }
        }
        return true;
    }

    protected void setKey(Object key) {
        for (TableValue value : values) {
            if (value.getField().getIndexType() != null && value.getField().getIndexType().equals(IndexType.PRIMARY)) {
                value.setKey(key);
            }
        }
    }
}
