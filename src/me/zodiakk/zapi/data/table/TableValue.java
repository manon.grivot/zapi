package me.zodiakk.zapi.data.table;

import me.zodiakk.zapi.data.result.ResultValue;
import me.zodiakk.zapi.data.table.TableField.IndexType;

public class TableValue {
    private TableField<?> field;
    private Object value;

    /**
     * Get a table value from a result value.
     * @param result The result value
     */
    public TableValue(ResultValue result) {
        this.field = result.getField();
        this.value = result.getValue();
    }

    /**
     * Get a new table value from its field definition.
     * @param field The field
     */
    public TableValue(TableField<?> field) {
        this.field = field;
        this.value = field.getDefaultValue() != null ? field.getDefaultValue().getValue() : null;
    }

    /**
     * Validate a value.
     * @param value The value to validate
     * @return {@code false} if the value did not pass the validation or if the field does not exists, {@code true} elsewise
     */
    public boolean validateValue(Object value) {
        return field.validate(value);
    }

    /**
     * Set the value.
     * @param value The new value
     * @return {@code false} if the value did not pass the validation or if the field does not exists, {@code true} elsewise
     */
    public boolean setValue(Object value) {
        if (field.getIndexType() != null && field.getIndexType().equals(IndexType.PRIMARY) && this.value != null) {
            return true;
        }
        if (field.validate(value)) {
            this.value = value;
            return true;
        }
        return false;
    }

    protected void setKey(Object value) {
        this.value = value;
    }

    /**
     * Get the value.
     * @return The value
     */
    public Object getValue() {
        return value;
    }

    /**
     * Get the field of this value.
     * @return The field
     */
    public TableField<?> getField() {
        return field;
    }
}
