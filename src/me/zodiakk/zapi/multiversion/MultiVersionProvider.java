package me.zodiakk.zapi.multiversion;

public class MultiVersionProvider<T extends VersionSpecificInterface> {
    private T[] implementations;

    @SafeVarargs
    public MultiVersionProvider(T... implementations) {
        this.implementations = implementations;
    }

    public T getImplementation() {
        NMSVersion version = NMSVersion.getCurrentVersion();

        for (T impl : implementations) {
            if (impl.isCompatible(version)) {
                return impl;
            }
        }
        return null;
    }
}
