package me.zodiakk.zapi.multiversion;

import org.bukkit.Bukkit;

@SuppressWarnings("checkstyle:regexp")
public enum NMSVersion {
    UNKNOWN(-1),
    v1_8_R1(8),
    v1_8_R2(8),
    v1_8_R3(8),
    v1_9_R1(9),
    v1_9_R2(9),
    v1_10_R1(10),
    v1_11_R1(11),
    v1_12_R1(12),
    v1_13_R1(13),
    v1_13_R2(13),
    v1_14_R1(14),
    v1_15_R1(15),
    v1_16_R1(16);

    private final int versionId;

    private NMSVersion(int versionId) {
        this.versionId = versionId;
    }

    public static NMSVersion getCurrentVersion() {
        String packageName = Bukkit.getServer().getClass().getPackage().getName();
        try {
            NMSVersion version = valueOf(packageName.substring(packageName.lastIndexOf('.') + 1));

            return version != null ? version : UNKNOWN;
        } catch (IllegalArgumentException ex) {
            return UNKNOWN;
        }

    }

    public boolean isOlderThan(NMSVersion otherVersion) {
        if (this.versionId < otherVersion.versionId) {
            return true;
        }
        return false;
    }

    public boolean isAtMost(NMSVersion otherVersion) {
        if (this.versionId <= otherVersion.versionId) {
            return true;
        }
        return false;
    }

    public boolean isNewerThan(NMSVersion otherVersion) {
        if (this.versionId > otherVersion.versionId) {
            return true;
        }
        return false;
    }

    public boolean isAtLeast(NMSVersion otherVersion) {
        if (this.versionId >= otherVersion.versionId) {
            return true;
        }
        return false;
    }

    public boolean equals(NMSVersion otherVersion) {
        if (this.versionId == otherVersion.versionId) {
            return true;
        }
        return false;
    }
}
