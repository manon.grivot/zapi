package me.zodiakk.zapi.multiversion;

public interface VersionSpecificInterface {
    public boolean isCompatible(NMSVersion version);
}
