package me.zodiakk.zapi.tests;

import java.util.function.BiPredicate;

public final class TestAsserter {
    private TestCase testCase;

    public TestAsserter(TestCase testCase) {
        this.testCase = testCase;
    }

    public void assertEquals(Object actual, Object expected) throws TestException {
        assertEquals(actual, expected, "Failed on expectEquals");
    }

    public void assertEquals(Object actual, Object expected, String errorMessage) throws TestException {
        if (!actual.equals(expected)) {
            throw new TestException("[" + testCase.getName() + "] "
                    + errorMessage + ": actual '" + valueToString(actual) + "', expected '" + valueToString(expected) + "'.", testCase);
        }
    }

    public void assertNotEquals(Object actual, Object unexpected) throws TestException {
        assertNotEquals(actual, unexpected, "Failed on expectNotEquals");
    }

    public void assertNotEquals(Object actual, Object unexpected, String errorMessage) throws TestException {
        if (actual.equals(unexpected)) {
            throw new TestException("[" + testCase.getName() + "] "
                    + errorMessage + ": actual '" + valueToString(actual) + "', not expected '" + valueToString(unexpected) + "'.", testCase);
        }
    }

    public void assertNull(Object value) throws TestException {
        assertNull(value, "Failed on expectNull");
    }

    public void assertNull(Object value, String errorMessage) throws TestException {
        if (value != null) {
            throw new TestException("[" + testCase.getName() + "] "
                    + errorMessage + ": actual '" + value.toString() + "'.", testCase);
        }
    }

    public void assertNotNull(Object value) throws TestException {
        assertNotNull(value, "Failed on expectNotNull");
    }

    public void assertNotNull(Object value, String errorMessage) throws TestException {
        if (value == null) {
            throw new TestException("[" + testCase.getName() + "] "
                    + errorMessage + ": actual '(null)'.", testCase);
        }
    }

    public void assertRawComparator(Object actual, Object reference, BiPredicate<Object, Object> biPredicate) throws TestException {
        assertRawComparator(actual, reference, biPredicate, "Failed on expectRawComparator");
    }

    public void assertRawComparator(Object actual, Object reference, BiPredicate<Object, Object> biPredicate, String errorMessage) throws TestException {
        try {
            if (!biPredicate.test(actual, reference)) {
                throw new TestException("[" + testCase.getName() + "] "
                        + errorMessage + ": value '" + valueToString(actual) + "', reference '" + valueToString(reference) + "'.", testCase);
            }
        } catch (NullPointerException | ClassCastException ex) {
            throw new TestException("[" + testCase.getName() + "] (From catched exception: '" + ex.getClass().getName() + "') "
                        + errorMessage + ": value '" + valueToString(actual) + "', reference '" + valueToString(reference) + "'.", testCase);
        }
    }

    public <A,R> void assertComparator(A actual, R reference, BiPredicate<A, R> biPredicate) throws TestException {
        assertComparator(actual, reference, biPredicate, "Failed on expectComparator");
    }

    public <A,R> void assertComparator(A actual, R reference, BiPredicate<A, R> biPredicate, String errorMessage) throws TestException {
        try {
            if (!biPredicate.test(actual, reference)) {
                throw new TestException("[" + testCase.getName() + "] "
                        + errorMessage + ": value '" + valueToString(actual) + "', reference '" + valueToString(reference) + "'.", testCase);
            }
        } catch (NullPointerException ex) {
            throw new TestException("[" + testCase.getName() + "] (From catched exception: '" + ex.getClass().getName() + "') "
                        + errorMessage + ": value '" + valueToString(actual) + "', reference '" + valueToString(reference) + "'.", testCase);
        }
    }

    private static String valueToString(Object value) {
        if (value == null) {
            return "(null)";
        } else {
            return value.toString();
        }
    }
}
