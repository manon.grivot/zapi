package me.zodiakk.zapi.tests;

public abstract class TestCase {
    private String name;

    public TestCase(String name) {
        this.name = name;
    }

    /**
     * Run the test case.
     * To compare values, use asserter.expectX
     * @return The result of this test
     * @throws TestException If the test failed, a TestException must be thrown with more informations about why it failed.
     */
    public abstract TestResult run() throws TestException;

    /**
     * Get this test's name.
     * @return The test's name
     */
    public String getName() {
        return name;
    }
}
