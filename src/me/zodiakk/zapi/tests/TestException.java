package me.zodiakk.zapi.tests;

public class TestException extends Exception {
    private static final long serialVersionUID = 6523296292172603251L;
    private TestCase testCase;
    private boolean continueTests;

    public TestException(String description, TestCase testCase) {
        this(description, testCase, false);
    }

    public TestException(String description, TestCase testCase, boolean continueTests) {
        super(description);
        this.testCase = testCase;
        this.continueTests = continueTests;
    }

    public TestCase getTestCase() {
        return testCase;
    }

    public boolean getContinueTests() {
        return continueTests;
    }
}
