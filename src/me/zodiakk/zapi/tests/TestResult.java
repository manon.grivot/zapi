package me.zodiakk.zapi.tests;

public enum TestResult {
    /**
     * The test passed without any errors.
     */
    OK("§a§lPASSED"),

    /**
     * The test did not pass.
     */
    KO("§c§lFAILED"),

    /**
     * The test did not pass and an exception was thrown.
     */
    CRASHED("§4§lCRASHED"),

    /**
     * The test did not pass and an error was thrown, which can cause server crash.
     */
    FATAL("§4§lFATAL"),

    /**
     * The test was skipped.
     */
    SKIPPED("§b§lSKIPPED"),

    /**
     * The test did not pass to load and therefore was not executed.
     */
    FAILED("§e§lFAILED"),

    /**
     * The test did not pass but it should not stop tests execution.
     */
    KO_CONTINUE("§c§lFAILED §c(did not stop the test suite)"),

    /**
     * This test does not exist.
     */
    UNDEFINED("§c§lUNDEFINED");

    private String outputFormat;

    private TestResult(String outputFormat) {
        this.outputFormat = outputFormat;
    }

    public String getOutput() {
        return outputFormat;
    }
}
