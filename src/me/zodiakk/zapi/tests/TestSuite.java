package me.zodiakk.zapi.tests;

import java.util.Arrays;
import java.util.LinkedList;

import me.zodiakk.zapi.util.ChatSession;

public class TestSuite {
    private LinkedList<TestCase> testCases;
    private boolean running = false;
    private ChatSession outputSession;
    private LinkedList<String> lastOutput;

    public TestSuite(LinkedList<? extends TestCase> testCases) {
        this.testCases = new LinkedList<TestCase>();
        testCases.forEach(testCase -> this.testCases.add(testCase));
    }

    @SafeVarargs
    public <T extends TestCase> TestSuite(T... cases) {
        this.testCases = new LinkedList<TestCase>();
        for (int i = 0; i < cases.length; i++) {
            this.testCases.add(cases[i]);
        }
    }

    public TestSuite() {
        this.testCases = new LinkedList<TestCase>();
    }

    public <T extends TestCase> void addTestCase(T testCase) {
        this.testCases.add(testCase);
    }

    public void addAllTestCases(LinkedList<? extends TestCase> testCases) {
        testCases.forEach(testCase -> this.testCases.add(testCase));
    }

    @SafeVarargs
    public final <T extends TestCase> void addAllTestCases(T... cases) {
        for (int i = 0; i < cases.length; i++) {
            this.testCases.add(cases[i]);
        }
    }

    public void runSuite(ChatSession session) {
        if (running) {
            session.send("This test suite is already being executed, please retry later.");
            return;
        }
        running = true;
        this.lastOutput = new LinkedList<String>();
        this.outputSession = session;
        outputMessage("§aTest suite started.");
        for (TestCase testCase : testCases) {
            TestResult result = runCase(testCase);
            if (Arrays.asList(TestResult.CRASHED, TestResult.FATAL, TestResult.KO, TestResult.UNDEFINED).contains(result)) {
                outputMessage("§cTest suite ended prematurely.");
                running = false;
                return;
            }
        }
        outputMessage("§aTest suite ended.");
        running = false;
    }

    public TestResult runCase(TestCase testCase) {
        TestResult result;

        if (testCase == null) {
            result = TestResult.UNDEFINED;
            outputMessage("§6???: " + result.getOutput());
            return result;
        }

        try {
            outputMessage("§6Running test " + testCase.getName() + ".");
            result = testCase.run();
            if (!Arrays.asList(TestResult.OK, TestResult.SKIPPED, TestResult.FAILED).contains(result)) {
                outputMessage("§eWarning! This test returned an invalid test result (" + result.toString() + "). Return value of a test should only be OK, "
                        + "SKIPPED or FAILED, other possible values are reserved for internal use only.");
            }
            outputMessage("§6" + testCase.getName() + ": " + result.getOutput());
        } catch (TestException ex) {
            if (ex.getContinueTests()) {
                result = TestResult.KO_CONTINUE;
            } else {
                result = TestResult.KO;
            }
            outputMessage("§6" + testCase.getName() + ": " + result.getOutput());
            outputMessage(ex.getMessage());
        } catch (Exception ex) {
            result = TestResult.CRASHED;
            outputMessage("§6" + testCase.getName() + ": " + result.getOutput());
            outputMessage(ex.getMessage());
            outputMessage("§cMore informations have been printed to the console.");
            ex.printStackTrace();
        } catch (Error err) {
            result = TestResult.FATAL;
            outputMessage("§6" + testCase.getName() + ": " + result.getOutput());
            outputMessage(err.getMessage());
            outputMessage("§cMore informations have been printed to the console.");
            err.printStackTrace();
        }
        return result;
    }

    public void runOneCase(String name, ChatSession session) {
        for (TestCase testCase : testCases) {
            if (testCase.getName().equals(name)) {
                TestResult result;
                try {
                    session.send("§6Running test " + testCase.getName() + ".");
                    result = testCase.run();
                    if (!Arrays.asList(TestResult.OK, TestResult.SKIPPED, TestResult.FAILED).contains(result)) {
                        session.send("§eWarning! This test returned an invalid test result (" + result.toString() + "). Return value of a test should only "
                                + "be OK, SKIPPED or FAILED, other possible values are reserved for internal use only.");
                    }
                    session.send("§6" + testCase.getName() + ": " + result.getOutput());
                } catch (TestException ex) {
                    if (ex.getContinueTests()) {
                        result = TestResult.KO_CONTINUE;
                    } else {
                        result = TestResult.KO;
                    }
                    session.send("§6" + testCase.getName() + ": " + result.getOutput());
                    session.send(ex.getMessage());
                } catch (Exception ex) {
                    result = TestResult.CRASHED;
                    session.send("§6" + testCase.getName() + ": " + result.getOutput());
                    session.send(ex.getMessage());
                    session.send("§cMore informations have been printed to the console.");
                    ex.printStackTrace();
                } catch (Error err) {
                    result = TestResult.FATAL;
                    session.send("§6" + testCase.getName() + ": " + result.getOutput());
                    session.send(err.getMessage());
                    session.send("§cMore informations have been printed to the console.");
                    err.printStackTrace();
                }
            }
        }
    }

    public void printLastOutput(ChatSession session, Integer trim) {
        if (running) {
            session.send("§cThis test suite is running, please try again later.");
        } else {
            if (trim == 0) {
                lastOutput.forEach(line -> session.send(line));
            } else {
                for (int i = lastOutput.size() - trim - 1; i < lastOutput.size(); i++) {
                    session.send(lastOutput.get(i));
                }
            }
        }
    }

    public void outputMessage(String message) {
        this.outputSession.send(message);
        this.lastOutput.addLast(message);
    }
}
