package me.zodiakk.zapi.threading;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scheduler.BukkitTask;

import me.zodiakk.zapi.ZApi;

/**
 * A class representing a thread pool of the size specified in the configuration file. It also contains multiple functions using the Bukkit Scheduler.
 *
 * @author Zodiak
 * @since 2.0
 */
public class ThreadsManager {
    ExecutorService executor;

    /**
     * Internal constructor.
     */
    public ThreadsManager(ZApi api) {
        executor = Executors.newFixedThreadPool(ZApi.getConfiguration().getInteger("maxThreads"));
    }

    /**
     * Executes a command in one of the threads of the thread pool.
     * @param command The command to execute
     * @see ExecutorService#execute(Runnable)
     */
    public void execute(Runnable command) {
        executor.execute(command);
    }

    /**
     * Executes a command in one of the threads of the thread pool after a certain delay. The delay will be handled by the Bukkit Scheduler.
     * @param command The command to execute
     * @param delay The delay (in ticks) before the execution of the command
     * @return The BukkitTask handling the period and the delay. Cancelling it will cancel the whole task (but not its execution if it is being executed).
     * @see ExecutorService#execute(Runnable)
     * @see BukkitScheduler#runTaskLater(Plugin, Runnable, long)
     */
    public BukkitTask executeLater(Runnable command, Long delay) {
        return Bukkit.getScheduler().runTaskLater(ZApi.toPlugin(), new Runnable() {
            @Override
            public void run() {
                executor.execute(command);
            }
        }, delay);
    }

    /**
     * Executes a command in one of the threads of the thread pool repeatedly until cancelled, with a specified period and after a certain delay.
     * The delay and the period will be handled by the Bukkit Scheduler.
     * @param command The command to execute
     * @param delay The delay (in ticks) before the execution of the command
     * @param period The period (in ticks) between each execution of the command
     * @return The BukkitTask handling the period and the delay. Cancelling it will cancel the whole task (but not its execution if it is being executed).
     * @see ExecutorService#execute(Runnable)
     * @see BukkitScheduler#runTaskTimer(Plugin, Runnable, long, long)
     */
    public BukkitTask executeTimer(Runnable command, Long delay, Long period) {
        return Bukkit.getScheduler().runTaskTimer(ZApi.toPlugin(), new Runnable() {
            @Override
            public void run() {
                executor.execute(command);
            }
        }, delay, period);
    }

    /**
     * Executes a command inside of the main Bukkit thread.
     * @param command The command to execute
     * @return The BukkitTask representing this task.
     * @see BukkitScheduler#runTask(Plugin, Runnable)
     */
    public BukkitTask executeInMainThread(Runnable command) {
        return Bukkit.getScheduler().runTask(ZApi.toPlugin(), command);
    }

    /**
     * Executes a command inside of the main Bukkit thread after a certain delay.
     * @param command The command to execute
     * @param delay The delay (in ticks) before the execution of the command
     * @return The BukkitTask representing this task.
     * @see BukkitScheduler#runTaskLater(Plugin, Runnable, long)
     */
    public BukkitTask executeInMainThreadLater(Runnable command, Long delay) {
        return Bukkit.getScheduler().runTaskLater(ZApi.toPlugin(), command, delay);
    }

    /**
     * Executes a command inside of the main Bukkit thread repeatedly until cancelled, with a specified period and after a certain delay.
     * @param command The command to execute
     * @param delay The delay (in ticks) before the execution of the command
     * @param period The period (in ticks) between each execution of the command
     * @return The BukkitTask representing this task.
     * @see BukkitScheduler#runTaskTimer(Plugin, Runnable, long, long)
     */
    public BukkitTask executeInMainThreadTimer(Runnable command, Long delay, Long period) {
        return Bukkit.getScheduler().runTaskTimer(ZApi.toPlugin(), command, delay, period);
    }

    /**
     * Internal function.
     */
    public void close(ZApi api) {
        executor.shutdown();
    }

    /*
     * Wait for the thread pool to close.
     * @see ExecutorService#awaitTermination()
     */
    public void awaitClosing(long time, TimeUnit unit) throws InterruptedException {
        executor.awaitTermination(time, unit);
    }

    /**
     * Submit a command and get a {@link Future} representing this task.
     * @param command The command to execute
     * @return A {@link Future} representing this task. Will return null upon successful completion.
     * @see ExecutorService#submit(Runnable)
     */
    public Future<?> submit(Runnable command) {
        return executor.submit(command);
    }

    /**
     * Submit a command that will retrieve a value.
     * @param <T> The type of the value returned
     * @param command The command to execute
     * @return A {@link Future} representing the value being retrieved.
     * @see ExecutorService#submit(Callable)
     */
    public <T> Future<T> submit(Callable<T> command) {
        return executor.submit(command);
    }

    /**
     * Submit a command and return the given result.
     * @param <T> The type of the value returned
     * @param command The command to execute
     * @param result The result to return
     * @see ExecutorService#submit(Runnable, Object)
     */
    public <T> Future<T> submit(Runnable command, T result) {
        return executor.submit(command, result);
    }

    /**
     * See {@link ExecutorService#invokeAll(Collection)}.
     * @see ExecutorService#invokeAll(Collection)
     */
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks) throws InterruptedException {
        return executor.invokeAll(tasks);
    }

    /**
     * See {@link ExecutorService#invokeAll(Collection, long, TimeUnit)}.
     * @see ExecutorService#invokeAll(Collection, long, TimeUnit)
     */
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit) throws InterruptedException {
        return executor.invokeAll(tasks, timeout, unit);
    }

    /**
     * See {@link ExecutorService#invokeAny(Collection)}.
     * @see ExecutorService#invokeAny(Collection)
     */
    public <T> T invokeAny(Collection<? extends Callable<T>> tasks) throws InterruptedException, ExecutionException {
        return executor.invokeAny(tasks);
    }

    /**
     * See {@link ExecutorService#invokeAny(Collection, long, TimeUnit)}.
     * @see ExecutorService#invokeAny(Collection, long, TimeUnit)
     */
    public <T> T invokeAny(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit)
            throws InterruptedException, ExecutionException, TimeoutException {
        return executor.invokeAny(tasks, timeout, unit);
    }

    /**
     * Get the number of active threads on this thread pool.
     * @return The active thread count. Returns -1 if the thread pool is not initialized.
     */
    public int getActiveCount() {
        if (executor instanceof ThreadPoolExecutor) {
            return ((ThreadPoolExecutor) executor).getActiveCount();
        } else {
            return -1;
        }
    }
}
