package me.zodiakk.zapi.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import me.zodiakk.zapi.ZApi;

/**
 * A class used to send messages to the player, with a predefined format (prefix, colors, etc...).<br>
 * <p>
 * It also contains static methods for sending single messages to people and broadcasting messages.<br>
 * It can be used to ask the player for a value which he will send in the chat.
 *
 * @author Zodiak
 * @since 1.0
 */
public class ChatSession implements Listener {
    private static Collection<UUID> waitingForValue;
    private static Map<UUID, String> values;

    static {
        waitingForValue = new HashSet<UUID>();
        values = new HashMap<UUID, String>();
    }

    /**
     * Internal function.
     */
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onChat(AsyncPlayerChatEvent ev) {
        if (waitingForValue.contains(ev.getPlayer().getUniqueId())) {
            waitingForValue.remove(ev.getPlayer().getUniqueId());
            ev.setCancelled(true);
            values.put(ev.getPlayer().getUniqueId(), ev.getMessage());
        }
    }

    /**
     * Ask the player for a value that he will be writing in the chat.
     * His/her message will be hidden from the chat.
     * <b>WARNING!</b> It <b>MUST</b> be used in a separate thread, otherwise the server will freeze until the player send a message.
     * @return The value the player sent in the chat.
     * @throws InterruptedException If any other thread interrupted this thread
     */
    public String askForValue() throws InterruptedException {
        waitingForValue.add(player);
        while (!values.containsKey(player)) {
            Thread.sleep(50);
        }
        return values.remove(player);
    }

    private String prefix;
    private UUID player;
    private boolean def;

    /**
     * Creates a new ChatSession with the specified player.
     * @param prefix The text that will be displayed in the prefix.
     * @param player The player to whom the messages will be sent. Can be set to null to send messages to the console instead.
     */
    public ChatSession(@Nonnull String prefix, @Nullable UUID player) {
        this.prefix = new String(ChatColor.valueOf(ZApi.getConfiguration().getString("chatTitle")) + "&l" + prefix + " &8» &f");
        if (player == null) {
            def = true;
        } else {
            this.player = player;
            def = false;
        }
    }

    /**
     * Send a message to the player.
     * @param text The message to send
     */
    public void send(String text) {
        String m = prefix + text;
        if (!def) {
            Bukkit.getPlayer(player).sendMessage(ChatColor.translateAlternateColorCodes('&', m));
        } else {
            Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', m));
        }
    }

    /**
     * Send a message to the player without the prefix.
     * @param text The message to send
     * @deprecated Useless, use {@link Player#sendMessage(String)}
     */
    @Deprecated
    public void sendNoPrefix(String text) {
        if (!def) {
            Bukkit.getPlayer(player).sendMessage(ChatColor.translateAlternateColorCodes('&', text));
        } else {
            Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', text));
        }
    }

    /**
     * Send a message to another player.
     * @param player The player to whom the message will be sent
     * @param text The message to send
     */
    public void sendTo(UUID player, String text) {
        String m = prefix + text;
        Bukkit.getPlayer(player).sendMessage(ChatColor.translateAlternateColorCodes('&', m));
    }

    /**
     * Send a message to another player without the prefix.
     * @param player The player to whom the message will be sent
     * @param text The message to send
     * @deprecated Useless, use {@link Player#sendMessage(String)}
     */
    @Deprecated
    public void sendToNoPrefix(UUID player, String text) {
        Bukkit.getPlayer(player).sendMessage(ChatColor.translateAlternateColorCodes('&', text));
    }

    /**
     * Broadcast a message to all players on the server.
     * @param prefix The prefix of this message
     * @param text The message to send
     */
    public static void broadcast(String prefix, String text) {
        String m = "&8[" + ChatColor.valueOf(ZApi.getConfiguration().getString("chatTitle")) + "&l" + prefix + "&8] &7" + text;
        Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', m));
    }

    /**
     * Broadcast a message to all players on the server.
     * @deprecated Refactored, use {@link ChatSession#broadcast(String, String)}
     */
    @Deprecated
    public static void broadcastOne(String prefix, String text) {
        broadcast(prefix, text);
    }

    /**
     * Broadcast a message without prefix to all players on the server.
     * @param text The message to send
     * @deprecated Useless, use {@link Bukkit#broadcastMessage(String)}
     */
    @Deprecated
    public static void broadcastOneNoPrefix(String text) {
        Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', text));
    }

    /**
     * Send a message to a player.
     * @param player The player to whom the message will be sent
     * @param prefix The prefix of this message
     * @param text The message to send
     */
    public static void sendOne(UUID player, String prefix, String text) {
        String m = "&8[" + ChatColor.valueOf(ZApi.getConfiguration().getString("chatTitle")) + "&l" + prefix + "&8] &7" + text;
        Bukkit.getPlayer(player).sendMessage(ChatColor.translateAlternateColorCodes('&', m));
    }

    /**
     * Send a message without prefix to a player.
     * @param player The player to whom the message will be sent
     * @param text The message to send
     * @deprecated Useless, use {@link Player#sendMessage(String)}
     */
    @Deprecated
    public static void sendOneNoPrefix(UUID player, String text) {
        Bukkit.getPlayer(player).sendMessage(ChatColor.translateAlternateColorCodes('&', text));
    }

    /**
     * Get the player to whom the messages are being sent.
     * @return The player to whom the messages are being sent.
     */
    public Player getPlayer() {
        return Bukkit.getPlayer(player);
    }
}
