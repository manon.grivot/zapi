package me.zodiakk.zapi.util;

import java.io.File;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

/**
 * Static methods to retrieve config files.
 *
 * @since 3.0
 * @author Zodiak
 * @deprecated Yaml files are no longer used
 */
@Deprecated
public final class Config {
    @Deprecated
    public static FileConfiguration getConfig(String pluginName) {
        return getConfig(pluginName, "config.yml");
    }

    @Deprecated
    public static FileConfiguration getConfig(Plugin plugin) {
        return getConfig(plugin.getName(), "config.yml");
    }

    @Deprecated
    public static FileConfiguration getConfig(String pluginName, String file) {
        return YamlConfiguration.loadConfiguration(getConfigFile(pluginName, file));
    }

    @Deprecated
    public static FileConfiguration getConfig(Plugin plugin, String file) {
        return getConfig(plugin.getName(), file);
    }

    @Deprecated
    public static File getConfigFile(String pluginName) {
        return getConfigFile(pluginName, "config.yml");
    }

    @Deprecated
    public static File getConfigFile(Plugin plugin) {
        return getConfigFile(plugin.getName(), "config.yml");
    }

    @Deprecated
    public static File getConfigFile(String pluginName, String file) {
        return new File("plugins/" + pluginName + "/" + file);
    }

    @Deprecated
    public static File getConfigFile(Plugin plugin, String file) {
        return getConfigFile(plugin.getName(), file);
    }

    public static FileConfiguration combineConfigs(FileConfiguration into, FileConfiguration combined) {
        combineSections(into.getRoot(), combined.getRoot());
        return into;
    }

    private static void combineSections(ConfigurationSection into, ConfigurationSection combined) {
        for (String key : combined.getKeys(false)) {
            if (combined.isConfigurationSection(key)) {
                if (!into.isConfigurationSection(key)) {
                    into.createSection(key);
                }
                combineSections(into.getConfigurationSection(key), combined.getConfigurationSection(key));
            } else if (!into.isSet(key)) {
                into.set(key, combined.get(key));
            }
        }
    }
}
