package me.zodiakk.zapi.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFactory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.tr7zw.changeme.nbtapi.NBTItem;
import me.zodiakk.zapi.ZApi;

/**
 * A class containing useful functions to manipulate {@link ItemStack} objects.
 *
 * @author Zodiak
 * @since 3.0
 */
public class ItemUtil {
    private static final HashMap<Character, Integer> CHAR_LENGTH;
    private static ItemFactory factory = null;
    private static HashSet<Material> materials;

    static {
        factory = Bukkit.getServer().getItemFactory();
        materials = new HashSet<Material>();
        CHAR_LENGTH = new HashMap<Character, Integer>();

        for (Material mat : Material.values()) {
            materials.add(mat);
        }

        for (char ch : "i.,!:;|".toCharArray()) {
            CHAR_LENGTH.put(ch, 2);
        }
        for (char ch : "l'`".toCharArray()) {
            CHAR_LENGTH.put(ch, 3);
        }
        for (char ch : "It[] ".toCharArray()) {
            CHAR_LENGTH.put(ch, 4);
        }
        for (char ch : "fk()\"*<>²".toCharArray()) {
            CHAR_LENGTH.put(ch, 5);
        }
        for (char ch : "ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghjmnopqrsuvwxyzÆÇÉÈÊËÔÖŒÜàæçéèêëñôöü0123456789/\\?&$%+-=#_".toCharArray()) {
            CHAR_LENGTH.put(ch, 6);
        }
        for (char ch : "œ~@".toCharArray()) {
            CHAR_LENGTH.put(ch, 7);
        }
    }

    /**
     * Get the {@link ItemMeta} of an {@link ItemStack} without worrying if it has one - it will be created if needed.
     * @param item The {@link ItemStack}
     * @return The {@link ItemMeta} of this {@link ItemStack}
     */
    public static ItemMeta getItemMeta(ItemStack item) {
        if (item.hasItemMeta()) {
            return item.getItemMeta();
        } else {
            if (factory == null) {
                factory = Bukkit.getServer().getItemFactory();
            }
            return factory.getItemMeta(item.getType());
        }
    }

    /**
     * Set an NBT tag to an {@link ItemStack}.
     * @param item The {@link ItemStack}
     * @param key The key of this NBT tag
     * @param value The value of this NBT tag
     * @return The modified {@link ItemStack}.
     */
    public static ItemStack setNBTTag(ItemStack item, String key, Object value) {
        NBTItem nbti = new NBTItem(item);
        nbti.setObject(key, value);
        return nbti.getItem();
    }

    /**
     * Get the value of an NBT tag from an {@link ItemStack}.
     * @param item The {@link ItemStack}
     * @param key The key of this NBT tag
     * @param type The type of data being retrieved
     * @return The value of this NBT tag.
     */
    public static <T> T getNBTTag(ItemStack item, String key, Class<T> type) {
        NBTItem nbti = new NBTItem(item);
        return nbti.getObject(key, type);
    }

    /**
     * Try to match a {@link Material}, from its enum name, its key or by numerical ID.
     * @param key Key of the material
     * @return The corresponding {@link Material}, if found, {@code null} elsewise.
     */
    public static Material getMaterial(String key) {
        Material mat = Material.valueOf(key.toUpperCase());

        if (mat != null) {
            return mat;
        }
        mat = Material.matchMaterial(key);
        if (mat != null) {
            return mat;
        }
        try {
            mat = materials.stream().filter(mats -> mats.name().equalsIgnoreCase(key)).findFirst().get();
        } catch (NoSuchElementException ex) {
            return null;
        }
        return mat;
    }

    /**
     * Get the attack damage of a given {@link Material}.
     * @param material Material to get its attack damage
     * @return Attack damage of this Material.
     */
    public static Double getAttackDamage(Material material) {
        switch (material) {
        case DIAMOND_SWORD:
            return 8.0d;
        case DIAMOND_AXE: case IRON_SWORD:
            return 7.0d;
        case DIAMOND_PICKAXE: case IRON_AXE: case STONE_SWORD:
            return 6.0d;
        case DIAMOND_SPADE: case IRON_PICKAXE: case STONE_AXE: case WOOD_SWORD: case GOLD_SWORD:
            return 5.0d;
        case IRON_SPADE: case STONE_PICKAXE: case WOOD_AXE: case GOLD_AXE:
            return 4.0d;
        case STONE_SPADE: case WOOD_PICKAXE: case GOLD_PICKAXE:
            return 3.0d;
        case WOOD_SPADE: case GOLD_SPADE:
            return 2.0d;
        default:
            return 1.0d;
        }
    }

    /**
     * Parse a lore line so that it doesn't exceeds the pixels limit and with added colors.
     * @param line Line to parse
     * @return An array containing the line, or multiple lines if they needed to be split
     */
    public static List<String> parseLoreLine(String line) {
        line = ChatColor.translateAlternateColorCodes('&', line);
        List<String> result = new ArrayList<String>();
        String[] words = line.split(" ");
        String currentLine = "";
        Integer currentLineLength = 0;

        for (String word : words) {
            Integer wordLength = 0;

            for (char ch : word.toCharArray()) {
                if (!CHAR_LENGTH.containsKey(ch)) {
                    wordLength += 0;
                } else {
                    wordLength += CHAR_LENGTH.get(ch);
                }
            }

            if (wordLength + currentLineLength > ZApi.getConfiguration().getInteger("loreLineLength")) {
                String lastColor = ChatColor.getLastColors(currentLine);

                result.add(new String(currentLine));
                currentLine = lastColor + word;
                currentLineLength = wordLength;
            } else {
                if (currentLineLength != 0) {
                    currentLine += " ";
                    currentLineLength += CHAR_LENGTH.get(' ');
                }
                currentLine += word;
                currentLineLength += wordLength;
            }
        }
        result.add(currentLine);
        return result;
    }
}
