package me.zodiakk.zapi.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

import de.tr7zw.changeme.nbtapi.NBTItem;
import me.zodiakk.zapi.ZApi;
import me.zodiakk.zapi.util.Attributes.Attribute;
import me.zodiakk.zapi.util.Attributes.AttributeType;
import me.zodiakk.zapi.util.Attributes.Operation;
import net.md_5.bungee.api.ChatColor;

/**
 * A class to parse JSON into objects and objects into JSON.
 *
 * @author Zodiak
 * @since 4.0
 */
public class JsonUtil {
    private static final Gson GSON;

    static {
        GsonBuilder builder = new GsonBuilder();

        GSON = builder.setPrettyPrinting().create();
    }

    /**
     * Write a file with the given JSON element. If the file exists, it will be overwritten.
     * @param json JSON to write
     * @param file File in which it will be written
     */
    public static void writeJsonFile(JsonElement json, File file) {
        writeJsonFile(json, file, true);
    }

    /**
     * Write a file with the given JSON element.
     * @param json JSON to write
     * @param file File in which it will be written
     * @param overwrite If the file must be overwritten if it already exists
     */
    public static void writeJsonFile(JsonElement json, File file, boolean overwrite) {
        FileWriter fileWriter = null;
        BufferedWriter bufferedWriter = null;

        if (!overwrite && file.exists()) {
            return;
        }
        try {
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            file.setExecutable(false);
            file.setReadable(true);
            file.setWritable(true);

            fileWriter = new FileWriter(file);
            bufferedWriter = new BufferedWriter(fileWriter);

            bufferedWriter.write(GSON.toJson(json));

        } catch (IOException ex) {
            ZApi.postError(ex);
            return;
        } finally {
            try {
                bufferedWriter.close();
                fileWriter.close();
            } catch (NullPointerException ex) {
                // Do nothing as both variables may be null if anything happened before they were initialized
            } catch (IOException ex) {
                ZApi.postError(ex);
            }
        }
    }

    /**
     * Read a JSON file.
     * @param file File to read
     * @return JSON representation of this file
     */
    public static JsonElement readJsonFile(File file) {
        FileReader fileReader = null;
        BufferedReader bufferedReader = null;
        StringBuilder stringBuilder = new StringBuilder();
        JsonParser jsonParser = new JsonParser();
        JsonElement jsonElement;

        try {
            if (!file.exists()) {
                Bukkit.getLogger().severe("Trying to read a non-existent JSON file: " + file.getPath() + ".");
                return null;
            }

            fileReader = new FileReader(file);
            bufferedReader = new BufferedReader(fileReader);

            bufferedReader.lines().forEach(line -> stringBuilder.append(line));
            jsonElement = jsonParser.parse(stringBuilder.toString());

            return jsonElement;
        } catch (IOException ex) {
            ZApi.postError(ex);
            return null;
        } finally {
            try {
                bufferedReader.close();
                fileReader.close();
            } catch (NullPointerException ex) {
                // Do nothing as both variables may be null if anything happened before they were initialized
            } catch (IOException ex) {
                ZApi.postError(ex);
            }
        }
    }

    /**
     * Convert JSON to an Object.
     * @param <T> Type of the object
     * @param object JSON to convert
     * @param clazz Object class
     * @return Object representation
     */
    public static <T extends Serializable> T objectFromJson(JsonElement object, Class<T> clazz) {
        return GSON.fromJson(object, clazz);
    }

    /**
     * Convert an Object to JSON.
     * @param object Object to convert
     * @return JSON representation
     */
    public static JsonObject objectToJson(Serializable object) {
        return GSON.toJsonTree(object).getAsJsonObject();
    }

    /**
     * Convert JSON to an ItemStack.
     * @param object JSON to convert
     * @return ItemStack representation
     */
    public static ItemStack itemStackFromJson(JsonObject object) {
        try {
            Material itemMaterial = ItemUtil.getMaterial(object.get("material").getAsString());
            ItemStack stack;

            if (object.has("skull_skin") && itemMaterial.equals(Material.SKULL_ITEM)) {
                stack = PlayerUtil.getCustomHead(object.get("skull_skin").getAsString());
            } else {
                stack = new ItemStack(itemMaterial);
            }
            if (object.has("amount")) {
                stack.setAmount(object.get("amount").getAsInt());
            }
            if (object.has("damage")) {
                stack.setDurability(object.get("damage").getAsShort());
            }
            stack.setItemMeta(ItemUtil.getItemMeta(stack)); // Make sure the ItemStack has an ItemMeta
            if (object.has("attributes")) {
                if (object.get("attributes").getAsJsonArray().size() != 0) {
                    stack = addItemAttributes(stack, object.get("attributes").getAsJsonArray());
                }
            }
            if (object.has("custom_id") || object.has("bag_id")) {
                NBTItem nbt = new NBTItem(stack);

                if (object.has("custom_id")) {
                    nbt.setString("custom_id", object.get("custom_id").getAsString());
                }
                if (object.has("bag_id")) {
                    nbt.setString("bag_id", object.get("bag_id").getAsString());
                }
                stack = nbt.getItem();
            }

            ItemMeta meta = ItemUtil.getItemMeta(stack);
            if (object.has("name")) {
                meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', object.get("name").getAsString()));
            }
            if (object.has("lore")) {
                List<String> lore = new ArrayList<String>();

                object.get("lore").getAsJsonArray().forEach(line -> lore.add(ChatColor.translateAlternateColorCodes('&', line.getAsString())));
                meta.setLore(lore);
            }
            if (object.has("enchantments")) {
                object.get("enchantments").getAsJsonArray().forEach(enchantment -> {
                    JsonObject obj = enchantment.getAsJsonObject();

                    try {
                        meta.addEnchant(Enchantment.getByName(obj.get("id").getAsString().toUpperCase()), obj.get("level").getAsInt(), true);
                    } catch (Exception ex) {
                        ZApi.logDebug("Failed to parse ");
                    }
                });
            }
            if (object.has("flags")) {
                object.get("flags").getAsJsonArray().forEach(flag -> meta.addItemFlags(ItemFlag.valueOf(flag.getAsString().toUpperCase())));
            }
            stack.setItemMeta(meta);
            return stack;
        } catch (Exception ex) {
            ZApi.postError(ex);
            return null;
        }
    }

    /**
     * Convert an ItemStack to JSON.<p> Note that the skull skin will not be supported.
     * @param item ItemStack to convert
     * @return JSON representation
     */
    public static JsonObject itemStackToJson(ItemStack item) {
        JsonObject stackObject = new JsonObject();
        final ItemMeta meta = ItemUtil.getItemMeta(item);
        final Attributes attributes = new Attributes(item);
        final NBTItem nbt = new NBTItem(item);

        stackObject.addProperty("material", item.getType().toString());
        if (item.getAmount() != 1) {
            stackObject.addProperty("amount", item.getAmount());
        }
        if (item.getDurability() != 0) {
            stackObject.addProperty("damage", item.getDurability());
        }
        if (meta.hasDisplayName()) {
            stackObject.addProperty("name", meta.getDisplayName());
        }
        if (meta.hasLore()) {
            JsonArray jsonLore = new JsonArray();

            meta.getLore().forEach(line -> {
                jsonLore.add(new JsonPrimitive(line));
            });
            stackObject.add("lore", jsonLore);
        }
        if (meta.hasEnchants()) {
            JsonArray jsonEnchants = new JsonArray();

            meta.getEnchants().forEach((ench, level) -> {
                JsonObject enchantmentObject = new JsonObject();

                enchantmentObject.addProperty("id", ench.toString());
                enchantmentObject.addProperty("level", level);
                jsonEnchants.add(enchantmentObject);
            });
            stackObject.add("enchantments", jsonEnchants);
        }
        if (attributes.size() > 0) {
            JsonArray jsonAttributes = new JsonArray();

            attributes.values().forEach(attribute -> {
                JsonObject attributeObject = new JsonObject();

                attributeObject.addProperty("id", attribute.getAttributeType().toString());
                attributeObject.addProperty("operation", attribute.getOperation().getId());
                attributeObject.addProperty("amount", attribute.getAmount());
                jsonAttributes.add(attributeObject);
            });
        }
        if (meta.getItemFlags().size() > 0) {
            JsonArray jsonFlags = new JsonArray();

            meta.getItemFlags().forEach(flag -> {
                jsonFlags.add(new JsonPrimitive(flag.toString()));
            });
            stackObject.add("flags", jsonFlags);
        }
        if (nbt.hasKey("custom_id")) {
            stackObject.addProperty("custom_id", nbt.getString("custom_id"));
        }
        if (nbt.hasKey("bag_id")) {
            stackObject.addProperty("bag_id", nbt.getString("bag_id"));
        }
        return stackObject;
    }

    /**
     * Convert JSON to a Location.
     * @param object JSON to convert
     * @return Location representation
     */
    public static Location locationFromJson(JsonObject object) {
        try {
            Location location = new Location(
                Bukkit.getWorld(object.get("world").getAsString()),
                object.get("x").getAsDouble(),
                object.get("y").getAsDouble(),
                object.get("z").getAsDouble());

            if (object.has("yaw")) {
                location.setYaw(object.get("yaw").getAsFloat());
            }
            if (object.has("pitch")) {
                location.setPitch(object.get("pitch").getAsFloat());
            }
            return location;
        } catch (Exception ex) {
            ZApi.postError(ex);
            return null;
        }
    }

    /**
     * Convert a Location to JSON.
     * @param location Location to convert
     * @return JSON representation
     */
    public static JsonObject locationToJson(Location location) {
        JsonObject locationObject = new JsonObject();

        locationObject.addProperty("x", location.getX());
        locationObject.addProperty("y", location.getY());
        locationObject.addProperty("z", location.getZ());
        locationObject.addProperty("yaw", location.getYaw());
        locationObject.addProperty("pitch", location.getPitch());
        locationObject.addProperty("world", location.getWorld().getName());
        return locationObject;
    }

    /**
     * Convert JSON to an Inventory.
     * @param object JSON to convert
     * @return Inventory representation
     */
    public static Inventory inventoryFromJson(JsonObject object) {
        try {
            JsonArray array = object.get("items").getAsJsonArray();
            Inventory inventory;
            Integer[] idx = {-1};

            if (InventoryType.valueOf(object.get("type").getAsString()).equals(InventoryType.CHEST) && array.size() % 9 == 0 && array.size() <= 54) {
                inventory = Bukkit.getServer().createInventory(null, array.size());
            } else {
                inventory = Bukkit.getServer().createInventory(null, InventoryType.valueOf(object.get("type").getAsString()));
            }

            array.forEach(jsonItem -> {
                idx[0]++;
                if (jsonItem.isJsonNull()) {
                    inventory.setItem(idx[0], new ItemStack(Material.AIR));
                } else {
                    inventory.setItem(idx[0], itemStackFromJson(jsonItem.getAsJsonObject()));
                }
            });
            return inventory;
        } catch (Exception ex) {
            ZApi.postError(ex);
            return null;
        }
    }

    /**
     * Convert an Inventory to JSON.
     * @param inventory Inventory to convert
     * @return JSON representation
     */
    public static JsonObject inventoryToJson(Inventory inventory) {
        JsonObject inventoryObject = new JsonObject();
        JsonArray items = new JsonArray();

        inventoryObject.addProperty("type", inventory.getType().toString());
        for (int i = 0; i < inventory.getSize(); i++) {
            if (inventory.getItem(i) == null || inventory.getItem(i).getType().equals(Material.AIR)) {
                items.add(JsonNull.INSTANCE);
            } else {
                items.add(itemStackToJson(inventory.getItem(i)));
            }
        }
        inventoryObject.add("items", items);
        return inventoryObject;
    }

    /**
     * Convert JSON to a PlayerInventory.
     * @param object JSON to convert
     * @return PlayerInventory representation
     */
    public static PlayerInventory playerInventoryFromJson(JsonObject object) {
        object.addProperty("type", InventoryType.PLAYER.toString());
        PlayerInventory inventory = (PlayerInventory) inventoryFromJson(object);
        JsonArray armorArray = object.get("armor").getAsJsonArray();
        ItemStack[] armorStacks = new ItemStack[armorArray.size()];

        for (int i = 0; i < armorStacks.length; i++) {
            armorStacks[i] = itemStackFromJson(armorArray.get(i).getAsJsonObject());
        }
        inventory.setArmorContents(armorStacks);
        return inventory;
    }

    /**
     * Convert a PlayerInventory to JSON.
     * @param playerInventory PlayerInventory to convert
     * @return JSON representation
     */
    public static JsonObject playerInventoryToJson(PlayerInventory playerInventory) {
        JsonObject inventoryObject = new JsonObject();
        JsonArray itemsArray = new JsonArray();
        JsonArray armorArray = new JsonArray();

        for (int i = 0; i < playerInventory.getSize(); i++) {
            if (playerInventory.getItem(i) == null) {
                itemsArray.add(JsonNull.INSTANCE);
            } else {
                itemsArray.add(itemStackToJson(playerInventory.getItem(i)));
            }
        }
        for (int i = 0; i < playerInventory.getArmorContents().length; i++) {
            if (playerInventory.getArmorContents()[i] == null) {
                armorArray.add(JsonNull.INSTANCE);
            } else {
                armorArray.add(itemStackToJson(playerInventory.getArmorContents()[i]));
            }
        }
        inventoryObject.add("items", itemsArray);
        inventoryObject.add("armor", armorArray);
        return inventoryObject;
    }

    private static ItemStack addItemAttributes(ItemStack stack, JsonArray attributes) {
        Attributes itemAttributes = new Attributes(stack);
        int[] i = {0};

        // Re-add base damage to items
        itemAttributes.add(Attribute.newBuilder().name("zapi-base-damage")
                .type(AttributeType.GENERIC_ATTACK_DAMAGE)
                .operation(Operation.ADD_NUMBER)
                .amount(ItemUtil.getAttackDamage(stack.getType()).intValue() - 1)
                .build());

        attributes.forEach(attribute -> {
            JsonObject obj = attribute.getAsJsonObject();

            itemAttributes.add(Attribute.newBuilder().name("zapi-attributes-" + i[0]++)
                    .type(Attributes.matchAttributeType(obj.get("id").getAsString()))
                    .operation(Operation.fromId(obj.get("operation").getAsInt()))
                    .amount(obj.get("amount").getAsDouble())
                    .build());
        });

        return itemAttributes.getStack();
    }
}
