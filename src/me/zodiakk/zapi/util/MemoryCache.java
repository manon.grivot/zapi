package me.zodiakk.zapi.util;

import java.util.ArrayList;
import org.apache.commons.collections4.MapIterator;
import org.apache.commons.collections4.map.LRUMap;

import me.zodiakk.zapi.ZApi;

/**
 * A re-implementation of the {@link java.util.Map} class that introduces a cache system.<br>
 *
 * @author Zodiak
 * @since 2.2
 * @param <K> The key type
 * @param <T> The value type
 */
@SuppressWarnings("rawtypes")
public class MemoryCache<K, T> {
    private long timeToLive;
    private LRUMap cacheMap;

    protected class CacheObject {
        public long lastAccessed = System.currentTimeMillis();
        public T value;

        protected CacheObject(T value) {
            this.value = value;
        }
    }

    /**
     * Default constructor with 15 minutes of time to live, 15 seconds of interval between cleanups, and 500.000 maximum items.
     */
    public MemoryCache() {
        this(900, 15, 500000);
    }

    /**
     * Creates a new MemoryCache instance with the specified parameters.
     * @param timeToLive The maximum amount of time after last access of an object before destruction
     * @param timerInterval The interval between each cleanup
     * @param maxItems The maximum amount of objects in the map
     */
    public MemoryCache(long timeToLive, final long timerInterval, int maxItems) {
        this.timeToLive = timeToLive * 1000;
        cacheMap = new LRUMap(maxItems);

        if (timeToLive > 0 && timerInterval > 0) {
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        try {
                            Thread.sleep(timerInterval * 1000);
                        } catch (InterruptedException ex) {
                            ZApi.postError(ex);
                        }
                        cleanup();
                    }
                }
            });
            t.setDaemon(true);
            t.start();
        }
    }

    /**
     * Put an object into the map.
     * @param key The key of this object
     * @param value The value of this object
     * @return The value previously mapped to this key, null if absent.
     * @see LRUMap#put(Object, Object)
     */
    @SuppressWarnings("unchecked")
    public Object put(K key, T value) {
        synchronized (cacheMap) {
            return cacheMap.put(key, new CacheObject(value));
        }
    }

    /**
     * Put an object into the map if there is no object linked to the specified key.
     * @param key The key of this object
     * @param value The value of this object
     * @return A boolean representing whether if the value has been added to the map.
     * @see LRUMap#putIfAbsent(Object, Object)
     */
    @SuppressWarnings("unchecked")
    public boolean putIfAbsent(K key, T value) {
        synchronized (cacheMap) {
            if (cacheMap.get(key) == null) {
                return false;
            }
            cacheMap.putIfAbsent(key, new CacheObject(value));
        }
        return true;
    }

    /**
     * Get the value mapped to the key.
     * @param key The key
     * @return The mapped value, null if there is none
     */
    @SuppressWarnings("unchecked")
    public T get(K key) {
        synchronized (cacheMap) {
            CacheObject c = (CacheObject) cacheMap.get(key);

            if (c == null) {
                return null;
            } else {
                c.lastAccessed = System.currentTimeMillis();
                return c.value;
            }
        }
    }

    /**
     * Remove an object from the map.
     * @param key The key of this object
     * @return The last value of this object, null if there was none.
     */
    public Object remove(K key) {
        synchronized (cacheMap) {
            return cacheMap.remove(key);
        }
    }

    /**
     * Get the size of this map.
     * @return The amount of objects in the map.
     */
    public int size() {
        synchronized (cacheMap) {
            return cacheMap.size();
        }
    }

    /**
     * Delete all the objects that has not been accessed since more time than the time to live specified when creating the map.
     */
    @SuppressWarnings("unchecked")
    public void cleanup() {
        long now = System.currentTimeMillis();
        ArrayList<K> deleteKey = null;

        synchronized (cacheMap) {
            MapIterator itr = cacheMap.mapIterator();
            deleteKey = new ArrayList<K>((cacheMap.size() / 2) + 1);
            K key = null;
            CacheObject c = null;

            while (itr.hasNext()) {
                key = (K) itr.next();
                c = (CacheObject) itr.getValue();
                if (c != null && (now > (timeToLive + c.lastAccessed))) {
                    deleteKey.add(key);
                }
            }
        }

        for (K key : deleteKey) {
            synchronized (cacheMap) {
                cacheMap.remove(key);
            }
            Thread.yield();
        }
    }

    /**
     * Check if a key is present in the map.
     * @param key The key to check
     * @return True if the key is present, false if not.
     */
    public boolean containsKey(K key) {
        synchronized (cacheMap) {
            return cacheMap.containsKey(key);
        }
    }

    /**
     * Check if a value is present in the map.
     * @param value The value to check
     * @return True if the value is present, false if not.
     */
    public boolean containsValue(T value) {
        synchronized (cacheMap) {
            return cacheMap.containsValue(value);
        }
    }
}
