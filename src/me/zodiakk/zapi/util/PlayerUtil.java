package me.zodiakk.zapi.util;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.google.common.collect.ImmutableList;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

import org.apache.commons.codec.binary.Base64;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import me.zodiakk.zapi.ZApi;

/**
 * Utility functions for players.
 *
 * @author Zodiak
 * @since 1.0
 */
public final class PlayerUtil {
    public static HashMap<String, UUID> uuidCache = new HashMap<String, UUID>();
    public static HashMap<String, ItemStack> headCache = new HashMap<String, ItemStack>();

    /**
     * A class for retrieving UUIDs.<br>
     * <p>
     * <b>WARNING</b>, this class will only return premium UUIDs, even on an offline
     * server.
     *
     * @author evilmidget38 (Bukkit)
     * @since 0.1
     */
    public static class UUIDFetcher {
        private static final double PROFILES_PER_REQUEST = 100;
        private static final String PROFILE_URL = "https://api.mojang.com/profiles/minecraft";
        private final JSONParser jsonParser = new JSONParser();
        private final List<String> names;
        private final boolean rateLimiting;

        /**
         * Initialize a new UUID fetcher with a list of names.
         *
         * @param names        The list of minecraft usernames
         * @param rateLimiting Limit the rate at which the request are being sent (100ms
         *                     per request)
         */
        public UUIDFetcher(List<String> names, boolean rateLimiting) {
            this.names = ImmutableList.copyOf(names);
            this.rateLimiting = rateLimiting;
        }

        /**
         * Initialize a new UUID fetcher with a list of names.
         *
         * @param names The list of minecraft usernames
         */
        public UUIDFetcher(List<String> names) {
            this(names, true);
        }

        /**
         * Run the UUID fetcher and retrieve UUIDs.
         *
         * @return A map associating each usernames with its UUID.
         * @throws ParseException       If the JSON parser failed to read the HTTP
         *                              response from Mojang's server
         * @throws IOException          If an I/O error occurs when reading any I/O
         *                              stream or with the HTTP connection
         * @throws InterruptedException If the rate limiter is interrupted
         */
        public Map<String, UUID> call() throws IOException, InterruptedException, ParseException {
            Map<String, UUID> uuidMap = new HashMap<String, UUID>();
            int requests = (int) Math.ceil(names.size() / PROFILES_PER_REQUEST);
            for (int i = 0; i < requests; i++) {
                HttpURLConnection connection = createConnection();
                String body = JSONArray.toJSONString(names.subList(i * 100, Math.min((i + 1) * 100, names.size())));
                writeBody(connection, body);
                JSONArray array = (JSONArray) jsonParser.parse(new InputStreamReader(connection.getInputStream()));
                for (Object profile : array) {
                    JSONObject jsonProfile = (JSONObject) profile;
                    String id = (String) jsonProfile.get("id");
                    String name = (String) jsonProfile.get("name");
                    UUID uuid = UUIDFetcher.getUUID(id);
                    uuidMap.put(name, uuid);
                }
                if (rateLimiting && i != requests - 1) {
                    Thread.sleep(100L);
                }
            }
            return uuidMap;
        }

        private static void writeBody(HttpURLConnection connection, String body) throws IOException {
            OutputStream stream = connection.getOutputStream();
            stream.write(body.getBytes());
            stream.flush();
            stream.close();
        }

        private static HttpURLConnection createConnection() throws IOException {
            URL url = new URL(PROFILE_URL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            return connection;
        }

        private static UUID getUUID(String id) {
            return UUID.fromString(id.substring(0, 8) + "-" + id.substring(8, 12) + "-" + id.substring(12, 16) + "-"
                    + id.substring(16, 20) + "-" + id.substring(20, 32));
        }

        /**
         * Converts an {@link UUID} into a byte array representing this UUID.
         *
         * @param uuid The UUID to convert
         * @return A byte array representing this UUID.
         */
        public static byte[] toBytes(UUID uuid) {
            ByteBuffer byteBuffer = ByteBuffer.wrap(new byte[16]);
            byteBuffer.putLong(uuid.getMostSignificantBits());
            byteBuffer.putLong(uuid.getLeastSignificantBits());
            return byteBuffer.array();
        }

        /**
         * Converts a byte array representing an UUID into an actual {@link UUID}.
         *
         * @param array The byte array
         * @return The {@link UUID} object
         */
        public static UUID fromBytes(byte[] array) {
            if (array.length != 16) {
                throw new IllegalArgumentException("Illegal byte array length: " + array.length);
            }
            ByteBuffer byteBuffer = ByteBuffer.wrap(array);
            long mostSignificant = byteBuffer.getLong();
            long leastSignificant = byteBuffer.getLong();
            return new UUID(mostSignificant, leastSignificant);
        }

        /**
         * Get the UUID of a player.
         *
         * @param name The name of the player
         * @return The UUID of this player.
         * @throws ParseException       If the JSON parser failed to read the HTTP
         *                              response from Mojang's server
         * @throws IOException          If an I/O error occurs when reading any I/O
         *                              stream or with the HTTP connection
         * @throws InterruptedException If the rate limiter is interrupted
         * @deprecated Prefer using {@link PlayerUtil#getUUIDOf(String)} which uses an
         *             UUID cache to store recently retrieved UUIDs.
         */
        @Deprecated
        public static UUID getUUIDOf(String name) throws IOException, InterruptedException, ParseException {
            return new UUIDFetcher(Arrays.asList(name)).call().get(name);
        }
    }

    /**
     * Get the UUID of a player.
     *
     * @param name The name of the player
     * @return The UUID of this player.
     * @throws ParseException       If the JSON parser failed to read the HTTP
     *                              response from Mojang's server
     * @throws IOException          If an I/O error occurs when reading any I/O
     *                              stream or with the HTTP connection
     * @throws InterruptedException If the rate limiter is interrupted
     */
    public static UUID getUUIDOf(String name) throws IOException, InterruptedException, ParseException {
        if (uuidCache.containsKey(name)) {
            return uuidCache.get(name);
        }
        UUID u = new UUIDFetcher(Arrays.asList(name)).call().get(name);
        uuidCache.put(name, u);
        return u;
    }

    /**
     * Get the UUID of the default player (named Player).
     *
     * @return The UUID of "Player", null if retrieving it failed.
     */
    public static UUID getDefaultPlayer() {
        try {
            return getUUIDOf("Player");
        } catch (IOException | InterruptedException | ParseException e) {
            ZApi.postError(e);
            return null;
        }
    }

    /**
     * Get the ping of the player.
     *
     * @param p The player
     * @return The ping of this player, in ms.
     */
    public static int getPlayerPing(Player p) {
        Class<?> entityPlayerClass = ReflectionUtil.getNMSClass("EntityPlayer");
        Class<?> craftPlayerClass = ReflectionUtil.getNMSClass("CraftPlayer");
        Object craftPlayer = craftPlayerClass.cast(p);
        Object entityPlayer;
        try {
            entityPlayer = ReflectionUtil.getMethod(craftPlayerClass, "getHandle").invoke(craftPlayer);

            return ReflectionUtil.getField(entityPlayerClass, "ping").getInt(entityPlayer);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            ZApi.postError(ex);
            return -1;
        }
    }

    /**
     * Get the language of the player.
     * @param p The player
     * @return The language of the player, in the format xx_XX. If any error occured, it will return "fr_FR".
     */
    public static String getLanguage(Player p) {
        Object ep;
        try {
            ep = ReflectionUtil.getMethod(p.getClass(), "getHandle", Object[].class).invoke(p, (Object[]) null);
            Field f = ep.getClass().getDeclaredField("locale");
            f.setAccessible(true);
            String language = (String) f.get(ep);
            return language;
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchFieldException | SecurityException e) {
            ZApi.postError(e);
            return "fr_FR";
        }
    }

    /**
     * Verify if the player has enough inventory slots available for x items.
     * @param p The player
     * @param slots The number of inventory slots needed
     * @return {@code true} if the player has enough space, {@code false} elsewise
     */
    public static boolean hasEnoughFreeSpace(Player p, Integer slots) {
        Integer free = 0;
        for (ItemStack i : p.getInventory()) {
            if (i == null) {
                free++;
            }
        }
        return free >= slots;
    }

    /**
     * Check if a player is online.
     * @param player The player name to check
     * @return True if the player is online, false if not.
     */
    public static boolean isOnline(String player) {
        for (Player pl : Bukkit.getOnlinePlayers()) {
            if (pl.getName().equals(player)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get a custom head with the specified skin.
     * <p>This function implements a cache system, already retrieved heads will be cached and return upon request.<br>
     * Each {@link ItemStack} returned is guaranteed to be unique.
     * @param url URL to the skin
     * @return A custom head, or {@code null} if it couldn't be created
     */
    public static ItemStack getCustomHead(String url) {
        synchronized (headCache) {
            if (headCache.containsKey(url)) {
                return headCache.get(url).clone();
            }
        }

        ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
        if (url.isEmpty()) {
            synchronized (headCache) {
                headCache.put(url, head);
            }
            return head.clone();
        }

        SkullMeta meta = (SkullMeta) ItemUtil.getItemMeta(head);
        GameProfile profile = new GameProfile(UUID.randomUUID(), null);
        byte[] encodedData = Base64.encodeBase64(String.format("{textures:{SKIN:{url:\"%s\"}}}", url).getBytes());
        Field profileField = null;

        profile.getProperties().put("textures", new Property("textures", new String(encodedData)));
        try {
            profileField = meta.getClass().getDeclaredField("profile");
            profileField.setAccessible(true);
            profileField.set(meta, profile);
        } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException ex) {
            ZApi.postError(ex);
            return null;
        }

        head.setItemMeta(meta);

        synchronized (headCache) {
            headCache.put(url, head);
        }

        return head.clone();
    }
}
