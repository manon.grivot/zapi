package me.zodiakk.zapi.util;

import java.util.concurrent.ThreadLocalRandom;

/**
 * A class providing random generating methods.
 *
 * @author Zodiak
 * @since 4.0
 */
public class RandomUtil {
    private static ThreadLocalRandom rnd;

    static {
        rnd = ThreadLocalRandom.current();
    }

    /**
     * Test a probability.
     * @param chance Probability of the result being true, between 0.0 and 1.0.
     * @return True if the test succeeded, false if it failed
     */
    public static boolean testChance(Double chance) {
        return (rnd.nextDouble() < chance);
    }

    /**
     * Get a random double between 0.0 and 1.0.
     * @return Random double
     */
    public static double getDouble() {
        return rnd.nextDouble();
    }

    /**
     * Get a random integer between 0 and a specified value, excluded.
     * @param bound Maximum value, excluded
     * @return Random integer
     */
    public static int getInt(int bound) {
        double res = rnd.nextDouble();

        return Double.valueOf(res * bound).intValue();
    }

    /**
     * Get a random boolean with equal probability.
     * @return True or false, with equal probability
     */
    public static boolean getBoolean() {
        return rnd.nextDouble() < 0.5;
    }

    /**
     * Get a random number based on a gaussian distribution, with a mean of 0.0 and a derivation of 1.0.
     * @return Random double based on a gaussian distribution
     */
    public static double getGaussian() {
        return rnd.nextGaussian();
    }

    /**
     * Get a random number based on a gaussian distribution.
     * @param mean Mean (μ)
     * @param deviation Derivation (σ²)
     * @return Random double based on a gaussian distribution
     */
    public static double getGaussian(double mean, double deviation) {
        return (rnd.nextGaussian() / Math.sqrt(deviation) + mean);
    }

    /**
     * Get a random base 64 string.
     * @param length Length of the string
     * @return Random string
     */
    public static String getBase64String(int length) {
        return getString(length, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-");
    }

    /**
     * Get a random base 16 string.
     * @param length Length of the string
     * @return Random string
     */
    public static String getBase16String(int length) {
        return getString(length, "0123456789abcdef");
    }

    /**
     * Get a random string made of uppercase characters and numbers.
     * @param length Length of the string
     * @return Random string
     */
    public static String getString(int length) {
        return getString(length, "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
    }

    /**
     * Get a random string made of specified characters.
     * @param length Length of the string
     * @param range Range to use (a string containing all possible characters)
     * @return Random string
     */
    public static String getString(int length, String range) {
        final char[] sym = range.toCharArray();
        final char[] buf = new char[length];

        for (int i = 0; i < length; ++i) {
            buf[i] = sym[getInt(sym.length)];
        }
        return new String(buf);
    }
}
