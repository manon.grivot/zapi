package me.zodiakk.zapi.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.regex.Pattern;

import org.apache.commons.codec.binary.Base64;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import me.zodiakk.zapi.ZApi;

/**
 * Utility functions for (de)serializing objects.
 *
 * @author Zodiak
 * @since 1.0
 */
public final class Serializer {
    private static final String LOCATION_DIVIDER = "||LOC||";

    /**
     * Deserialize an {@link Object} array.
     * @param s The serialized {@link Object} array
     * @return The actual {@link Object} array.
     */
    public static Object[] objectArrayFromBase64(String s) {
        ByteArrayInputStream in = new ByteArrayInputStream(Base64.decodeBase64(s.getBytes()));
        try {
            return (Object[]) new ObjectInputStream(in).readObject();
        } catch (ClassNotFoundException | IOException e) {
            ZApi.postError(e);
        }
        return null;
    }

    /**
     * Deserialize an {@link Object}.
     * @param s The serialized {@link Object}
     * @return The actual {@link Object}.
     */
    public static Object objectFromBase64(String s) {
        ByteArrayInputStream in = new ByteArrayInputStream(Base64.decodeBase64(s.getBytes()));
        try {
            return new ObjectInputStream(in).readObject();
        } catch (ClassNotFoundException | IOException e) {
            ZApi.postError(e);
        }
        return null;
    }

    /**
     * Serialize a {@link Serializable} object.
     * @param o The {@link Serializable} to serialize
     * @return A string representing this object.
     */
    public static String objectToBase64(Serializable o) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            new ObjectOutputStream(out).writeObject(o);
        } catch (IOException e) {
            ZApi.postError(e);
        }

        return new String(Base64.encodeBase64(out.toByteArray()));
    }

    /**
     * Deserialize a {@link Location}.
     * @param s The serialized {@link Location}
     * @return The actual {@link Location}.
     */
    public static Location locationFromBase64(String s) {
        if (s.length() == 0) {
            return null;
        }
        String[] data = s.split(Pattern.quote(LOCATION_DIVIDER));
        if (data.length == 0) {
            return null;
        }
        try {
            double x = Double.parseDouble(data[0]);
            double y = Double.parseDouble(data[1]);
            if (y < 1) {
                y = 1;
            }
            double z = Double.parseDouble(data[2]);
            float yaw = Float.parseFloat(data[3]);
            float pitch = Float.parseFloat(data[4]);
            World w = Bukkit.getServer().getWorld(data[5]);
            if (w == null) {
                w = Bukkit.getWorlds().get(0);
                Bukkit.getLogger().severe("Unable to deserialize location " + s + " !");
                return w.getSpawnLocation();
            }
            return new Location(w, x, y, z, yaw, pitch);
        } catch (Exception e) {
            ZApi.postError(e);
            System.out.println("Corrupted location save: " + s);
        }
        return null;
    }

    /**
     * Serialize a {@link Location}.
     * @param loc The {@link Location} to serialize
     * @return A string representing the serialized {@link Location}.
     */
    public static String locationToBase64(Location loc) {
        StringBuilder sb = new StringBuilder();
        sb.append(loc.getX());
        sb.append(LOCATION_DIVIDER);
        if (loc.getY() < 1) {
            sb.append("1.0");
        } else {
            sb.append(loc.getY());
        }
        sb.append(LOCATION_DIVIDER);
        sb.append(loc.getZ());
        sb.append(LOCATION_DIVIDER);
        sb.append(loc.getYaw());
        sb.append(LOCATION_DIVIDER);
        sb.append(loc.getPitch());
        sb.append(LOCATION_DIVIDER);
        sb.append(loc.getWorld().getName());
        return sb.toString();
    }

    /**
     * Serialize an {@link ItemStack} array.
     * @param item The {@link ItemStack} array to serialize
     * @return A string representing the serialized {@link ItemStack} array.
     * @throws IllegalStateException If any error occurs during the serialization
     */
    public static String itemStackToBase64(ItemStack item) throws IllegalStateException {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);
            dataOutput.writeObject(item);
            dataOutput.close();
            return Base64Coder.encodeLines(outputStream.toByteArray());
        } catch (Exception e) {
            ZApi.postError(e);
            throw new IllegalStateException("Unable to save item stacks.", e);
        }
    }

    /**
     * Deserialize an {@link ItemStack}.
     * @param data The serialized {@link ItemStack}
     * @return The actual {@link ItemStack}.
     * @throws IllegalStateException If any error occurs during the deserialization
     */
    public static ItemStack itemStackFromBase64(String data) throws IllegalStateException {
        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(data));
            BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
            ItemStack item = (ItemStack) dataInput.readObject();
            dataInput.close();
            return item;
        } catch (Exception e) {
            throw new IllegalStateException("Unable to decode class type.", e);
        }
    }

    /**
     * Serialize a {@link PlayerInventory}.
     * @param playerInventory The inventory to serialize
     * @return A string array representing the serialized inventory.
     * @throws IllegalStateException If any error occurs during the serialization
     */
    public static String[] playerInventoryToBase64(PlayerInventory playerInventory) throws IllegalStateException {
        String content = inventoryToBase64(playerInventory);
        String armor = itemStackArrayToBase64(playerInventory.getArmorContents());
        return new String[] { content, armor };
    }

    /**
     * Deserialize a {@link PlayerInventory}.
     * @param playerInventory The serialized {@link PlayerInventory}
     * @return The actual {@link PlayerInventory}.
     */
    public static PlayerInventory playerInventoryFromBase64(String[] playerInventory) {
        PlayerInventory inventory = (PlayerInventory) inventoryFromBase64(playerInventory[0]);
        inventory.setArmorContents(itemStackArrayFromBase64(playerInventory[1]));
        return inventory;
    }

    /**
     * Serialize an {@link ItemStack} array.
     * @param items The {@link ItemStack} array to serialize
     * @return A string representing the serialized {@link ItemStack} array.
     * @throws IllegalStateException If any error occurs during the serialization
     */
    public static String itemStackArrayToBase64(ItemStack[] items) throws IllegalStateException {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);
            dataOutput.writeInt(items.length);
            for (int i = 0; i < items.length; i++) {
                dataOutput.writeObject(items[i]);
            }
            dataOutput.close();
            return Base64Coder.encodeLines(outputStream.toByteArray());
        } catch (Exception e) {
            throw new IllegalStateException("Unable to save item stacks.", e);
        }
    }

    /**
     * Serialize an {@link Inventory}.
     * @param inventory The {@link Inventory} to serialize
     * @return A string representing the serialized {@link Inventory}.
     * @throws IllegalStateException If any error occurs during the serialization
     */
    public static String inventoryToBase64(Inventory inventory) throws IllegalStateException {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);
            dataOutput.writeInt(inventory.getSize());
            for (int i = 0; i < inventory.getSize(); i++) {
                dataOutput.writeObject(inventory.getItem(i));
            }
            dataOutput.close();
            return Base64Coder.encodeLines(outputStream.toByteArray());
        } catch (Exception e) {
            throw new IllegalStateException("Unable to save item stacks.", e);
        }
    }

    /**
     * Deserialize an {@link Inventory}.
     * @param data The serialized {@link Inventory}
     * @return The actual {@link Inventory}.
     * @throws IllegalStateException If any error occurs during the deserialization
     */
    public static Inventory inventoryFromBase64(String data) throws IllegalStateException {
        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(data));
            BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
            Inventory inventory = Bukkit.getServer().createInventory(null, dataInput.readInt());
            for (int i = 0; i < inventory.getSize(); i++) {
                inventory.setItem(i, (ItemStack) dataInput.readObject());
            }
            dataInput.close();
            return inventory;
        } catch (Exception e) {
            throw new IllegalStateException("Unable to decode class type.", e);
        }
    }

    /**
     * Deserialize an {@link ItemStack} array.
     * @param data The serialized {@link ItemStack} array
     * @return The actual {@link ItemStack} array.
     * @throws IllegalStateException If any error occurs during the deserialization
     */
    public static ItemStack[] itemStackArrayFromBase64(String data) throws IllegalStateException {
        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(data));
            BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
            ItemStack[] items = new ItemStack[dataInput.readInt()];
            for (int i = 0; i < items.length; i++) {
                items[i] = (ItemStack) dataInput.readObject();
            }
            dataInput.close();
            return items;
        } catch (Exception e) {
            throw new IllegalStateException("Unable to decode class type.", e);
        }
    }
}
