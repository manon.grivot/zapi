package me.zodiakk.zapi.util;

import org.bukkit.ChatColor;

/**
 * Utility functions for strings manipulation.
 *
 * @author Zodiak
 * @since 1.0
 */
public final class StringUtil {
    /**
     * More efficient (but slightly slower) String.split(" ").
     * @param s The string to split
     * @return The resulting array of strings.
     * @deprecated Use {@link String#split(String)}.
     */
    @Deprecated
    public static final String[] tokenise(String s) {
        String[] w = s.split("\\s+");
        for (int i = 0; i < w.length; i++) {
            w[i] = w[i].replaceAll("[^\\w]", "");
        }
        return w;
    }

    /**
     * Converts a string array into a string (separated by spaces).
     * @param s The string array
     * @param index The number of strings to skip
     * @return The resulting string.
     * @deprecated Use {@link String#join(CharSequence, CharSequence...)}.
     */
    @Deprecated
    public static final String untokenise(String[] s, Integer index) {
        int i = 0;
        String result = "";
        for (String w : s) {
            if (i++ >= index) {
                result = result + w + " ";
            }
        }
        return result.substring(0, result.length() - 1);
    }

    /**
     * Converts a string array into a string (separated by spaces).
     * @param s The string array
     * @return The resulting string.
     * @deprecated Use {@link String#join(CharSequence, CharSequence...)}
     */
    @Deprecated
    public static final String untokenise(String[] s) {
        return untokenise(s, 0);
    }

    /**
     * Put colors into a text following a pattern.<br><br>
     * List of formatting rules:
     * $aqua, $black, $blue, $bold, $dark_aqua, $dark_blue,
     * $dark_gray, $dark_green, $dark_purple, $dark_red, $gold, $gray,
     * $green, $italic, $light_purple, $magic (blinking text), $red, $reset (reset all formatting),
     * $strikethrought, $underline, $white, $yellow, $mp (":").
     * @param s The text to format
     * @return The formatted text.
     * @deprecated Prefer using "§x" formatting instead.
     */
    @Deprecated
    public static final String colorFormat(String s) {
        return s.replace("$aqua", "" + ChatColor.AQUA)
                .replace("$black", "" + ChatColor.BLACK)
                .replace("$blue", "" + ChatColor.BLUE)
                .replace("$bold", "" + ChatColor.BOLD)
                .replace("$dark_aqua", "" + ChatColor.DARK_AQUA)
                .replace("$dark_blue", "" + ChatColor.DARK_BLUE)
                .replace("$dark_gray", "" + ChatColor.DARK_GRAY)
                .replace("$dark_green", "" + ChatColor.DARK_GREEN)
                .replace("$dark_purple", "" + ChatColor.DARK_PURPLE)
                .replace("$dark_red", "" + ChatColor.DARK_RED)
                .replace("$gold", "" + ChatColor.GOLD)
                .replace("$gray", "" + ChatColor.GRAY)
                .replace("$green", "" + ChatColor.GREEN)
                .replace("$italic", "" + ChatColor.ITALIC)
                .replace("$light_purple", "" + ChatColor.LIGHT_PURPLE)
                .replace("$magic", "" + ChatColor.MAGIC)
                .replace("$red", "" + ChatColor.RED)
                .replace("$reset", "" + ChatColor.RESET)
                .replace("$strikethrough", "" + ChatColor.STRIKETHROUGH)
                .replace("$underline", "" + ChatColor.UNDERLINE)
                .replace("$white", "" + ChatColor.WHITE)
                .replace("$yellow", "" + ChatColor.YELLOW)
                .replace("$mp", ":");
    }

    /**
     * Get a random string in the range of A-Z, 0-9.
     * @param length The length of the string
     * @return The generated string.
     * @deprecated Moved to {@link RandomUtil#getString(int)}
     */
    public static String getRandomString(int length) {
        return RandomUtil.getString(length);
    }

    /**
     * Get a random string in the specified range.
     * @param length The length of the string
     * @param range The range to use (a string containing all possible characters)
     * @return The generated string.
     * @deprecated Moved to {@link RandomUtil#getString(int, String)}
     */
    public static String getRandomString(int length, String range) {
        return RandomUtil.getString(length, range);
    }
}
