package me.zodiakk.zapi.util;

import java.util.Collection;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

/**
 * Utility functions for worlds.
 *
 * @author Zodiak
 * @since 2.2
 */
public final class WorldUtil {
    /**
     * Purge entities in a specified zone.
     * @param w The world in which the entities will be purged
     * @param l1 The first position representing this area
     * @param l2 The second position representing this area
     * @return The number of entities removed.
     */
    public static int purgeEntities(World w, Location l1, Location l2) {
        int i = 0;
        Location center = new Location(w, (l1.getX() + l2.getX()) / 2.0d,
                                          (l1.getY() + l2.getY()) / 2.0d,
                                          (l1.getZ() + l2.getZ()) / 2.0d);
        double xdistance = Math.abs((l1.getX() - l2.getX()) / 2.0d);
        double ydistance = Math.abs((l1.getY() - l2.getY()) / 2.0d);
        double zdistance = Math.abs((l1.getZ() - l2.getZ()) / 2.0d);

        for (Entity e : w.getNearbyEntities(center, xdistance, ydistance, zdistance)) {
            e.remove();
            i++;
        }
        return i;
    }

    /**
     * Purge entities in a specified zone.
     * @param w The world in which the entities will be purged
     * @param l1 The first position representing this area
     * @param l2 The second position representing this area
     * @param type The type of entity to remove
     * @return The number of entities removed.
     */
    public static int purgeEntities(World w, Location l1, Location l2, EntityType type) {
        int i = 0;
        Location center = new Location(w, (l1.getX() + l2.getX()) / 2.0d,
                                          (l1.getY() + l2.getY()) / 2.0d,
                                          (l1.getZ() + l2.getZ()) / 2.0d);
        double xdistance = Math.abs((l1.getX() - l2.getX()) / 2.0d);
        double ydistance = Math.abs((l1.getY() - l2.getY()) / 2.0d);
        double zdistance = Math.abs((l1.getZ() - l2.getZ()) / 2.0d);

        for (Entity e : w.getNearbyEntities(center, xdistance, ydistance, zdistance)) {
            if (e.getType().equals(type)) {
                e.remove();
                i++;
            }
        }
        return i;
    }

    /**
     * Purge entities in a specified zone.
     * @param w The world in which the entities will be purged
     * @param l1 The first position representing this area
     * @param l2 The second position representing this area
     * @param types A list of entity types to purge
     * @return The number of entities removed.
     */
    public static int purgeEntities(World w, Location l1, Location l2, Collection<EntityType> types) {
        int i = 0;
        Location center = new Location(w, (l1.getX() + l2.getX()) / 2.0d,
                                          (l1.getY() + l2.getY()) / 2.0d,
                                          (l1.getZ() + l2.getZ()) / 2.0d);
        double xdistance = Math.abs((l1.getX() - l2.getX()) / 2.0d);
        double ydistance = Math.abs((l1.getY() - l2.getY()) / 2.0d);
        double zdistance = Math.abs((l1.getZ() - l2.getZ()) / 2.0d);

        for (Entity e : w.getNearbyEntities(center, xdistance, ydistance, zdistance)) {
            for (EntityType type : types) {
                if (e.getType().equals(type)) {
                    e.remove();
                    i++;
                }
            }
        }

        return i;
    }

    /**
     * Compare two locations and return true if they are on the same location. By default, comparison is made with integers and ignores pitch and yaw.
     * @param l1 The first location to compare
     * @param l2 The second location to compare
     * @return True if these locations are equals
     */
    public static boolean compareLocations(Location l1, Location l2) {
        return compareLocations(l1, l2, false, false);
    }

    /**
     * Compare two locations and return true if they are on the same location.
     * @param l1 The first location to compare
     * @param l2 The second location to compare
     * @param precise Whether if comparison between points must be done with doubles instead of integers
     * @param orientation Whether if head orientation must be taken into account when comparing the locations
     * @return True if these locations are equals
     */
    public static boolean compareLocations(Location l1, Location l2, boolean precise, boolean orientation) {
        if (precise) {
            if (l1.getX() != l2.getX() || l1.getY() != l2.getY() || l1.getZ() != l2.getZ()) {
                return false;
            }
        } else {
            if (l1.getBlockX() != l2.getBlockX() || l1.getBlockY() != l2.getBlockY() || l1.getBlockZ() != l2.getBlockZ()) {
                return false;
            }
        }

        if (!l1.getWorld().getName().equals(l2.getWorld().getName())) {
            return false;
        }

        if (orientation) {
            if (l1.getPitch() != l2.getPitch() || l1.getYaw() != l2.getYaw()) {
                return false;
            }
        }

        return true;
    }
}
